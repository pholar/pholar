<?php
/**
 * Default values for settings in Pholar
 *
 * DO NOT EDIT THIS FILE
 * to change settings, add a file in "etc" called "<env>.settigns.php"
 * Default env is "default". Set env to use with env var "PHOLAR_ENV"
 */

use Pholar\Meta\Meta;
use Monolog\Logger;

$basedir = dirname(dirname(__DIR__));

return [
	'basedir' => $basedir,
	'db' => "sqlite:" . $basedir . "/var/database.sqlite3",
	'lastupdatefile' => $basedir . "/var/lastupdate",
	'app' => [
		'storage' => $basedir . "/storage",
		'expath' =>  null,
		'expath_win' => null,
		'writeback' => Meta::WRITEBACK_DEFAULT,
		'orderby' => "id DESC",
		'listfields' => ["filename", "title", "datetime"],
		'widgets' => ["tags", "people", "folders"],
		'autologin' => null,
		'sizes' => [
			'thumb' => 150,
			'preview' => 1080,
		],
	],
	'paths' => [
		'etc' => $basedir . '/etc',
		'public' => $basedir . '/public',
		'templates' => $basedir . '/templates',
		'cache' => $basedir . "/var/cache",
		'migrations' => $basedir . "/migrations",
	],
	'facetool' => [
		'enabled' => false,
		'commandline' => ['facetool'],
	],
	'folders' => [
		'limit' => 1,
	],
	'debug' => false,
	'display' => [
		'error_details' => false,
		'stats_panel' => false,
	],
	'logger' => [
		'name' => 'fotomrg',
		//
		'path' => $basedir . "/var/logs/app.log" ,
		'level' => Logger::WARNING,
	],
];
