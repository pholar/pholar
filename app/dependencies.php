<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
declare(strict_types=1);

use DI\ContainerBuilder;

use Psr\Container\ContainerInterface;
use Psr\Http\Client\ClientInterface;
use Psr\SimpleCache\CacheInterface;
use Psr\Log\LoggerInterface;

use Slim\Interfaces\RouteParserInterface;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Processor\UidProcessor;

use Pholar\SQL\SQLDriver;
use Envms\FluentPDO\Query;

use Slim\Views\Twig;
use Slim\Flash\Messages;

use GuzzleHttp\Client as HttpClient;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use Cache\Adapter\Filesystem\FilesystemCachePool;
use Geocoder\Geocoder;
use Geocoder\Provider\Provider as GeocoderProvider;
use Pholar\Geocoding\FakeProvider;

use Pholar\Utils;
use Pholar\TwigExtension;
use Pholar\SystemSettings;
use Pholar\Settings;

use Phinx\Config\Config;

return function (ContainerBuilder $containerBuilder) {
	$containerBuilder->addDefinitions([

		RouteParserInterface::class => function () {
			return \Pholar\App::getApp()->getRouteCollector()->getRouteParser();
		},

		LoggerInterface::class => function (SystemSettings $settings) {
			$loggerSettings = $settings->get('logger');
			$logger = new Logger($loggerSettings['name']);

			$processor = new UidProcessor();
			$logger->pushProcessor($processor);

			$handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
			$logger->pushHandler($handler);

			return $logger;
		},

		'logger' => function (LoggerInterface $logger) {
			return $logger;
		},

		SQLDriver::class => function (SystemSettings $settings) {
			$dbtype = $settings->get('dsn', 'scheme');
			$driverclass = [
				'sqlite' => \Pholar\SQL\SQLite::class,
				'mysql' => \Pholar\SQL\MySQL::class,
			][$dbtype];
			return new $driverclass();
		},

		PDO::class => function (LoggerInterface $logger, SQLDriver $sqldriver, SystemSettings $settings) {
			$dsn = $settings->get('dsn');
			switch ($dsn['scheme']) {
				case "sqlite":
					$pdodsn = $settings->get('db');
					break;
				case "mysql":
					$pdodsn = "mysql:";
					$pdodsn .= "host=" . $dsn['host'] . ";";
					$pdodsn .= "port=" . $dsn['port'] . ";";
					$pdodsn .= "dbname=" . $dsn['dbname'];
					break;
				default:
					throw new \DomainException("Unsupported DB driver '" . $dsn['scheme'] . "'");
			}

			$user = $dsn['user'] ?? null;
			$password = $dsn['pass'] ?? null;

			$logger->debug("pdo dsn: $pdodsn");
			try {
				if ($settings->get('display', 'stats_panel')) {
					$pdo = new \Pholar\Utils\Stats\PDOT($pdodsn, $user, $password);
				} else {
					$pdo = new PDO($pdodsn, $user, $password);
				}
			} catch (\PDOException $e) {
				$logger->critical($e->getMessage());
				die($e->getMessage() . " : '$pdodsn'");
			}
			
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$sqldriver->setupPdo($pdo);

			return $pdo;
		},

		Query::class => function (PDO $pdo) {
			$fpdo =  new Query($pdo);
			// this will work on fluent pdo 2.2.2 which I can't test
			// right now because I'm on php8 and fpdo has `php ^7.1` as requirement
			/*$fpdo->debug = function($BaseQuery) use ($container) {
				$container->get('log')->debug("FluentPDO", [
					'query' => $BaseQuery->getQuery(false),
					'parameters' => $BaseQuery->getParameters(),
					'rowCount' => $BaseQuery->getResult()->rowCount(),
				]);
			};*/
			return $fpdo;
		},

		// Twig message flash
		Messages::class => function () {
			return new Messages();
		},

		// Twig view
		Twig::class => function (ContainerInterface $container, SystemSettings $settings, Messages $flash) {
			$templatedir = $settings->get('paths', 'templates');
			$CLASS = Twig::class;
			if ($settings->get('display', 'stats_panel')) {
				$CLASS = \Pholar\Utils\Stats\SlimViewT::class;
			}

			$twig = $CLASS::create(
				$templatedir,
				[
					'cache' => Utils::istrue($settings->get('debug')) ? false : $settings->get('paths', 'cache'),
					'debug' => Utils::istrue($settings->get('debug'))
				]
			);

			// add extensions
			$twig->addExtension(new \Twig\Extension\DebugExtension());
			$twig->getEnvironment()->addExtension($container->get(TwigExtension::class));

			// add 'flash' global
			$environment = $twig->getEnvironment();
			$environment->addGlobal('flash', $flash);
			// default searchroute
			$environment->addGlobal('searchroute', 'search');

			return $twig;
		},

		Config::class => function (Settings $settings, PDO $pdo) {
			return new Config([
				'environments' => [
					$settings->getEnv() => [
						'adapter' => $settings->get('dsn', 'scheme'),
						'name' => $settings->get('dsn', 'dbname'),
						'connection' => $pdo,
					]
				],
				'paths' => [
					'migrations' => $settings->get('paths', 'migrations'),
				],
			]);
		},



		// geocoding
		ClientInterface::class => function () {
			return new HttpClient();
		},

		CacheInterface::class => function (SystemSettings $settings) {
			$filesystemAdapter = new Local($settings->get('paths', 'cache'));
			$filesystem        = new Filesystem($filesystemAdapter);
			$pool = new FilesystemCachePool($filesystem);
			return $pool;
		},

		GeocoderProvider::class => function (
			SystemSettings $settings,
			ClientInterface $httpclient,
			CacheInterface $cache,
		) {
			// TODO: nominatim server in settings
			$provider = \Geocoder\Provider\Nominatim\Nominatim::withOpenStreetMapServer(
				$httpclient,
				"Pholar-" . \Pholar\App::getVersion()
			);
			return new \Geocoder\Provider\Cache\ProviderCache(
				$provider,
				$cache,
				60*60*24*30 // cache duration: 30 days
			);
		},

		Geocoder::class => function (ContainerInterface $container, GeocoderProvider $provider) {
			return new \Geocoder\StatefulGeocoder(
				$provider
			);
		},



	]);
};
