<?php
/*
  Copyright (c) 2020 Fabio Comuni

  This file is part of Pholar.

  Pholar is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  Pholar is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
declare(strict_types=1);

use Slim\App;
use Slim\Handlers\ErrorHandler;
use Monolog\Logger;
use Pholar\Settings;
use Pholar\Utils;

return function (App $app, Settings $settings, Logger $logger) {
		/** @var bool $displayErrorDetails */
		$displayErrorDetails = Utils::istrue(
			$settings->get('display', 'error_details')
		);

		/*
		// Create Shutdown Handler
		$shutdownHandler = new ShutdownHandler($request, $errorHandler, $displayErrorDetails);
		register_shutdown_function($shutdownHandler);
		*/

		// Add Error Middleware
		$errorMiddleware = $app->addErrorMiddleware($displayErrorDetails, true, true, $logger);
		/** @var ErrorHandler */
		$errorHandler = $errorMiddleware->getDefaultErrorHandler();
		$errorHandler->registerErrorRenderer(
			'text/html',
			\Pholar\Exceptions\Renderer\HtmlRenderer::class
		);
};
