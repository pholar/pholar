<?php
declare(strict_types=1);

use Pholar\Controllers\NotificationsController;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

use Pholar\Controllers\ActionController;
use Pholar\Controllers\CollectionController;
use Pholar\Controllers\LoginController;
use Pholar\Controllers\PhotoController;
use Pholar\Controllers\ShareController;
use Pholar\Controllers\TagController;
use Pholar\Controllers\UploadController;
use Pholar\Controllers\FolderController;
use Pholar\Controllers\FaceController;

use Pholar\Controllers\Admin\AdminController;
use Pholar\Controllers\Admin\UsersController;

use Pholar\Middleware\LogoutGuestMiddleware;
use Pholar\Middleware\AuthMiddleware;
use Pholar\Middleware\AdminMiddleware;

return function (App $app) {

		$app->get('/login', LoginController::class . ':login')
			->setName('login');
		$app->post('/login', LoginController::class . ':postlogin')
			->setName('postlogin');
		$app->post('/logout', LoginController::class . ':logout')
			->setName('logout');

		$app->get(
			'/share/{sid}',
			ShareController::class . ':search'
		)
			->setName('sharesearch');
		$app->get(
			'/share/{sid}/photo/{id}',
			ShareController::class . ':details'
		)
			->setName('sharedetails');
		$app->get(
			'/share/{sid}/source/{id}',
			ShareController::class . ':source'
		)
			->setName('sharesource');
		$app->map(
			['GET', 'POST'],
			'/share/{sid}/login',
			ShareController::class . ':password'
		)
			->setName('sharelogin');

		$app->group('', function (Group $group) {
			$group->get('/', PhotoController::class . ':search')
				->setName('search');
			$group->get('/photo/{id}', PhotoController::class . ':details')
				->setName('details');
			$group->post('/photo/{id}/edit', PhotoController::class . ':edit')
				->setName('editmeta');
			$group->post('/photo/{id}/delete', PhotoController::class . ':delete')
				->setName('deletephoto');

			$group->get(
				'/collection/{cid}',
				CollectionController::class . ':search'
			)
				  ->setName('collectionsearch');
			$group->post(
				'/collection/{cid}',
				CollectionController::class . ':edit'
			)
				->setName('collectionedit');
			$group->get(
				'/collection/{cid}/photo/{id}',
				CollectionController::class . ':details'
			)
				->setName('collectiondetails');


			$group->post(
				'/tag/{tid}',
				TagController::class . ':rename'
			)
				->setName('tagrename');
			/**
			* This shit because I can't submit a form with method="delete"
			* (or "put"), bacause of stupid reasons.
			*
			* See https://softwareengineering.stackexchange.com/a/211790
			* to get angry
			*/
			$group->post(
				'/tag/{tid}/delete',
				TagController::class . ':delete'
			)
				->setName('tagdelete');

			$group->post('/face/{fid}', FaceController::class . ":rename")
				->setName('facerename');

			$group->post('/face/{fid}/delete', FaceController::class . ':delete')
				->setName('facedelete');
		})
			->add(LogoutGuestMiddleware::class)
			->add(AuthMiddleware::class);

		// share creation and share edit checks for user in controller
		// and has no use to be redirected to login page.
		$app->post(
			'/collection/{cid}/share',
			CollectionController::class . ':createShare'
		)
			  ->setName('sharecreate');

		$app->post(
			'/collection/{cid}/share/{suid}',
			CollectionController::class . ':editShare'
		)
			  ->setName('shareedit');

		// this are post-only reoutes where redirect has not use either
		$app->post('/system/folder', FolderController::class . ':create')
			  ->setName('folder.create');

		// this is notification json. if user not logged in, returns nothing anyway
		$app->get('/system/poll', NotificationsController::class . ":poll")
			->setName('poll');


		// this routes needs auth but can't be reached by guest
		$app->group('', function (Group $group) {
			$group->get('/system/thumbnail/{size}/{id}', PhotoController::class . ':thumbnail')
				->setName('thumb');
			$group->get('/system/source/{id}', PhotoController::class . ':source')
				->setName('source');

			$group->post('/system/batch', ActionController::class . ':batch')
				->setName('batch');

			$group->post('/system/update', ActionController::class . ':update')
				->setName('update');

			$group->get('/upload', UploadController::class . ':upload')
				->setName('upload');
			$group->post('/upload', UploadController::class . ':uploadPost')
				->setName('upload.post');
		})->add(AuthMiddleware::class);

		/* admin routes */
		$app->group('/admin', function (Group $group) {
			$group->map(['GET', 'POST'], '/advanced', AdminController::class . ':advancedQuery')
				->setName('advanced');

			$group->get('/users', UsersController::class . ':list')
				->setName('admin.users');

			$group->get('/users/new', UsersController::class . ':new')
				->setName('admin.users.new');

			$group->get('/users/user/{user}', UsersController::class . ':edit')
				->setName('admin.users.edit');

			$group->post('/users', UsersController::class . ':save')
				->setName('admin.users.save');

			$group->get('', AdminController::class . ':index')
				->setName('admin');
		})->add(AdminMiddleware::class);

		// Add Routing Middleware
		$app->addRoutingMiddleware();
};
