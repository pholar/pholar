<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * var_dump parameters and die
 *
 * @return never
 */
function dd()
{
	echo "<pre>";
	call_user_func_array('var_dump', func_get_args());
	die();
}

/**
 * var_dump parameters
 */
function d() : void
{
	echo "<pre>";
	call_user_func_array('var_dump', func_get_args());
	echo "</pre>";
}

/**
 * var_dump query and die
 *
 * @param  Envms\FluentPDO\Queries\Base $query
 * @return never
 */
function ddq($query) : void
{
	dd($query->getQuery(), $query->getParameters());
}

/**
 *  print instance class and id
 */
function di(mixed $obj) : void
{
	ob_start();
	var_dump($obj);
	echo preg_replace('|object\(([^)]*)\)(#[0-9]+).*|', "$1$2\n", explode("\n", ob_get_clean())[0]);
}
