<?php
/*
	Copyright (c) 2021 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\SQL;

use \PDO;

class MySQL extends SQLDriver
{
	const INSERT_IGNORE = "INSERT IGNORE";
	const INSERT_REPLACE = "REPLACE";
	const NOW = "NOW()";

	/**
	 * @inheritDoc
	 */
	public function quote(string $str) : string
	{
		return '`' . $str . '`';
	}

	/**
	 * @inheritDoc
	 */
	public function setupPdo(PDO $pdo) : void
	{
	}

	/**
	 * @inheritDoc
	 */
	public function prevnextQuery(string $query, int $photoid) : ?string
	{
		// this is the same query as in SQlite::prevnextQuery
		return sprintf(
			"WITH p1(id, prev, next) AS (
				SELECT id,  first_value(id) OVER win1 as prev , last_value(id) OVER win1 as next
				FROM (%s) as photo
				WINDOW win1 AS (ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING)
			) SELECT * from p1 WHERE id = %d",
			$query,
			$photoid,
		);
	}
}
