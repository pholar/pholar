<?php
/*
	Copyright (c) 2021 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\SQL;

use Jenssegers\ImageHash\ImageHash;
use Jenssegers\ImageHash\Hash;
use PDO;

class SQLite extends SQLDriver
{
	const INSERT_IGNORE = "INSERT OR IGNORE";
	const INSERT_REPLACE = "INSERT OR REPLACE";
	const NOW = "datetime('now')";

	/**
	 * @inheritDoc
	 */
	public function quote(string $str) : string
	{
		return '"' . $str . '"';
	}

	/**
	 * @inheritDoc
	 */
	public function setupPdo(PDO $pdo) : void
	{
		$pdo->setAttribute(PDO::ATTR_TIMEOUT, 5); //lock timeout in seconds
		// custom functions
		$pdo->sqliteCreateFunction('hashdistance', "\Pholar\SQL\SQLite::hashdistance", 2);
		/// from https://gcollazo.com/optimal-sqlite-settings-for-django/
		// activate WAL journaling
		$pdo->exec('PRAGMA journal_mode = WAL;');
		// enable foregin keys enforcement
		$pdo->exec('PRAGMA foreign_keys=ON;');
		// sync frequency
		$pdo->exec('PRAGMA synchronous = NORMAL;');
		// wait for a specified amount of time when a database file is locked,
		// rather than returning an error immediately
		$pdo->exec('PRAGMA busy_timeout = 5000;');
		// temporary tables and indices are kept as
		// if they were in pure in-memory databases.
		$pdo->exec('PRAGMA temp_store = MEMORY;');
		// the maximum number of bytes of the database file
		// that will be accessed using memory-mapped I/O
		$pdo->exec('PRAGMA mmap_size = 134217728;');
		//eh.. non mi è chiarissimo
		$pdo->exec('PRAGMA journal_size_limit = 67108864;');
		// maximum number of database disk pages that SQLite will
		// hold in memory at once per open database file
		$pdo->exec('PRAGMA cache_size = 2000;');
	}

	/**
	 * Custom functions for SQLite
	 */
	public static function hashdistance(string $hasha, string $hashb) : int
	{
		$hashah = Hash::fromBits($hasha);
		$hashbh = Hash::fromBits($hashb);
		$imghash = new ImageHash();
		$d = $imghash->distance($hashah, $hashbh);
		return $d;
	}

	/**
	 * @inheritDoc
	 */
	public function prevnextQuery(string $query, int $photoid) : ?string
	{
		// https://sqlite.org/windowfunctions.html
		//
		// We use `first_value()` and `last_value()` over a bounded window of the original query.
		// The inner query is the query built by `$this->getObjects()`.
		// We define a window over that, limited by (-1, 1).
		// So, for every row in the original query, windows functions will get:
		// the previous row, the current row, the next row.
		// `first_value(id)` will get the id value from the first row in window, that is the row before the current,
		// while `last_value(id)` will get the value from the last row in window.
		// We save this values in a new temp table `p1(id, prev, next)` and then we filter that by current photo id.
		// If there is no prevous or no next row, window will repeat the current row, so we then check in code that ids
		// are different.
		return sprintf(
			"WITH p1(id, prev, next) AS (
				SELECT id, first_value(id) OVER win1 as prev , last_value(id) OVER win1 as next
					FROM (%s) as photo
                	WINDOW win1 AS ( ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING )
			) SELECT * from p1 WHERE id = %d",
			$query,
			$photoid
		);
	}
}
