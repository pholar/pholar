<?php
/*
	Copyright (c) 2021 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\SQL;

use \PDO;

class SQLDriver
{
	const INSERT_IGNORE = "";
	const INSERT_REPLACE = "";
	const NOW = "";

	public function setupPdo(PDO $pdo) : void
	{
	}
 
	/**
	 * Quote a string value to be used in query (eg. a table or a column name)
	 */
	public function quote(string $str) : string
	{
		throw new \RuntimeException("Not implemented");
	}

	/**
	 * Get sql query to fetch prev/next photo in a search
	 *
	 * @param string $query Search sql query
	 * @param int $photoid  Current photo id
	 *
	 * @return ?string SQL string or null if not supported
	 */
	public function prevnextQuery(string $query, int $photoid) : ?string
	{
		throw new \RuntimeException("Not implemented");
	}
}
