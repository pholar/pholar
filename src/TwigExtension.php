<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar;

use Envms\FluentPDO\Queries\Select;
use Twig\Environment;
use Pholar\Settings;

class TwigExtension extends \Twig\Extension\AbstractExtension implements \Twig\Extension\GlobalsInterface
{
	/** @var Settings */
	protected $settings;

	/** @var PhotoMgr */
	protected $photomgr;

	/** @var TagMgr */
	protected $tagmgr;

	/** @var FaceMgr */
	protected $facemgr;

	/** @var CollectionMgr */
	protected $collectionmgr;

	/** @var UserMgr */
	protected $usermgr;

	public function __construct(
		Settings $settings,
		PhotoMgr $photomgr,
		TagMgr $tagmgr,
		FaceMgr $facemgr,
		CollectionMgr $collectionmgr,
		UserMgr $usermgr
	) {
		$this->settings = $settings;
		$this->photomgr = $photomgr;
		$this->tagmgr = $tagmgr;
		$this->facemgr = $facemgr;
		$this->collectionmgr = $collectionmgr;
		$this->usermgr = $usermgr;
	}

	/**
	 * @return Context
	 */
	public function getGlobals(): array
	{
		return [
			'user' => $this->usermgr->getForSession(),
			'listfields' => $this->settings->get('app', 'listfields'),
		];
	}

	/**
	 * @return \Twig\TwigFilter[]
	 */
	public function getFilters() : array
	{
		return [
			new \Twig\TwigFilter('repeat', 'str_repeat'),
		];
	}

	/**
	 * @return \Twig\TwigFunction[]
	 */
	public function getFunctions() : array
	{
		return [
			new \Twig\TwigFunction('settings', [$this, 'settings']),

			new \Twig\TwigFunction('tagCloud', [$this, 'tagCloud']),
			new \Twig\TwigFunction('collectionsCloud', [$this, 'collectionsCloud']),

			new \Twig\TwigFunction('tags', [$this, 'tags']),
			new \Twig\TwigFunction('people', [$this, 'people']),
			new \Twig\TwigFunction('thumbUrl', [$this, 'thumbUrl']),
			new \Twig\TwigFunction('canDisplayField', [$this, 'canDisplayField']),
			new \Twig\TwigFunction(
				'displayField',
				[$this, 'displayField'],
				[
					'is_safe' => ['html'],
					'needs_environment' => true
				]
			),
			new \Twig\TwigFunction(
				'displayFieldHtml',
				[$this, 'displayFieldHtml'],
				[
					'is_safe' => ['html'],
					'needs_environment' => true
				]
			),
			new \Twig\TwigFunction(
				'displayFieldValueHtml',
				[$this, 'displayFieldValueHtml'],
				[
					'is_safe' => ['html'],
					'needs_environment' => true
				]
			),
			new \Twig\TwigFunction('field_meta', [$this, 'fieldMeta']),
			new \Twig\TwigFunction(
				'orderDialog',
				[$this, 'orderDialog'],
				[
					'is_safe' => ['html'],
					'needs_environment' => true,
					'needs_context' => true
				]
			),
			new \Twig\TwigFunction(
				'stateQuery',
				[$this, 'stateQuery'],
				[
					'needs_context' => true
				]
			)
		];
	}

	/**
	 * get value from settings
	 */
	public function settings(string $f, string $k = null) : mixed
	{
		return $this->settings->get($f, $k);
	}

	/**
	 * get tag cloud values
	 */
	public function tagCloud() : Select
	{
		return $this->tagmgr->photoCount();
	}

	/**
	 * get collection cloud values
	 */
	public function collectionsCloud() : Select
	{
		return $this->collectionmgr->photoCount();
	}

	/**
	 * Get the thumb url for the photo at the given size
	 *
	 * If photo is a multipage document, page can be passed also
	 * If thumb is not found on disk, url to generic thumb image is returned
	 * and a task to generate it is scheduled
	 *
	 * @param int|PhotoData $photo Photo id or photo data array
	 */
	public function thumbUrl(int|array $photo, int $size, int $page = 1) : string
	{
		if (is_array($photo)) {
			$photo = $photo['id'];
		}
		$id = intval($photo);
		
		$fallbackimageurl = "/img/thumb-in-progress.png";
		$staticthumburl = "/thumb/$size/$id.jpg";
		if ($page > 1) {
			$staticthumburl = "/thumb/{$size}/{$id}p{$page}.jpg";
		}

		$thumbfile = $this->settings->get('paths', 'public'). $staticthumburl;
		
		if (file_exists($thumbfile)) {
			return $staticthumburl;
		} else {
			$this->photomgr->scheduleThumbs($id);
			return $fallbackimageurl;
		}
	}

	/**
	 * get tags for photo
	 *
	 * @param int|PhotoData $photo Photo id or photo data
	 */
	public function tags(int|array $photo) : Select
	{
		if (is_array($photo)) {
			$photo = $photo['id'];
		}
		$photo = intval($photo);
		return $this->photomgr->getTagsFor($photo);
	}
	
	/**
	 * get faces for photo
	 *
	 * @param int|PhotoData $photo Photo id or photo data
	 */
	public function people(int|array $photo) : Select
	{
		if (is_array($photo)) {
			$photo = $photo['id'];
		}
		$photo = intval($photo);
		return $this->facemgr->getFacesFor($photo);
	}
	

	/**
	 * get collections for photo
	 *
	 * @param int|PhotoData $photo Photo id or photo data
	 */
	public function collections($photo) : Select
	{
		if (is_array($photo)) {
			$photo = $photo['id'];
		}
		$photo = intval($photo);
		return $this->photomgr->getCollectionsFor($photo);
	}

	/**
	 * return true if photo field can be displayed to user.
	 *
	 * A field can be displayed if is set, is not empty and if field meta
	 * says that is visible
	 *
	 * @param PhotoData  $photo
	 * @param string $field Field name
	 **/
	public function canDisplayField(array $photo, string $field) : bool
	{
		if (!isset($photo[$field]) || trim($photo[$field]) === "") {
			return false;
		}
		$meta = Meta\Meta::fieldsMeta($field);
		if (is_null($meta)) {
			return false;
		}
		if ($meta['visible'] === false) {
			return false;
		}
		return true;
	}
	
	/**
	 * Return display string form for a given field in a photo
	 *
	 * @param PhotoData  $photo
	 * @param string $field Field name
	 * @return string
	 */
	public function displayField(Environment $env, array $photo, string $field) : string
	{
		$meta = Meta\Meta::fieldsMeta($field);
		if (is_null($meta)) {
			return "";
		}
		$class = $meta['class'];
		return $class::display($field, $photo[$field], $env);
	}

	/**
	 * Return html string form for a given field in a photo
	 *
	 * @param PhotoData  $photo
	 * @param string $field Field name
	 * @return string
	 */
	public function displayFieldHtml(Environment $env, array $photo, string $field) : string
	{
		return $this->displayFieldValueHtml($env, $photo[$field], $field);
	}

	/**
	 * Return html string form for a value of a given field
	 *
	 * @param mixed $value Field value
	 * @param string $field Field name
	 * @return string
	 */
	public function displayFieldValueHtml(Environment $env, mixed $value, string $field) : string
	{
		$meta = Meta\Meta::fieldsMeta($field);
		if (is_null($meta)) {
			return "";
		}
		$class = $meta['class'];
		return $class::displayHtml($field, $value, $env);
	}

	/**
	 * Return meta info about a field
	 *
	 * @param string $fieldname
	 * @return FieldMeta
	 */
	public function fieldMeta(string $fieldname) : ?array
	{
		return Meta\Meta::fieldsMeta($fieldname);
	}

	/**
	 * Render order dialog
	 *
	 * @param Context $ctx View context (auto)
	 * @return string
	 */
	public function orderDialog(Environment $env, array $ctx) : string
	{
		$fields = [
			'title', 'filename', 'location', 'filesize', 'datetime',
			'recordmtime', 'mtime'
		];
		list($current, $direction) = explode(
			' ',
			$ctx['order'] ?? $this->settings->get('app', 'orderby')
		);
		$direction = strtoupper($direction);
		$inverse = ($direction == "ASC") ? "DESC" : "ASC";
		return $env->render(
			'fragments/order-dialog.twig',
			array_merge($ctx, [
				'fields' => $fields,
				'current' => $current,
				'direction' => $direction,
				'inverse' => $inverse
			])
		);
	}

	/**
	 * Build querystring map with state values and optional new values
	 *
	 * `urlFor()` gets a mapping to build querystring. This function ensures that
	 * needed querystring params to track state are set.
	 *
	 * Optional new params (or new values for state) cam be passed vi $newctx
	 *
	 * usage:
	 *
	 *    urlFor('named.view', stateQuery({'foo': 'bar'}))
	 *
	 * results in
	 *
	 *    /path/to/view?q=..&p=..&o=..&foo=bar
	 *
	 * @param Context $ctx View context (auto)
	 * @param Context $newctx
	 * @return Context
	 */
	public function stateQuery(array $ctx, array $newctx = []) : array
	{
		return array_merge(
			[
				'q' => $ctx['query'] ?? null,
				'p' => $ctx['page'] ?? null,
				'o' => $ctx['order'] ?? null,
			],
			$newctx
		);
	}
}
