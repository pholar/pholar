<?php
/*
	Copyright (c) 2023 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Tasks;

use Pholar\PhotoMgr;

class ExtractMeta implements ITask
{
	/** @var PhotoMgr */
	private $photomgr;

	public function __construct(PhotoMgr $photomgr)
	{
		$this->photomgr = $photomgr;
	}

	/**
	 * @param TaskArgs $args
	 */
	public function run(int $taskid, array $args) : ?string
	{
		$id = $args['id'];
		$this->photomgr->updateById($id);
		return null;
	}
}
