<?php
/*
	Copyright (c) 2023 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Tasks;

use Pholar\Settings;
use Pholar\PhotoMgr;
use Pholar\Thumbnailer;

class Thumbnail implements ITask
{
	/** @var Settings */
	private $settings;

	/** @var PhotoMgr */
	private $photomgr;

	/** @var Thumbnailer */
	private $thumbnailer;

	public function __construct(
		Settings $settings,
		PhotoMgr $photomgr,
		Thumbnailer $thumbnailer,
	) {
		$this->settings = $settings;
		$this->photomgr = $photomgr;
		$this->thumbnailer = $thumbnailer;
	}

	public function run(int $taskid, array $args) : ?string
	{
		$id = $args['id'];

		$p = $this->photomgr->byId($id);
		if (is_null($p)) {
			throw new \Exception("Thumbnail on upload: No photo with id $id");
		}

		$mime = $p['format'];

		$sourcefile = $this->photomgr->getFullSourcePath($p);
		if (!file_exists($sourcefile)) {
			throw new \Exception("No source '$sourcefile' for photo id $id");
		}

		$public = $this->settings->get('paths', 'public');

		foreach ($this->settings->get('app', 'sizes') as $size) {
			$thumbfile = $public . "/thumb/{$size}/{$id}.jpg";
			$image = $this->thumbnailer->make($sourcefile, $mime, $size, $thumbfile);
		}

		return null;
	}
}
