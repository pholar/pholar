<?php
/*
	Copyright (c) 2023 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Tasks;

use Envms\FluentPDO\Query;

use Pholar\Settings;
use Pholar\TaskMgr;
use Pholar\FaceMgr;
use Pholar\Exceptions\TaskRescheduled;

class FaceTag implements ITask
{
	/** @var Query */
	protected $db;

	/** @var Settings */
	protected $settings;

	/** @var TaskMgr */
	protected $taskmgr;
	
	/** @var FaceMgr */
	protected $facemgr;

	public function __construct(Query $db, Settings $settings, TaskMgr $taskmgr, FaceMgr $facemgr)
	{
		$this->db = $db;
		$this->settings = $settings;
		$this->taskmgr = $taskmgr;
		$this->facemgr = $facemgr;
	}

	/**
	 * @param array<string,mixed> $args
	 */
	public function run(int $taskid, array $args) : ?string
	{
		$face_id = (int) $args['face_id'];

		$face = $this->facemgr->byId($face_id);
		$photo_id = $face['photo_id'];

		$size = $this->settings->get('app', 'sizes')['preview'];
		$thumbpath = $this->settings->get('paths', 'public') . '/thumb/' . $size . '/' . $photo_id . ".jpg";

		if (! file_exists($thumbpath)) {
			$tid = $this->taskmgr->schedule(Thumbnail::class, ['id'=>$photo_id, 'size'=>$size]);
			$this->taskmgr->reschedule($taskid, ['dependson' => $tid]);
			throw new TaskRescheduled();
		}
		
		$this->facemgr->tagFace($face, $thumbpath);

		return null;
	}
}
