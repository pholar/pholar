<?php
/*
	Copyright (c) 2023 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Tasks;

use InvalidArgumentException;
use Exception;

use Envms\FluentPDO\Query;
use Envms\FluentPDO\Literal;
use Pholar\PhotoMgr;
use Pholar\Meta\Meta;
use Pholar\SQL\SQLDriver;

class WritebackMeta implements ITask
{
	/** @var Query */
	private $db;

	/** @var PhotoMgr */
	private $photomgr;

	/** @var Meta */
	private $meta;

	/** @var SQLDriver */
	private $sqldriver;
	
	public function __construct(Query $db, PhotoMgr $photomgr, Meta $meta, SQLDriver $sqldriver)
	{
		$this->db = $db;
		$this->photomgr = $photomgr;
		$this->meta = $meta;
		$this->sqldriver = $sqldriver;
	}

	public function run(int $taskid, array $args) : ?string
	{
		$id = $args['id'] ?? null;
		if (is_null($id)) {
			throw new InvalidArgumentException("Param 'id' cannot be null");
		}
		$id = (int) $id;
		$photo = $this->photomgr->byId($id);
		if (is_null($photo)) {
			throw new Exception("Photo #{$id} not found");
		}

		$tags = $this->photomgr->getTagsFor($id)->fetchAll();
		$tags = array_map(
			function ($e) {
				return $e['tag'];
			},
			$tags
		);
		$collections = $this->photomgr->getCollectionsFor($id)->fetchAll();
		$collections = array_map(
			function ($e) {
				return "collection:" . $e['name'];
			},
			$collections
		);
		$photo['_tags'] = array_merge($tags, $collections);

		$this->meta->put($photo);

		#dd(__file__);
		// update mtime e recordmtime record to now...
		$literalnow = $this->sqldriver::NOW;
		$this->db->update('photo')
				->set('recordmtime', new Literal($literalnow))
				->set('mtime', new Literal($literalnow))
				->where('id', $id)
				->execute();
		
		return null;
	}
}
