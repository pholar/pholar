<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Psr7;

use Slim\Psr7\Stream;

/*
 * Extend Slim\Psr7\Strema to delete temp file after closing
 */
class TempStream extends Stream
{
	/** @var string */
	private $filename;

	public function __construct(string $filename)
	{
		$this->filename = $filename;
		$fh = fopen($filename, 'rb');
		parent::__construct($fh);
	}

	public function __destruct()
	{
		$this->close();
		unlink($this->filename);
	}
}
