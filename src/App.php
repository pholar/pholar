<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar;

use Symfony\Component\Dotenv\Dotenv;

use DI\ContainerBuilder;
use Slim\Factory\AppFactory;
use Slim\App as SlimApp;
use Pholar\Settings;

class App
{

	/** @var ?SlimApp */
	protected static $app = null;

	/**
	 * Get app insstance
	 *
	 * @return SlimApp
	 */
	public static function getApp() : SlimApp
	{
		if (is_null(self::$app)) {
			self::$app = self::setup();
		}
		return self::$app;
	}

	/**
	 * Get application version string
	 */
	public static function getVersion() : string
	{
		return "v0.2.0";
	}


	/**
	 * Setup app instance
	 *
	 * @return SlimApp
	 */
	public static function setup() : SlimApp
	{
		error_reporting(E_ALL ^ E_DEPRECATED);
		session_start();

		if (file_exists(__DIR__ . '/../.env')) {
			$dotenv = new Dotenv();
			$dotenv->loadEnv(__DIR__ . '/../.env');
		}
		
		// Instantiate PHP-DI ContainerBuilder
		$containerBuilder = new ContainerBuilder();

		#$containerBuilder->enableCompilation($_ENV['DEBUG'] ?? false);

		// Set up dependencies
		$dependencies = require __DIR__ . '/../app/dependencies.php';
		$dependencies($containerBuilder);

		// Set up repositories
		#$repositories = require __DIR__ . '/../app/repositories.php';
		#$repositories($containerBuilder);

		// Build PHP-DI Container instance
		$container = $containerBuilder->build();

		// Instantiate the app
		AppFactory::setContainer($container);
		$app = AppFactory::create();
		$callableResolver = $app->getCallableResolver();

		$logger = $container->get('logger');
		$settings = $container->get(Settings::class);

		// Register routes
		$routes = require __DIR__ . '/../app/routes.php';
		$routes($app);

		// Register middleware
		$middleware = require __DIR__ . '/../app/middleware.php';
		$middleware($app, $settings);


		// Register error handlersSettings
		$errorhandler = require __DIR__ . '/../app/errorhandlers.php';
		$errorhandler($app, $settings, $logger);

		return $app;
	}

	public static function run() : void
	{
		$app = self::getApp();


		// Run app and emit response
		$app->run();
	}
}
