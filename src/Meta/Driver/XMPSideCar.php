<?php
/*
	Copyright (c) 2021 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar\Meta\Driver;

use Pholar\Meta\Meta;

use Pholar\App;
use PHPExiftool\Reader;
use PHPExiftool\Driver\Value\ValueInterface;
use PHPExiftool\Exception\RuntimeException;

/**
 * XMP Sidecar Metadata Driver
 *
 * Read and Write metadata from and into XML-encoded XMP Sidecar file, unsing ExifTool
 */
class XMPSideCar extends ExifTool
{
	/**
	 * @inherithDoc
	 */
	public function read(string $filename, array $info = []) : array
	{
		$metadatas = [];
		try {
			$reader = Reader::create($this->logger);
			$metadatas = $reader->files($filename)->first();
		} catch (RuntimeException $e) {
			if ($this->settings->get('debug')) {
				throw $e;
			}
			$this->logger->warning("Exif Reader Exception: {$e}");
		}

		// set info from metadata.
		// Set only fields editable by user, overwritting existing values
		foreach ($metadatas as $metadata) {
			if ($metadata->getValue()->getType() !== ValueInterface::TYPE_BINARY) {
				$tag = $metadata->getTag();
				$field = Meta::mapTagToField($tag->getTagname());

				// writable fields are mapped back to tags
				$is_writable = !is_null(Meta::mapFieldToTag($field));

				if ($is_writable) {
					$info[$field] = $metadata->getValue()->asString();
				}
			}
		}
		return $info;
	}

	/**
	 * @param PhotoData  $photo
	 */
	public function write(array $photo) : void
	{
		$destfile = $this->photomgr->getFullSourcePath($photo) . ".xmp";
		if (file_exists($destfile)) {
			unlink($destfile);
		}
		$this->doWrite($photo, null, $destfile);
	}
}
