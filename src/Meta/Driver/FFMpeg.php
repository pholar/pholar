<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar\Meta\Driver;

use Pholar\Utils;
use Symfony\Component\Process\Exception\ProcessFailedException;

class FFMpeg extends Base
{
	/**
	 * @inherithDoc
	 */
	public function read(string $filename, array $info = []) : array
	{
		$cmd = ['ffprobe', '-v', 'quiet', '-print_format', 'json', '-show_format', '-show_streams', $filename];
		try {
			$strdata = Utils::runcmd($cmd);
		} catch (ProcessFailedException $e) {
			return $info;
		}

		$data = json_decode($strdata);

		// container
		$format = $data->format;
		$info['type'] = $format->format_long_name;
		$info['duration'] = floatval($format->duration);
		$info['bitrate'] = intval($format->bit_rate);
		$info['streams'] = intval($format->nb_streams);

		// streams
		// we indicize only the first video stream and the first audio stream
		$video = false;
		$audio = false;
		foreach ($data->streams as $stream) {
			if (!$video && $stream->codec_type == "video") {
				$video = true;
				$info['videocodec'] = $stream->codec_long_name;
				$info['width'] = intval($stream->width);
				$info['height'] = intval($stream->height);
				$info['pixelformat'] = $stream->pix_fmt;
				$info['framerate'] = $stream->r_frame_rate;
				$info['videobitrate'] = intval($stream->bit_rate);
			}
			if (!$audio && $stream->codec_type == "audio") {
				$audio = true;
				$info['audiocodec'] = $stream->codec_long_name;
				$info['samplerate'] = intval($stream->sample_rate);
				$info['channels'] = intval($stream->channels);

				$channellayout = "";
				if (property_exists($stream, 'channel_layout')) {
					$channellayout = $stream->channel_layout;
				}
				$info['channellayout'] = $channellayout;
				$info['audiobitrate'] = intval($stream->bit_rate);
			}
		}
	
		return $info;
	}
}
