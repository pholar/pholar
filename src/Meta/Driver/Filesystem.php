<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar\Meta\Driver;

use Pholar\Utils;
use Symfony\Component\Process\Exception\ProcessFailedException;

class Filesystem extends Base
{
	/**
	 * @inherithDoc
	 */
	public function read(string $filename, array $info = []) : array
	{
		$info = array_merge($info, [
			'filename' => basename($filename),
			'location' => dirname($filename),
			'filesize' => filesize($filename),
			'mtime' => date('Y-m-d H:i:s', filemtime($filename)),
			'format' => mime_content_type($filename),
			'type' => strtoupper(pathinfo($filename, PATHINFO_EXTENSION))
		]);

		// get mimetype
		if (!isset($info['format'])) {
			try {
				$info['format'] = Utils::runcmd(['file', '-b', '--mime-type', $filename]);
			} catch (ProcessFailedException $e) {
				$this->logger->warning("'file' failed. setting failback mime", [$e]);
				$info['format'] = 'application/octet-stream';
			}
		}

		return $info;
	}
}
