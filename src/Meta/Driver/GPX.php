<?php
/*
	Copyright (c) 2021 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar\Meta\Driver;

class GPX extends Base
{
	/**
	 * @inherithDoc
	 */
	public function read(string $filename, array $info = []) : array
	{
		$info['format'] = "application/gpx+xml";

		$xmlstr = file_get_contents($filename);
		$gpx = new \SimpleXMLElement($xmlstr);

		if (!is_null($gpx['creator'] ?? null)) {
			$info['creatortool'] = $gpx['creator'];
		}
		if ($gpx->metadata->time) {
			$info['datetime'] = "" . $gpx->metadata->time;
		}

		// TODO: this is very much wrong.
		/*
		$pt = $gpx->xpath('//trkpt')[0];
		$info['latitude'] = floatval($pt['lat']);
		$info['longitude'] = floatval($pt['lon']);
		if ($pt->ele) {
			$info['altitude'] = floatval($pt->ele);
		}
		*/

		return $info;
	}
}
