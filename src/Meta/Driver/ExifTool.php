<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar\Meta\Driver;

use Exception;

use Pholar\Meta\Meta;

use PHPExiftool\Reader;
use PHPExiftool\Writer;
use PHPExiftool\Driver\TagFactory;
use PHPExiftool\Driver\Metadata\MetadataBag;
use PHPExiftool\Driver\Metadata\Metadata;
use PHPExiftool\Driver\Value\Mono;
use PHPExiftool\Driver\Value\Multi;
use PHPExiftool\Driver\Value\ValueInterface;
use PHPExiftool\Exception\RuntimeException;
use PHPExiftool\Exception\TagUnknown;

/**
 * ExifTool Metadata Driver
 *
 * Read and Write metadata from and into image files
 */
class ExifTool extends Base
{
	/**
	 * @inherithDoc
	 */
	public function read(string $filename, array $info = []) : array
	{
		$metadatas = [];
		try {
			$reader = Reader::create($this->logger);
			$metadatas = $reader->files($filename)->first();
		} catch (RuntimeException $e) {
			$this->logger->info("Exif Reader Exception: {$e}");
		}

		// set info from metadata.
		// The first metadata sets the info field.
		// Next metadatas mapped to same field will be ignored.
		foreach ($metadatas as $metadata) {
			if ($metadata->getValue()->getType() !== ValueInterface::TYPE_BINARY) {
				$tag = $metadata->getTag();
				$field = Meta::mapTagToField($tag->getTagname());
				if (!is_null($field) && !isset($info[$field])) {
					$info[$field] = $metadata->getValue()->asString();
				}
			}
		}
		return $info;
	}

	/**
	 * @param PhotoData  $photo
	 */
	public function write(array $photo) : void
	{
		$sourcefile = $this->photomgr->getFullSourcePath($photo);
		$this->doWrite($photo, $sourcefile);
	}

	/**
	 * @param PhotoData  $photo
	 * @param ?string $sourcefile
	 * @param ?string $destfile
	 */
	protected function doWrite(array $photo, string $sourcefile = null, string $destfile = null) : void
	{
		$writer = Writer::create($this->logger);
		$bag = new MetadataBag();
		foreach ($photo as $field => $value) {
			if (is_numeric($value)) {
				if (strpos($value, ".")) {
					$value = floatval($value);
				} else {
					$value = intval($value);
				}
			}

			$tagnames = Meta::mapFieldToTag($field);
			if (!is_null($tagnames)) {
				foreach ($tagnames as $tagname) {
					try {
						$tagobject = TagFactory::getFromRDFTagname($tagname);
					} catch (TagUnknown $e) {
						continue;
					}

					// io avrei pensato che l'oggetto "Metadata" di cui
					// sotto si preoccupasse di formattare i valori per
					// la riga di comando, e invece.. no.. devo farlo io
					// tra l'altro sembra che solo 'Orientation' abbia bisogno
					// di ciò..

					// qui TagInterface::getValues() dice che ritorna 'array'
					// ma mica tutti quelli che lo implementano ritornano 'array', alcuni ritornano 'null'
					// quindi per farci contenti tutti, forziamo il tipo di $tagvalues
					/** @var ?array<string,?array<string,mixed>> */
					$tagvalues = $tagobject->getValues();
					if (!is_null($tagvalues)) {
						$tagvalue = $tagvalues[$value] ?? null;
						if (!is_null($tagvalue)) {
							$tagvalue = $tagvalue['Label'];
						}
					} else {
						$tagvalue = $value;
					}

					if (is_array($value)) {
						$tagvalue = new Multi($tagvalue);
					} else {
						$tagvalue = new Mono($tagvalue);
					}

					$bag->add(new Metadata($tagobject, $tagvalue));
				}
			}
		}

		try {
			$writer->write($sourcefile, $bag, $destfile);
		} catch (Exception $e) {
			if ($this->settings->get('debug')) {
				throw $e;
			}
			$this->logger->warning("Exif Writer Exception: {$e}");
		}
	}
}
