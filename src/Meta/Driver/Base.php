<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar\Meta\Driver;

use Pholar\PhotoMgr;
use Pholar\Settings;
use Psr\Log\LoggerInterface;

class Base
{
	/** @var Settings */
	protected $settings;

	/** @var LoggerInterface */
	protected $logger;

	/** @var PhotoMgr */
	protected $photomgr;

	public function __construct(Settings $settings, LoggerInterface $logger, PhotoMgr $photomgr)
	{
		$this->settings = $settings;
		$this->logger = $logger;
		$this->photomgr = $photomgr;
	}

	/**
	 * @param Metadata $info
	 * @return Metadata
	 */
	public function read(string $filename, array $info = []) : array
	{
		$this->logger->warning("Driver " . static::class . " don't define a 'read()' method");
		return $info;
	}

	/**
	 * @param PhotoData  $photo
	 */
	public function write(array $photo) : void
	{
		$this->logger->warning("Driver " . static::class . " don't define a 'write()' method");
	}
}
