<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar\Meta;

use Pholar\App;
use Pholar\Utils;
use Pholar\Settings;

use Psr\Log\LoggerInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;

class Meta
{
	const WRITEBACK_DEFAULT = 'default';
	const WRITEBACK_XMP = 'xmp';
	const WRITEBACK_NONE = 'none';

	/** @var Settings */
	private $settings;

	/** @var LoggerInterface */
	private $logger;

	/** @var Metadata */
	private $info;

	/**
	 * Get metadata for field
	 *
	 * ```
	 *          [
	 *              'class' => string, // Field handling class
	 *              'search' => bool,  // if true, field is searched in generic query
	 *              'edit' => bool,    // field is user editable
	 *              'visible' => bool  // field is visibile in photo details
	 *          ]
	 * ```
	 *
	 * @param string $name Field name
	 * @return ?FieldMeta
	 */
	public static function fieldsMeta(string $name) : ?array
	{
		return Fields::$meta[$name] ?? null;
	}

	/**
	 * Get a list of fields to be used in "generic" search
	 *
	 * @return array<string>
	 */
	public static function searchableFields() : array
	{
		return Fields::$searchable;
	}

	/**
	 * Get field for a metadata tag
	 *
	 * @param string $tag Tag name as 'Namespace:TagName'
	 * @return ?string Field name or null if tag is not mapped
	 */
	public static function mapTagToField(string $tag) : ?string
	{
		return Fields::$tagToField[$tag] ?? null;
	}

	/**
	 * Get metadata tags for a field
	 *
	 * Returns an array, as a field can be mapped to many tags
	 *
	 * @param string $field Field name
	 * @return ?array<string> Tag names as 'Namespace:TagName' or null
	 */
	public static function mapFieldToTag(string $field) : ?array
	{
		return Fields::$editableFieldToTag[$field] ?? null;
	}

	public function __construct(Settings $settings, LoggerInterface $logger)
	{
		$this->settings = $settings;
		$this->logger = $logger;
	}

	/**
	 * Read meta from $filename via $driver
	 *
	 * Results are assigned to $this->info
	 *
	 * @param string $driver Driver class name
	 * @param string $filename
	 */
	private function readDriver(string $driver, string $filename) : void
	{
		$cls = '\\Pholar\\Meta\\Driver\\' . $driver;
		$driver = App::getApp()->getContainer()->get($cls);
		$this->info = $driver->read($filename, $this->info);
	}

	/**
	 * Write metadata from $photo into $destfile via $driver
	 *
	 * @param string $driver Driver class name
	 * @param PhotoData  $photo
	 * @param ?string $destfile Output file. If null, is up to the driver to choose a location
	 */
	private function writeDriver(string $driver, array $photo, string $destfile = null) : void
	{
		$cls = '\\Pholar\\Meta\\Driver\\' . $driver;
		$driver = App::getApp()->getContainer()->get($cls);
		$driver->write($photo, $destfile);
	}

	/**
	 * Extract metadata from document
	 *
	 * Until we find how to extract meta directly from a document file,
	 * we convert it to PDF and then extract meta from there
	 *
	 * Results are assigned to $this->info
	 */
	private function getDocument(string $filename) : void
	{
		# TODO find a way to extract meta from document directly
		// convert to pdf and extract info
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		$tmpfile = "/tmp/" . str_replace(".$ext", ".pdf", basename($filename));
		$cmd = ['soffice', '--headless', '--convert-to', 'pdf', '--outdir', '/tmp', $filename];

		try {
			Utils::runcmd($cmd);
			// we are using exiftool to extract document info from pdf,
			$this->readDriver("ExifTool", $tmpfile);
			unset($this->info['creator']); // this is alwais "Writer", let's ignore it
			unlink($tmpfile);
		} catch (ProcessFailedException $e) {
			unlink($tmpfile);
		}

		// reset office mimes
		$msexts = ['docx','pptx', 'xlsx'];
		if (in_array($ext, $msexts) && $this->info['format'] == 'application/zip') {
			$this->info['type'] = strtoupper($ext);
			try {
				$this->info['format'] = Utils::runcmd(['file', '-b', '--mime-type', $filename]);
			} catch (ProcessFailedException $e) {
				$this->logger->warning("'file' failed. setting failback mime", [$e]);
				$this->info['format'] = 'application/octet-stream';
			}
		}
	}

	/**
	 * Get basic filesystem metadata for file
	 *
	 * This use only Filesystem driver to get basic info about file
	 *
	 * @param string $filename Full path to file
	 * @return Metadata File metadata
	 */
	public function getBasic(string $filename) : array
	{
		$this->info = [];
		$this->readDriver("Filesystem", $filename);
		return $this->info;
	}


	/**
	 * Get metadata from file using all possible drivers
	 *
	 * @param string $filename Full path to file
	 * @return Metadata File metadata
	 */
	public function get(string $filename) : array
	{
		$this->info = [];

		$this->readDriver("Filesystem", $filename);

		$mime = $this->info['format'];
		list($mime_maj, $mime_min) = explode("/", $mime);

		// extract metadata by file type

		// try exiftool on everything
		$this->readDriver("ExifTool", $filename);

		// . images
		if ($mime_maj == "image") {
			$this->readDriver("ImageHash", $filename);
		}

		// . audio video
		if ($mime_maj == "video" || $mime_maj == "audio") {
			$this->readDriver("FFMpeg", $filename);
		}
		// . documents
		if (strpos($mime_min, 'document') > 0 || $mime == "text/rtf") {
			$this->getDocument($filename);
		}
		// . pdf
		if ($mime_min == "pdf" || $mime_min == "postscript") {
			$this->readDriver("ExifTool", $filename);
		}

		if (($mime == "text/xml" && Utils::endsWith($filename, ".gpx")) || $mime == "application/gpx+xml") {
			$this->readDriver("GPX", $filename);
		}

		if (file_exists($filename . ".xmp")) {
			// read metadata from sidecar xmp file
			$this->logger->info("Read metadata from sidecar xmp file");
			$this->readDriver("XMPSideCar", $filename . ".xmp");
		}

		if (! isset($this->info['width']) ||  ! isset($this->info['height'])) {
			// try to get size from identify
			try {
				$r = Utils::runcmd(['identify', '-verbose', $filename."[0]"]);
			} catch (ProcessFailedException $e) {
				$r = "";
			}
			$r = array_map('trim', explode("\n", $r));
			foreach ($r as $line) {
				if (Utils::startsWith($line, "Geometry:")) {
					$line = str_replace("Geometry: ", "", $line);
					list($w, $h) = explode("x", explode("+", $line)[0]);
					$this->info['width'] = $w;
					$this->info['height'] = $h;
				}
			}
			#dd($this->info);
		}
		if (! isset($this->info['width']) ||  ! isset($this->info['height'])) {
			// try to get size from file
			$r = Utils::runcmd(['file', '-b', $filename]);
			$r = array_map('trim', explode(',', $r));
			foreach ($r as $line) {
				if (preg_match("|^(\d+)\s*x\s*(\d+)$|", $line, $matches)) {
					$this->info['width'] = $matches[1];
					$this->info['height'] = $matches[2];
				}
				if (Utils::startsWith($line, "width=")) {
					$this->info['width'] = str_replace("width=", "", $line);
				}
				if (Utils::startsWith($line, "height=")) {
					$this->info['height'] = str_replace("height=", "", $line);
				}
			}
		}

		if (isset($this->info['width']) && isset($this->info['height'])) {
			// fix width/height based on orientation
			if (($this->info['orientation'] ?? 0) > 0) {
				// https://www.impulseadventure.com/photo/exif-orientation.html
				$_w = $this->info['width'];
				$_h = $this->info['height'];
				$_islandscape = $this->info['orientation'] % 2;
				if ($_w > $_h && $_islandscape === 0) {
					// image is encoded horizontal, but is viewed vertical, swap sizes
					$this->info['width'] = $_h;
					$this->info['height'] = $_w;
				}
				if ($_w < $_h && $_islandscape === 1) {
					// image is encoded vertical, but is viewed horizontal, swap sizes
					$this->info['width'] = $_h;
					$this->info['height'] = $_w;
				}
			}
			$this->info['ratio'] = $this->info['width'] / $this->info['height'];
		}

		// set position form lat/long
		if (!isset($this->info['position']) && isset($this->info['latitude']) && isset($this->info['longitude'])) {
			$this->info['position'] = sprintf("%s %s", $this->info['latitude'], $this->info['longitude']);
		}

		return $this->info;
	}

	/**
	 * Remove null bytese from $vaule
	 *
	 * if $value is an array, remove from each values in array, recursively
	 *
	 * @param mixed $value
	 * @return mixed
	 */
	private function removenullbytes(mixed $value) : mixed
	{
		if (is_array($value)) {
			foreach ($value as $k => $v) {
				$value[$k] = $this->removenullbytes($v);
			}
		}
		if (is_string($value)) {
			$value = str_replace("\0", "", $value);
		}
		return $value;
	}

	/**
	 * Writeback metadata into file or sidecar
	 *
	 * @param PhotoData $photo Metadata record
	 */
	public function put(array $photo) : void
	{
		if ($this->settings->get('app', 'writeback') == self::WRITEBACK_NONE) {
			return;
		}

		$mime = $photo['format'];
		list($mime_maj, $mime_min) = explode("/", $mime);

		// let's get rid of null bytes
		$photo = $this->removenullbytes($photo);

		// write metadata by file type
		// this is "image" and "everything else".
		// images get tag embedded, everything else gets a sidecar xmp file
		$writeback = $this->settings->get('app', 'writeback') == self::WRITEBACK_DEFAULT;
		$writeback = $writeback && $mime_maj == "image";
		$writeback = $writeback && $mime_min !== "svg+xml"; // exiftool don't support svg
		if ($writeback) {
			$this->writeDriver("ExifTool", $photo);
		} else {
			$this->writeDriver("XMPSideCar", $photo);
		}
	}
}
