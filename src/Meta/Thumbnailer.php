<?php
/*
	This file is autogenerated by 'dev:setup-thumbnailer'
	from 'app/thumbnailer.yml'
*/

namespace Pholar\Meta;

class Thumbnailer
{

	/** @var array<string,array<string,string>> */
	public static $meta = array (
		'*/*' =>
		array (
			'fallback' => 'img/application-x-generic.png',
		),
		'image/*' =>
		array (
			'func' => 'image',
			'fallback' => 'img/image-x-generic.png',
		),
		'image/x-eps' =>
		array (
			'func' => 'ps',
		),
		'video/*' =>
		array (
			'func' => 'video',
			'fallback' => 'img/video-x-generic.png',
		),
		'audio/*' =>
		array (
			'fallback' => 'img/audio-x-generic.png',
		),
		'text/*' =>
		array (
			'func' => 'text',
		),
		'application/gpx+xml' =>
		array (
			'func' => 'gpx',
		),
		'*/pdf' =>
		array (
			'func' => 'ps',
		),
		'*/ps' =>
		array (
			'func' => 'ps',
		),
		'*/*document*' =>
		array (
			'fallback' => 'img/x-office-document.png',
			'func' => 'office',
		),
		'text/rtf' =>
		array (
			'fallback' => 'img/x-office-document.png',
			'func' => 'office',
		),
	);
}
