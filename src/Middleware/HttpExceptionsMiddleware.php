<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as Handler;
use Psr\Http\Message\ResponseInterface;

use Pholar\Exceptions\HttpExceptionInterface;

/**
 * This middleware catches only
 * \Pholar\Exceptions\HttpExceptionInterface,
 * which are exceptions who knows how create a response
 *
 * This is used atm only to catch \Pholar\Exception\HttpRedirect
 * which creates a redirect response.
 *
 * That exception is used only in \Pholar\Controllers\ShareController
 * to redirect to share login view, which is different from generic login.
 *
 * We then trow HttpRedirect in "ShareController::setup()", which is catch by this
 * middleware, which redirects the user to "ShareController::login()"
 *
 * There is no Slim defined Exception for redirect and we can't change Response in
 * '::setup()', because it can't return earlier, so we raise a custom exception.
 */
class HttpExceptionsMiddleware
{
	public function __invoke(Request $request, Handler $handler) : ResponseInterface
	{
		try {
			return $handler->handle($request);
		} catch (HttpExceptionInterface  $e) {
			return $e->makeResponse();
		}
	}
}
