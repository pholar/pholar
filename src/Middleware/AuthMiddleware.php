<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as Handler;
use Psr\Http\Message\ResponseInterface;

use Slim\Interfaces\RouteParserInterface;
use Slim\Psr7\Response;

use Pholar\UserMgr;

class AuthMiddleware
{
	/** @var RouteParserInterface */
	private $router;

	/** @var UserMgr */
	private $usermgr;

	public function __construct(
		RouteParserInterface $router,
		UserMgr $usermgr
	) {
		$this->router = $router;
		$this->usermgr = $usermgr;
	}

	public function __invoke(Request $request, Handler $handler): ResponseInterface
	{
		$user = $this->usermgr->getForSession();

		if (is_null($user)) {
			$uri = $request->getUri();
			$requested_relative = $uri->getPath();
			$requested_relative .= "?" . $uri->getQuery();

			$redirect = $this->router->urlFor(
				"login",
				[],
				['redirect' => $requested_relative]
			);
			$response = new Response();
			return $response->withHeader('Location', $redirect)
							->withStatus(302);
		}

		return $handler->handle($request);
	}
}
