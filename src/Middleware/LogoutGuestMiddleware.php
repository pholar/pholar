<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as Handler;
use Psr\Http\Message\ResponseInterface;

use Pholar\UserMgr;

class LogoutGuestMiddleware
{
	/** @var UserMgr */
	private $usermgr;

	public function __construct(UserMgr $usermgr)
	{
		$this->usermgr = $usermgr;
	}

	public function __invoke(Request $request, Handler $handler): ResponseInterface
	{
		$this->usermgr->logoutGuest();
		return $handler->handle($request);
	}
}
