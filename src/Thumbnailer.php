<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar;

use \Imagick;

use Psr\Log\LoggerInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Pholar\Settings;
use Pholar\Utils;

class Thumbnailer
{
	/**
	 * @var Settings
	 */
	protected $settings;

	/**
	 * @var LoggerInterface
	 */
	protected $logger;
	
	public function __construct(Settings $settings, LoggerInterface $logger)
	{
		$this->settings = $settings;
		$this->logger = $logger;
	}

	/**
	 * Create a thumbnail for a file
	 *
	 * Different libraries/external programs are used to create a thumb image
	 * for the source file. At minimum, ImageMagick extension is required.
	 *
	 * If thumbnail creation fails or source file is not supported, fallback
	 * generic icons are used instead.
	 *
	 * If source file is smaller than requested size, thumbnail will not be
	 * upscaled.
	 *
	 * @param string $sourcefile    Path to source image file
	 * @param string $mime          File mime type
	 * @param int    $size          Thumb max size. Ratio will be preserved.
	 * @param string $thumbfile     Path to output thumb file
	 * @param int	 $page			Page number 1-based. default 1
	 *
	 * @return Imagick  The thumbnail image
	 */
	public function make(string $sourcefile, string $mime, int $size, string $thumbfile, int $page = 1) : Imagick
	{
		
		//[$mimemaj, $mimemin] = explode("/", $mime);

		$opts = [
			'func' => null,
			'fallback' => 'img/application-x-generic.png',
		];
		foreach (Meta\Thumbnailer::$meta as $pattern => $data) {
			if (fnmatch($pattern, $mime)) {
				$opts = array_merge($opts, $data);
			}
		}

		$function = [$this, $opts['func']];
		$fallback = $this->settings->get('basedir') . "/public/" . $opts['fallback'];

		
		if ($mime == "this will never match but Stand doesn't know and will happly mark methods as used somewere") {
			$this->video("", 0, "");
			$this->text("", 0, "");
			$this->office("", 0, "", 0);
			$this->gpx("", 0, "");
		}

		try {
			if (!is_null($opts['func'])) {
				return call_user_func_array($function, [$sourcefile, $size, $thumbfile, $page]);
			} else {
				return $this->image($fallback, $size, $thumbfile);
			}
		} catch (\Exception $e) { // TODO: catch only specific Imagick exceptions
			$this->logger->warning(
				"Thumbnailer::make error: $e. Retrying",
				[$sourcefile, $mime, $size, $thumbfile]
			);
			// cleanup after Imagick failure
			foreach (glob("/tmp/magick-*") as $f) {
				unlink($f);
			}
			return $this->image($fallback, $size, $thumbfile);
		}
	}


	/**
	 * Create a thumbnail from an image file
	 *
	 * @param string   $sourcefile    Path to source image file
	 * @param int      $size          Thumb max size. Ratio will be preserved.
	 * @param string   $thumbfile     Path to output thumb file
	 * @param int      $page          Page number 1-based. default 1
	 * @param ?int[]   $crop          Crop options. Optional. Array is [w,h,x,y,gravity]. Default null
	 * @param bool     $checkerboard  Fill alpha with checkerboard pattern. Default true
	 *
	 * @return Imagick  The thumbnail image
	 */
	private function image(string $sourcefile, int $size, string $thumbfile, int $page = 1, array $crop = null, bool $checkerboard = true) : Imagick
	{
		$quality = 85; // TODO: config
		
		$image = new \Imagick($sourcefile);
		if (!$checkerboard) {
			$image->setImageBackgroundColor("#202b38");
			$image = $image->mergeImageLayers(\Imagick::LAYERMETHOD_FLATTEN);
		}

		if (!is_null($crop)) {
			if (!isset($crop[4])) {
				$crop[4] = 'northwest';
			}
			list($iw, $ih) = [$image->getImageWidth(), $image->getImageHeight()];
			switch ($crop[4]) {
				case 'nortwest': // normal case
					$image->cropImage($crop[0], $crop[1], $crop[2], $crop[3]);
					break;
				case 'southwest': // ps
					$image->cropImage($crop[0], $crop[1], $crop[2], $ih - $crop[1] - $crop[3]);
					break;
			}
		}

		$imgsize = max($image->getImageWidth(), $image->getImageHeight());
		$size = min($size, $imgsize); //don't scale up images smaller than size
		$image->thumbnailImage($size, $size, true);
		$orientation = $image->getImageOrientation();
		switch ($orientation) {
			case Imagick::ORIENTATION_BOTTOMRIGHT:
				$image->rotateImage("#000", 180);
				// rotate 180 degrees
				break;
			case Imagick::ORIENTATION_RIGHTTOP:
				$image->rotateImage("#000", 90);
				// rotate 90 degrees CW
				break;
			case Imagick::ORIENTATION_LEFTBOTTOM:
				$image->rotateImage("#000", -90);
				// rotate 90 degrees CCW
				break;
		}
		// Now that it's auto-rotated, make sure the EXIF data is correct in case the EXIF gets saved with the image!
		$image->setImageOrientation(Imagick::ORIENTATION_TOPLEFT);

		if ($image->getImageAlphaChannel()) {
			$img = new \Imagick();
			$img->newPseudoImage($image->getImageWidth(), $image->getImageHeight(), "pattern:checkerboard");
			$img->setImageColorspace($image->getImageColorspace());
			$img->compositeImage($image, \Imagick::COMPOSITE_DEFAULT, 0, 0);
			$img->setImageFormat('jpeg');
			$img->setImageCompression(\Imagick::COMPRESSION_JPEG);
			$img->setImageCompressionQuality($quality);
		} else {
			$img = $image;
		}
		$thumbdir = dirname($thumbfile);
		if (!file_exists($thumbdir)) {
			if (!mkdir($thumbdir, 0777, true)) {
				throw new \Exception("Could not create thumbnail folder '$thumbdir'.");
			}
		}
		if (file_put_contents($thumbfile, $img) === false) {
			throw new \Exception("Could not save thumbnail '$thumbfile'.");
		}
		
		return $img;
	}

	/**
	 * Create a thumbnail from a video file
	 *
	 * Get frame at 1s in video file using ffmpeg
	 *
	 * @param string $sourcefile    Path to source image file
	 * @param int    $size          Thumb max size. Ratio will be preserved.
	 * @param string $thumbfile     Path to output thumb file
	 * @param int    $page			Page number 1-based. default 1
	 *
	 * @return Imagick  The thumbnail image
	 */
	private function video(string $sourcefile, int $size, string $thumbfile, int $page = 1) : Imagick
	{
		$tmpfile = tempnam("/tmp", "fmthumb");
		unlink($tmpfile);
		$tmpfile = $tmpfile . ".png";
		
		$cmd = ['ffmpeg', '-i', $sourcefile , '-ss', '00:00:01.000', '-vframes', '1', $tmpfile];
		$cmd = implode(" ", array_map('escapeshellarg', $cmd));

		$this->logger->info("Thumbnailer video", [$cmd]);

		$process = new Process($cmd);
		$process->run();

		// executes after the command finishes
		if (!$process->isSuccessful()) {
			unlink($tmpfile);
			throw new ProcessFailedException($process);
		}

		try {
			$image = $this->image($tmpfile, $size, $thumbfile, $page);
		} catch (\Exception $e) {
			throw $e;
		} finally {
			unlink($tmpfile);
		}
		return $image;
	}

	/**
	 * Create a thumbnail from pdf/ps files
	 *
	 * Get first page in document using GhostScript
	 *
	 * @param string $sourcefile    Path to source image file
	 * @param int    $size          Thumb max size. Ratio will be preserved.
	 * @param string $thumbfile     Path to output thumb file
	 * @param int    $page			Page number 1-based. default 1
	 *
	 * @return Imagick  The thumbnail image
	 */
	private function ps(string $sourcefile, int $size, string $thumbfile, int $page = 1) : Imagick
	{
		$RES = 150;

		if (Utils::endsWith($sourcefile, ".eps")) {
			// get cropbox
			$cmd = ['gs', '-dNOPAUSE', '-dBATCH', '-dFirstPage='.$page, '-dLastPage='.$page, '-sDEVICE=bbox', '-r'.$RES, $sourcefile];
			$this->logger->info("Thumbnailer pdf get bbox", $cmd);
			// vecchie versioni. preferirei usare l'array, ma tant'è
			#$cmd = implode(" ", array_map('escapeshellarg', $cmd));

			$process = new Process($cmd);
			$process->run();
			if (!$process->isSuccessful()) {
				throw new ProcessFailedException($process);
			}
			$crop = null;
			if (preg_match_all("/%%BoundingBox: (\d+) (\d+) (\d+) (\d+)/", $process->getErrorOutput(), $m) > 0) {
				array_shift($m);
				list($x1,$y1,$x2,$y2) = array_map(function ($e) use ($RES) {
					return ($e[0] / 72) * $RES;
				}, $m);
				$crop = [$x2-$x1, $y2-$y1, $x1, $y1, 'southwest'];
				$this->logger->info("Thumbnailer pdf bbox", $crop);
			}
		} else {
			$crop = null;
		}

		// convert
		$tmpfile = tempnam("/tmp", "fmthumb");

		$cmd = ['gs', '-dNOPAUSE', '-dBATCH', '-dFirstPage='.$page, '-dLastPage='.$page,
				'-dUseTrimBox', '-dUseCropBox',
				'-sDEVICE=jpeg', '-r'.$RES, '-sOutputFile='.$tmpfile,
				$sourcefile];
		#$cmd = implode(" ", array_map('escapeshellarg', $cmd));

		$this->logger->info("Thumbnailer pdf", $cmd);

		$process = new Process($cmd);
		$process->run();
		if (!$process->isSuccessful()) {
			unlink($tmpfile);
			throw new ProcessFailedException($process);
		}

		try {
			$image = $this->image($tmpfile, $size, $thumbfile, $page, $crop);
		} catch (\Exception $e) {
			throw $e;
		} finally {
			unlink($tmpfile);
		}
		return $image;
	}

	/**
	 * Create a thumbnail from text files
	 *
	 * Write file text content in black on white image using GD functions
	 *
	 * @param string $sourcefile    Path to source image file
	 * @param int    $size          Thumb max size. Ratio will be preserved.
	 * @param string $thumbfile     Path to output thumb file
	 * @param int    $page			Page number 1-based. default 1
	 *
	 * @return Imagick  The thumbnail image
	 */
	private function text(string $sourcefile, int $size, string $thumbfile, int $page = 1) : Imagick
	{
		$ext = pathinfo($sourcefile, PATHINFO_EXTENSION);
		$tmpfile = "/tmp/" . str_replace(".$ext", ".jpg", basename($sourcefile));
		
		$handle = fopen($sourcefile, "r");
		if ($handle) {
			$img = imagecreate($size, $size);
			$bg = imagecolorallocate($img, 255, 255, 255);
			$fg = imagecolorallocate($img, 0, 0, 0);
			imagefilledrectangle($img, 0, 0, $size, $size, $bg);

			$y = 5;
			while (($line = fgets($handle)) !== false) {
				imagestring($img, 1, 5, $y, rtrim($line), $fg);
				$y = $y + 8;
				if ($y > $size) {
					break;
				}
			}
			fclose($handle);

			imagejpeg($img, $tmpfile);
			imagedestroy($img);
		} else {
			// error opening the file.
		}


		try {
			$image = $this->image($tmpfile, $size, $thumbfile, $page);
		} catch (\Exception $e) {
			throw $e;
		} finally {
			unlink($tmpfile);
		}
		return $image;
	}

	/**
	 * Create a thumbnail from office/libreoffice files
	 *
	 * Use soffice to convert document to pdf, then pass to `this::ps` to convert to png
	 *
	 * @param string $sourcefile    Path to source image file
	 * @param int    $size          Thumb max size. Ratio will be preserved.
	 * @param string $thumbfile     Path to output thumb file
	 * @param int    $page			Page number 1-based. default 1
	 *
	 * @return Imagick  The thumbnail image
	 */
	private function office(string $sourcefile, int $size, string $thumbfile, int $page) : Imagick
	{
		$ext = pathinfo($sourcefile, PATHINFO_EXTENSION);
		$tmpfile = "/tmp/" . str_replace(".$ext", ".pdf", basename($sourcefile));
		$cmd = ['soffice', '--headless', '--convert-to', 'pdf', '--outdir', '/tmp', $sourcefile];

		$this->logger->info("Thumbnailer ", [$cmd]);

		$process = new Process($cmd);
		$process->run();
		if (!$process->isSuccessful()) {
			unlink($tmpfile);
			throw new ProcessFailedException($process);
		}
	
		try {
			$image = $this->ps($tmpfile, $size, $thumbfile, $page);
		} catch (\Exception $e) {
			throw $e;
		} finally {
			#unlink($tmpfile);
		}
		return $image;
	}

	/**
	 * Create a thumbnail from gpx track files
	 *
	 * @param string $sourcefile    Path to source image file
	 * @param int    $size          Thumb max size. Ratio will be preserved.
	 * @param string $thumbfile     Path to output thumb file
	 * @param int    $page			Page number 1-based. default 1
	 *
	 * @return Imagick  The thumbnail image
	 */
	private function gpx(string $sourcefile, int $size, string $thumbfile, int $page = 1) : Imagick
	{
		// This function doesnt works
		throw new \Exception();

		
		/*
		function lon2x($lon)
		{
			return deg2rad($lon) * 6378137.0;
		}
		function lat2y($lat)
		{
			return log(tan(M_PI_4 + deg2rad($lat) / 2.0)) * 6378137.0;
		}
		function x2lon($x)
		{
			return rad2deg($x / 6378137.0);
		}
		function y2lat($y)
		{
			return rad2deg(2.0 * atan(exp($y / 6378137.0)) - M_PI_2);
		}

		function imagelinethick($image, $x1, $y1, $x2, $y2, $color, $thick = 1)
		{
			// this way it works well only for orthogonal lines
			//imagesetthickness($image, $thick);
			//return imageline($image, $x1, $y1, $x2, $y2, $color);
			//
			if ($thick == 1) {
				return imageline($image, $x1, $y1, $x2, $y2, $color);
			}
			$t = $thick / 2 - 0.5;
			if ($x1 == $x2 || $y1 == $y2) {
				return imagefilledrectangle($image, round(min($x1, $x2) - $t), round(min($y1, $y2) - $t), round(max($x1, $x2) + $t), round(max($y1, $y2) + $t), $color);
			}
			$k = ($y2 - $y1) / ($x2 - $x1); //y = kx + q
			$a = $t / sqrt(1 + pow($k, 2));
			$points = array(
				round($x1 - (1+$k)*$a), round($y1 + (1-$k)*$a),
				round($x1 - (1-$k)*$a), round($y1 - (1+$k)*$a),
				round($x2 + (1+$k)*$a), round($y2 - (1-$k)*$a),
				round($x2 + (1-$k)*$a), round($y2 + (1+$k)*$a),
			);
			imagefilledpolygon($image, $points, 4, $color);
			return imagepolygon($image, $points, 4, $color);
		}

		$tmpfile = tempnam("/tmp", "fmthumb");
		$xmlstr = file_get_contents($sourcefile);
		$gpx = new \SimpleXMLElement($xmlstr);

		$latmin = PHP_INT_MAX;
		$lonmin = PHP_INT_MAX;
		$latmax = -PHP_INT_MAX;
		$lonmax = -PHP_INT_MAX;

		foreach ($gpx->xpath('//trkpt') as $trkpt) {
			$lat = lat2y(-floatval($trkpt['lat']));
			$lon = lon2x(floatval($trkpt['lon']));
			if ($lat < $latmin) {
				$latmin = $lat;
			}
			if ($lat > $latmax) {
				$latmax = $lat;
			}
			if ($lon < $lonmin) {
				$lonmin = $lon;
			}
			if ($lon > $lonmax) {
				$lonmax = $lon;
			}
		}

		$latrange = max($latmax, $latmin) - min($latmax, $latmin);
		$lonrange = max($lonmax, $lonmin) - min($lonmax, $lonmin);
		$r = $lonrange / $latrange;

		if ($r == 1) {
			$width = $height = $size;
		}
		if ($r < 1) {
			$height = $size;
			$width = ceil($height * $r);
		}
		if ($r > 1) {
			$width = $size;
			$height = ceil($width / $r);
		}

		$border = 10;
		$width = $width + ($border * 2);
		$height = $height + ($border * 2);

		$background = [255,255,255];
		$foreground = [0,0,0];

		$image = imagecreate($width, $height) or die("Cannot Initialize new GD image stream");
		$background_color = imagecolorallocate($image, $background[0], $background[1], $background[2]);
		$foreground_color = imagecolorallocate($image, $foreground[0], $foreground[1], $foreground[2]);

		$x1 = $y1 = -1;

		imagefill($image, 0, 0, $background_color);

		foreach ($gpx->xpath('//trkpt') as $trkpt) {
			$lat = lat2y(-floatval($trkpt['lat']));
			$lon = lon2x(floatval($trkpt['lon']));

			$x2 = $border + (($lon - $lonmin) / $lonrange) * ($width - $border * 2);
			$y2 = $border + (($lat - $latmin) / $latrange) * ($height - $border * 2);
			if ($x1 != -1 && $x2 != -1) {
				imagelinethick(
					$image,
					$x1,
					$y1,
					$x2,
					$y2,
					$foreground_color,
					3
				);
			}
			$x1 = $x2;
			$y1 = $y2;
		}

		imagejpeg($image, $tmpfile);
		imagedestroy($image);

		try {
			$image = $this->image($tmpfile, $size, $thumbfile, $page);
		} catch (\Exception $e) {
			throw $e;
		} finally {
			unlink($tmpfile);
		}
		return $image;
		*/
	}
}
