<?php
/*
  Copyright (c) 2020-2021 Fabio Comuni

  This file is part of Pholar.

  Pholar is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  Pholar is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar;

use Pholar\Utils;

class SystemSettings
{
	/** @var array<string,mixed> */
	protected $settings;

	public function __construct()
	{
		$this->resetDefaults();
		$this->loadUserFile();
		$this->validate();
	}

	public function getEnv() : string
	{
		return $_ENV['PHOLAR_ENV'] ?? getenv('PHOLAR_ENV') ?: 'default';
	}

	protected function resetDefaults() : void
	{
		$this->settings = include(dirname(__DIR__) . "/app/conf/settings.php");
	}

	protected function getUserEnvFile() : string
	{
		$env = $this->getEnv();
		$env = str_replace(["..", ".", "/", "\\"], "", $env);
		return $this->get('paths', 'etc') . "/$env.settings.php";
	}

	protected function loadUserFile() : void
	{
		$usersettingsfile = $this->getUserEnvFile();
		if (file_exists($usersettingsfile)) {
			$usersettings = include($usersettingsfile);
			foreach ($usersettings as $f => $vals) {
				if (is_array($vals)) {
					foreach ($vals as $k => $v) {
						$this->put($f, $k, $v);
					}
				} else {
					$this->put($f, $vals);
				}
			}
		}
	}

	private function abspath(string $path) : string
	{
		if (!Utils::startsWith($path, '/')) {
			$path = realpath($this->get('basedir') . "/" . $path);
		}
		return $path;
	}

	protected function validate() : void
	{
		// storagedir
		$storagedir = $this->get('app', 'storage');
		$storagedir = $this->abspath($storagedir);
		$storagedir = rtrim($storagedir, "/");
		if (!is_dir($storagedir)) {
			die("Config error:<br>\nstoragedir '{$storagedir}' is not a directory");
		}
		$this->put('app', 'storage', $storagedir);

		// db
		$db = $this->get('db');
		$dsn = parse_url($db);
		switch ($dsn['scheme']) {
			case "sqlite":
				$dsn['path'] = $this->abspath($dsn['path']);
				$db = "sqlite:" . $dsn['path'];
				$this->put('db', $db);
				break;
			case "mysql":
				$dsn['dbname'] = str_replace("/", "", $dsn['path']);
				$dsn['port'] = $dsn['port'] ?? "3306";
				break;
			default:
				die("Config error:<br>\nDatabase '{$dsn['scheme']}' is not supported.");
		}
	
		$this->put('dsn', $dsn);

		// cache path
		$cachepath = $this->get('paths', 'cache');
		$cachepath = $this->abspath($cachepath);
		if (!Utils::endsWith($cachepath, "/")) {
			$cachepath = $cachepath . "/";
		}
		$this->put('paths', 'cache', $cachepath);
		
		// latsupdate file
		$lastupdatefile = $this->get('lastupdatefile');
		$lastupdatefile = $this->abspath($lastupdatefile);
		$this->put('lastupdatefile', $lastupdatefile);

		// writeback
		$writeback = $this->get('app', 'writeback');
		if (!in_array($writeback, ['default', 'xmp', 'none'])) {
			die("Config error:<br>\n'{$writeback}' is not a valid value for writeback");
		}
	}

	public function get(string $f, string $k = null) : mixed
	{
		$ff = $this->settings[$f] ?? null;
		if (is_null($k)) {
			return $ff;
		}
		return $ff[$k] ?? null;
	}

	public function put(string $f, mixed $k, mixed $v = null) : void
	{
		if (!isset($this->settings[$f])) {
			$this->settings[$f] = [];
		}
		if (is_null($v)) {
			$this->settings[$f] = $k;
		} else {
			if (!is_string($k)) {
				throw new \InvalidArgumentException('$k parameter must be string if $v is defined');
			}
			$this->settings[$f][$k] = $v;
		}
	}
}
