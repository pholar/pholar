<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar;

use Envms\FluentPDO\Literal;
use Psr\Log\LoggerInterface;

use Envms\FluentPDO\Query;
use Envms\FluentPDO\Queries\Select;

use Pholar\Settings;
use Pholar\Utils;
use Pholar\SQL\SQLDriver;
use Pholar\Meta\Meta;

class FaceMgr
{
	/** @var Settings */
	protected $settings;

	/** @var SQLDriver */
	protected $sqldriver;

	/** @var LoggerInterface */
	protected $logger;

	/** @var Query */
	protected $db;

	/** @var TagMgr */
	protected $tagmgr;

	/** @var CollectionMgr */
	protected $collectionmgr;

	/** @var Meta */
	protected $meta;

	public function __construct(
		Settings $settings,
		SQLDriver $sqldriver,
		LoggerInterface $logger,
		Query $db,
		TagMgr $tagmgr,
		CollectionMgr $collectionmgr,
		Meta $meta
	) {
		$this->settings = $settings;
		$this->sqldriver = $sqldriver;
		$this->logger =$logger;
		$this->db = $db;
		$this->tagmgr = $tagmgr;
		$this->collectionmgr = $collectionmgr;
		$this->meta = $meta;
	}

	/**
	 * Parse data from db
	 *
	 * @param array<string,?string> $face Face data from db
	 * @return array<string,mixed> Face data
	 */
	public function fromDb(array $face) : array
	{
		$face_id = $face['face_id'];
		if (!is_null($face_id)) {
			$face_id = (int) $face_id;
		}
		
		return [
			'id' => (int) $face['id'],
			'photo_id' => (int) $face['photo_id'],
			'face_id' => $face_id,
			'name' => $face['name'],
			'rect' => json_decode($face['rect'], true),
		];
	}

	/**
	 * Prepare data for db
	 *
	 * @param array<string,mixed> $face
	 * @return array<string,mixed>
	 */
	public function toDb(array $face) : array
	{
		$face['rect'] = json_encode($face['rect']);
		$face['face_id'] = $face['face_id'] ?? new Literal('NULL');
		return $face;
	}


	/**
	 * Get face by id
	 *
	 * @return false|array<string,mixed>
	 */
	public function byId(int $id) : false|array
	{
		$face = $this->db->from('face', $id)->fetch();
		if ($face === false) {
			return false;
		}
		return $this->fromDb($face);
	}

	/**
	 * Get faces for photo id
	 *
	 * @param int $id Photo id
	 * @return Select
	 */
	public function getFacesFor(int $id) : Select
	{
		return $this->db->from('face')->where('photo_id', $id);
	}


	/**
	 * Save face data
	 *
	 * @param array<string,mixed> $face Face data
	 */
	public function save(array &$face) : void
	{
		$id = $face['id'] ?? null;
		if (is_null($id)) {
			$id = $this->db->insertInto('face', $this->toDb($face))->execute();
		} else {
			$dbdata = $this->toDb($face);
			unset($dbdata['id']);
			$this->db->update('face', $dbdata)->where('id', $id)->execute();
		}
		$face['id'] = $id;
	}
	
	/**
	 * Delete face
	 */
	public function delete(int $face_id) : void
	{
		$this->db->deleteFrom('face', $face_id)->execute();
	}


	/**
	 * Get all names found so far, as a Query
	 */
	public function allNames() : Select
	{
		return $this->db->from('face')->where('name != ?', '')->groupBy('face.name');
	}

	/**
	 * find faces for $photo_id in $filename and store results in db
	 *
	 * Any existing face for $photo_id previously found (with 'face_id' not null)
	 * are removed.
	 *
	 * @param int $photo_id Photo id in database
	 * @param string $filename Image file to analyze
	 * @return array<array<string,mixed>> Faces data
	 */
	public function detectFaces(int $photo_id, string $filename) : array
	{
		if ($this->settings->get('facetool', 'enabled') == false) {
			return [];
		}
		$cmd = array_merge(
			$this->settings->get('facetool', 'commandline'),
			[
				"--db", $this->settings->get('basedir') . '/var/encodings.sqlite',
				"-j",
				"-p",
				$filename
			]
		);
		$this->logger->debug("Run {cmd}", ['cmd'=>implode(' ', $cmd)]);
		$r = Utils::runcmd($cmd);
		$faces = json_decode($r, true);

		// cancelliamo le righe associate a 'photo_id' dove 'face_id' è definito (e che quindi arrivano da facetool)
		// per poi ripopolare
		$this->db->deleteFrom('face')->where('face_id IS NOT NULL')->where('photo_id', $photo_id)->execute();
		
		$results = [];
		foreach ($faces as $face) {
			$dbdata = [
				'photo_id' => $photo_id,
				'face_id' => $face['id'],
				'rect' => json_encode($face['rect']),
				'name' => $face['name'] ?? '',
			];
			$face_id = $this->db->insertInto('face', $dbdata)->execute();
			$dbdata['id'] = $face_id;
			$results[] = $dbdata;
		}

		return $results;
	}

	/**
	 * name a face detected by facetool in $filename
	 *
	 * @param array<string,mixed> $face Face data
	 * @param string $filename
	 */
	public function tagFace(array $face, string $filename) : void
	{
		if ($this->settings->get('facetool', 'enabled') == false) {
			return;
		}
		$face_id = $face['face_id'];
		$name = $face['name'];

		$cmd = array_merge(
			$this->settings->get('facetool', 'commandline'),
			[
				"--db", $this->settings->get('basedir') . '/var/encodings.sqlite',
				"--tag=$face_id:$name",
				$filename
			]
		);
		$this->logger->debug("Run {cmd}", ['cmd'=>implode(' ', $cmd)]);
		$r = Utils::runcmd($cmd);
	}
}
