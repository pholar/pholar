<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar;

use Psr\Log\LoggerInterface;

use Envms\FluentPDO\Query;
use Envms\FluentPDO\Literal;
use Envms\FluentPDO\Queries\Select;

use Pholar\UUID;

class ShareMgr
{
	/** @var LoggerInterface */
	protected $logger;

	/** @var Query */
	protected $db;

	public function __construct(
		LoggerInterface $logger,
		Query $db
	) {
		$this->logger = $logger;
		$this->db = $db;
	}

	/**
	 * Create a new share
	 */
	public function create(
		string $username,
		int $collectionid,
		\DateTimeInterface $valid_from = null,
		\DateTimeInterface $valid_until = null,
		bool $can_download = true,
		bool $can_details = true,
		string $password = null
	) : string {
		$suid = UUID::v4();

		$password = is_null($password) ? null : password_hash($password, PASSWORD_DEFAULT);

		$data = $this->toDb([
			'shareuid' => $suid,
			'user' => $username,
			'collection_id' => $collectionid,
			'password' => $password,
			'valid_from' => $valid_from,
			'valid_until' => $valid_until,
			'can_download' => $can_download,
			'can_details' => $can_details
		]);

		$this->db->insertInto('share', $data)->execute();

		return $suid;
	}

	/**
	 * Delete a share
	 */
	public function delete(string $shareuid) : void
	{
		$this->db->deleteFrom('share')
			->where('shareuid', $shareuid)
			->execute();
	}

	/**
	 * Save a share
	 *
	 * @param ShareData $share
	 */
	public function save(array $share) : void
	{
		$shareuid = $share['shareuid'] ?? null;
		if (is_null($shareuid)) {
			throw new \InvalidArgumentException();
		}

		$share = $this->toDb($share);

		$this->db->update('share')
				 ->set($share)
				 ->where('shareuid', $shareuid)
				 ->execute();
	}

	/**
	 * Fromat data for db
	 *
	 * @param ShareData $s
	 * @return ShareData
	 */
	private function toDb(array $s) : array
	{
		$s['password'] = is_null($s['password']) ? new Literal('NULL') : $s['password'];
		$s['valid_from'] = is_null($s['valid_from']) ? new Literal('NULL') : $s['valid_from']->format("Y-m-d");
		$s['valid_until'] = is_null($s['valid_until']) ? new Literal('NULL') : $s['valid_until']->format("Y-m-d");
		$s['can_download'] = $s['can_download'] ? 1 : 0;
		$s['can_details'] = $s['can_details'] ? 1 : 0;
		return $s;
	}

	/**
	 * Format data from db
	 *
	 * @param array<string,string> $s
	 * @return ShareData
	 */
	private function fromDb(array $s) : array
	{
		$data = $s['valid_from'] ?? null;
		if (!is_null($data)) {
			$v = explode(' ', $data)[0];
			$s['valid_from'] = \DateTime::createFromFormat('!Y-m-d', $v);
		}
		
		$data = $s['valid_until'] ?? null;
		if (!is_null($data)) {
			$v = explode(' ', $data)[0];
			$s['valid_until'] = \DateTime::createFromFormat('!Y-m-d', $v);
		}
		$s['can_download'] = $s['can_download'] == '1';
		$s['can_details'] = $s['can_details'] == '1';
		return $s;
	}

	/**
	 * Get a share by UID
	 *
	 * @param string $uid
	 * @return ?ShareData
	 */
	public function byUID(string $uid) : ?array
	{
		$r = $this->db->from('share')
					  ->where('shareuid', $uid)
					  ->fetch();
		if ($r === false) {
			return null;
		}

		return $this->fromDb($r);
	}

	/**
	 * Check share password
	 *
	 * @param ShareData $share
	 * @param ?string $password
	 */
	public function checkPassword(array $share, string $password = null) : bool
	{
		if (is_null($share['password'])) {
			return true;
		}

		return password_verify($password, $share['password']);
	}

	/**
	 * Get shares for collection
	 *
	 * @param int $collectionid
	 */
	public function getForCollection(int $collectionid) : Select
	{
		return $this->db->from('share')->where('collection_id', $collectionid);
	}
}
