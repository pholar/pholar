<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Fields;

use Twig\Environment;

/**
 * Display a decimal number as a fraction
 */
class Fractional extends Numeric
{
	/**
	 * @return array<mixed>
	 */
	public static function filter(string $name, mixed $value) : array
	{
		list($name, $value) = parent::filter($name, $value);
		if (strpos($value, "/")) {
			list($a, $b) = array_map("intval", explode("/", $value));
			$value = $a/$b;
			// TODO: that's wrong.. a fraction is build rounding the original value,
			// 			so it will almost never match
		}

		return [$name, $value];
	}

	public static function display(string $name, mixed $value, Environment $view = null) : string
	{
		$res = "";
		$value = (float) $value;
		if ($value > 1) {
			$res = floor($value) . " ";
			$value = $value - floor($value);
		}

		$frac = intval(1/$value);
		if ($frac > 0) {
			$res .= "1/" . $frac;
		}

		return parent::display($name, $res);
	}

	public static function displayHtml(string $name, mixed $value, Environment $view) : string
	{
		$display = self::display($name, $value, $view);
		return $view->render('fields/fractional.twig', [
			'name' => $name,
			'value' => $value,
			'display' => $display
		]);
	}
}
