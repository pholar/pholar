<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Fields;

use Twig\Environment;

class Rating extends Numeric
{
	public static function display(string $name, mixed $value, Environment $view = null) : string
	{
		$off = 5 - $value;

		$display = str_repeat("★", $value);
		$display .= str_repeat("☆", $off);

		return $display;
	}

	public static function displayHtml(string $name, mixed $value, Environment $view) : string
	{
		$display = self::display($name, $value, $view);
		return $view->render('fields/rating.twig', [
			'name' => $name,
			'value' => $value,
			'display' => $display
		]);
	}
}
