<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Fields;

use Twig\Environment;
use Geocoder\Geocoder;

class Coordinates extends Generic
{
	/**
	 * @return array<mixed>
	 */
	public static function filter(string $name, mixed $value) : array
	{
		$app = \Pholar\App::getApp();
		$geocoder = $app->getContainer()->get(Geocoder::class);

		$first = $geocoder->geocodeQuery(\Geocoder\Query\GeocodeQuery::create($value))->first();
		if (!is_null($first)) {
			$bounds = $first->getBounds();

			$query = [
				"(photo.latitude >= ? AND photo.latitude <= ? AND photo.longitude >= ? AND photo.longitude <= ?)",
				[
					$bounds->getSouth(),
					$bounds->getNorth(),
					$bounds->getWest(),
					$bounds->getEast()
				]
			];
			return $query;
		}


		return [];
	}

	public static function displayHtml(string $name, mixed $value, Environment $view) : string
	{
		if (!is_null($value)) {
			list($lat, $lon) = explode(" ", $value);

			return $view->render('fields/coordinates.twig', [
				'name' => $name,
				'value' => $value,
				'lat' => $lat,
				'lon' => $lon,
			]);
		}
		return "";
	}
}
