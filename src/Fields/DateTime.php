<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Fields;

use Twig\Environment;

class DateTime extends Generic
{
	/**
	 * @return array<mixed>
	 */
	public static function filter(string $name, mixed $value) : array
	{
		return ["$name LIKE ?", "$value%"];
	}
	
	public static function displayHtml(string $name, mixed $value, Environment $view) : string
	{
		$date = array_map(
			function ($e) {
				return is_numeric($e) ? str_pad($e, 2, "0", STR_PAD_LEFT) : $e;
			},
			date_parse($value)
		);

		return $view->render('fields/datetime.twig', [
			'name' => $name,
			'value' => $value,
			'date' => $date
		]);
	}

	public static function store(mixed $value) : mixed
	{
		if ($value === "") {
			return null;
		}
		if (is_string($value)) {
			$value = strtotime($value);
		}

		# TODO: mysql doesn't like dates with timezone, how to fix this?
		# TODO: all date time should be saved in UTC. but many meta don't come with TZ...
		$value =  date("Y-m-d H:i:s", $value);
		return $value;
	}
}
