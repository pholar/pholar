<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Fields;

use Twig\Environment;

class ColorMode extends Numeric
{
	/**
	 * @return array<mixed>
	 */
	public static function filter(string $name, mixed $value) : array
	{
		$value = strtolower($value);
		if (in_array($value, ['c','color','colour'])) {
			$value = 3;
		} elseif (in_array($value, [ 'bw','gray', 'grey', 'grayscale', 'greyscale'])) {
			$value = 1;
		}
			
		return parent::filter($name, $value);
	}

	public static function display(string $name, mixed $value, Environment $view = null) : string
	{
		$value = $value == 3 ? "color" : "grayscale";
		return parent::display($name, $value);
	}

	public static function displayHtml(string $name, mixed $value, Environment $view) : string
	{
		return parent::displayHtml($name, self::display($name, $value, $view), $view);
	}
}
