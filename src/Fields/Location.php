<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Fields;

use Twig\Environment;
use Pholar\Utils;

class Location extends Generic
{
	/**
	 * @return array<mixed>
	 */
	public static function filter(string $name, mixed $value) : array
	{
		if (Utils::startsWith($value, "/")) {
			if (Utils::endsWith($value, "/")) {
				return ["$name = ?", substr($value, 0, -1)];
			}
			return ["$name LIKE ?", "$value%"];
		} else {
			return ["$name LIKE ?", "%$value%"];
		}
	}
	
	public static function displayHtml(string $name, mixed $value, Environment $view) : string
	{
		$path = explode("/", trim($value, "/"));

		return $view->render("fields/location.twig", [
			'name' => $name,
			'value' => $value,
			'path' => $path
		]);
	}
}
