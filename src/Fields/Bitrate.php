<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Fields;

use Twig\Environment;
use Pholar\Exceptions\SearchException;

/**
 * display a bitrate value
 */
class Bitrate extends Numeric
{
	/**
	 * @return array<mixed>
	 */
	public static function filter(string $name, mixed $value) : array
	{
		$dims = [
			'', 'k','m', 'g',
			'b', 'kb', 'mb', 'gb',
			'bps', 'kbps', 'mbps', 'gbps',
		];
		$match = [];
		if (preg_match("|([<>=!]*)([0-9.]+)([a-zA-Z]+)|", $value, $match)) {
			list($value, $op, $v, $d) = $match;
			$v = (int) $v;
			$d = strtolower($d);
			$dfact = array_search($d, $dims);
			if ($dfact === false) {
				throw new SearchException("'$d' not valid dimension for '$name'");
			}
			$dfact = $dfact % 4;
			$v = (1000 ** ($dfact+1)) * $v;
			$value = $op . $v;
		}
		return parent::filter($name, $value);
	}

	public static function display(string $name, mixed $value, Environment $view = null) : string
	{
		$precision = 0;
		$units = array('bps', 'Kbps', 'Mbps', 'Gbps', 'Tbps');

		$bits = max($value, 0);
		$pow = floor(($bits ? log($bits) : 0) / log(1000));
		$pow = min($pow, count($units) - 1);

		$bits /= (1 << (10 * $pow));

		return $view->render('fields/bytes.twig', [
			'name' => $name,
			'value' => $value,
			'display' => round($bits, $precision),
			'unit' => $units[$pow]
		]);
	}

	public static function displayHtml(string $name, mixed $value, Environment $view) : string
	{
		return self::display($name, $value, $view);
	}
}
