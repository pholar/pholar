<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Fields;

use Twig\Environment;

class MultilineText extends Text
{
	public static function display(string $name, mixed $value, Environment $view = null) : string
	{
		$value = str_replace("\r\n", "\n", $value);
		$value = str_replace("\r", "\n", $value);
		$value = str_replace("\n", "<br>\n", $value);
		return $value;
	}

	public static function displayHtml(string $name, mixed $value, Environment $view) : string
	{
		return self::display($name, $value);
	}
}
