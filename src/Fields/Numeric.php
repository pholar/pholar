<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Fields;

use Pholar\Utils;

class Numeric extends Generic
{
	/**
	 * @return array<mixed>
	 */
	public static function filter(string $name, mixed $value) : array
	{
		foreach (['<', '>', '<=', '>=', '!='] as $op) {
			if (Utils::startsWith($value, $op)) {
				$name = "$name $op ?";
				$value = trim(str_replace($op, "", $value));
				break;
			}
		}
		return [$name, $value];
	}

	public static function store(mixed $value) : mixed
	{
		if (!is_numeric($value)) {
			return null;
		}
		return $value;
	}
}
