<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Fields;

use Twig\Environment;

class Ratio extends Numeric
{
	/**
	 * @return array<mixed>
	 */
	public static function filter(string $name, mixed $value) : array
	{
		if (strpos($value, ":")) {
			list($a, $b) = array_map("intval", explode(":", $value, 2));
			$value = $a / $b;
		}
		$value = strtolower($value);
		if (in_array($value, ['h', 'horizontal', 'l',  'landscape'])) {
			return ["ratio > 1"];
		} elseif (in_array($value, ['v', 'vertical', 'p', 'portrait'])) {
			return ["ratio < 1"];
		} elseif (in_array($value, ['s', 'square'])) {
			return ["ratio = 1"];
		} else {
			return parent::filter($name, $value);
		}
	}

	public static function display(string $name, mixed $value, Environment $view = null) : string
	{
		if ($value > 1) {
			$display = "landscape";
		} elseif ($value < 1) {
			$display = "portrait";
		} else {
			$display = "square";
		}
	
		return $display;
	}

	public static function displayHtml(string $name, mixed $value, Environment $view) : string
	{
		$precision = 3;
		$display = self::display($name, $value, $view);
		$value = round($value, $precision);
		return $view->render('fields/ratio.twig', [
			'name' => $name,
			'value' => $value,
			'display' => $display
		]);
	}
}
