<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Exceptions;

/**
 * This is raised by Batch action executors in
 * \Pholar\Controllers\ActionController
 *
 * Looks like an http error but it isn't really.
 * (we also don't use Slim\Exception\HttpException there because
 * executors don't know nothing about Request)
 *
 * It's converted to a custom json response or to a flash message
 */
class ActionException extends \Exception
{
	/** @var int */
	protected $httpcode;

	public function __construct(int $httpcode, string $message)
	{
		$this->httpcode = $httpcode;
		parent::__construct($message);
	}

	public function getHTTPCode() : int
	{
		return $this->httpcode;
	}
}
