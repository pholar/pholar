<?php
/*
	Copyright (c) 2021 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Exceptions\Renderer;

use Throwable;

use Slim\Interfaces\ErrorRendererInterface;
use Slim\Exception\HttpException;

use Slim\Views\Twig;

use Pholar\Settings;
use Pholar\Utils\Stats\StatsBag;

/**
 * Render exceptions with a template
 */
class HtmlRenderer implements ErrorRendererInterface
{
	/** @var Settings */
	protected $settings;

	/** @var Twig */
	protected $view;

	public function __construct(Settings $settings, Twig $view)
	{
		$this->settings = $settings;
		$this->view = $view;
	}

	public function __invoke(Throwable $exception, bool $displayErrorDetails): string
	{
		$statusCode = 500;
		$title = "Server Error";
		$description = "";

		if ($exception instanceof HttpException) {
			$statusCode = $exception->getCode();
			$title = str_replace("$statusCode ", "", $exception->getTitle());
			$description = $exception->getMessage();
		}

		if (!($exception instanceof HttpException)
			&& $exception instanceof Throwable
			&& $displayErrorDetails
		) {
			$description = $exception->getMessage();
		}

		$s = "";
		if ($this->settings->get('display', 'stats_panel')) {
			$stats = StatsBag::instance();
			$s = $stats->render();
		}
		
		return $this->view->fetch("error.twig", [
			'displayErrorDetails' => $displayErrorDetails,
			'statusCode' => $statusCode,
			'title' => $title,
			'description' => $description,
			'exception' => $exception
		]) . $s;
	}
}
