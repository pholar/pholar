<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Exceptions;

use Psr\Http\Message\ResponseInterface as Response;

/**
 * This exception is used only in \Pholar\Controllers\ShareController
 * to redirect to share login view, which is different from generic login,
 * and is catched in \Pholar\Middleware\HttpExceptionsMiddleware
 * (see comment there for more details)
 */
class HttpRedirect extends \Exception implements HttpExceptionInterface
{
	/** @var string */
	private $httplocation;

	public function __construct(string $location, int $code = 302)
	{
		parent::__construct();
		$this->httplocation = $location;
		$this->code = $code;
	}

	public function makeResponse(Response $response = null) : Response
	{
		if (is_null($response)) {
			$response = new \Slim\Psr7\Response();
		}
		return $response->withHeader('Location', $this->httplocation)
						->withStatus($this->code);
	}
}
