<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar;

use Psr\Log\LoggerInterface;

use \PDOException;

use Envms\FluentPDO\Query;
use Envms\FluentPDO\Literal;
use Envms\FluentPDO\Queries\Select;

use Pholar\Settings;
use Pholar\Utils;
use Pholar\Exceptions\SearchException;
use Pholar\SQL\SQLDriver;
use Pholar\Meta\Meta;

/**
 * Photo Manager
 */
class PhotoMgr
{
	/** @var Settings */
	protected $settings;

	/** @var SQLDriver */
	protected $sqldriver;

	/** @var LoggerInterface */
	protected $logger;

	/** @var Query */
	protected $db;

	/** @var TagMgr */
	protected $tagmgr;

	/** @var CollectionMgr */
	protected $collectionmgr;

	/** @var Meta */
	protected $meta;
	
	/** @var TaskMgr */
	protected $taskmgr;

	public function __construct(
		Settings $settings,
		SQLDriver $sqldriver,
		LoggerInterface $logger,
		Query $db,
		TagMgr $tagmgr,
		CollectionMgr $collectionmgr,
		Meta $meta,
		TaskMgr $taskmgr
	) {
		$this->settings = $settings;
		$this->sqldriver = $sqldriver;
		$this->logger =$logger;
		$this->db = $db;
		$this->tagmgr = $tagmgr;
		$this->collectionmgr = $collectionmgr;
		$this->meta = $meta;
		$this->taskmgr = $taskmgr;
	}



	/**
	* Get photo table name
	*
	* By default is 'photo', but return $_SESSION['view'] if is set.
	* This allow to use a view per-user, settings 'view' at user login
	*/
	public static function getTable() : string
	{
		if (!isset($_SESSION)) {
			return "photo";
		}
		return $_SESSION['view'] ?? 'photo';
	}



	/**
	 * Return relative path for absolute path filename
	 */
	public function relativePath(string $filename) : string
	{
		$r = str_replace($this->settings->get('app', 'storage'), '', $filename);
		if ($r === "") {
			$r = "/";
		}
		return $r;
	}

	/**
	 * Return absolute path for relative path filename
	 */
	public function absPath(string $filename) : string
	{
		if (!Utils::startsWith($filename, "/")) {
			$filename = "/" . $filename;
		}
		return $this->settings->get('app', 'storage') . $filename;
	}

	/**
	 * Check if $abspath is a valid absolute path under STORAGEDIR
	 */
	public function isValidAbsPath(string $abspath) : bool
	{
		$abspath = Utils\Path::getAbsolute($abspath);
		return Utils::startsWith($abspath, $this->settings->get('app', 'storage'));
	}

	/**
	 * Add a record for file, extract basic filesystem info, schedule tasks
	 *
	 * an hidden file called ".$filename.ignore" will cause the update process to stop
	 * Tasks for extract metadata and generate thumbnails are scheuled
	 *
	 * @throws PDOException
	 * @param  string $filename Absolute path to file
	 * @return ?int Photo id or null if photo is ignored
	 */
	public function add(string $filename) : ?int
	{
		$this->logger->info("add", func_get_args());

		// ignore "hidden" files
		if ($filename[0] == ".") {
			$this->logger->debug("ignore hidden file");
			return null;
		}

		// Quit the update proceure if media file must be ignored
		$ignoremarker = dirname($filename) . "/." . basename($filename) . ".ignore";
		if (file_exists($ignoremarker)) {
			$this->logger->info("ignore $filename");
			return null;
		}

		$dbdata = $this->meta->getBasic($filename);
		$dbdata['location'] = $this->relativePath($dbdata['location']);

		// format data for db
		$dbdata = $this->dbdatafmt($dbdata);

		// record update datetime
		$dbdata['recordmtime'] = \Pholar\Fields\DateTime::store(time());

		$q = $this->db->insertInto('photo')->values($dbdata);
		$id = $q->execute();

		$this->scheduleExtract($id);
		$this->scheduleThumbs($id);
		$this->scheduleFaces($id);
		
		return $id;
	}


	/**
	 * Schedule a task to generate thumb for photo
	 *
	 * @param int $id Photo id
	 */
	public function scheduleThumbs(int $id) : void
	{
		$this->taskmgr->schedule(\Pholar\Tasks\Thumbnail::class, ['id' => $id]);
	}

	/**
	 * Schedule a task to extract metadata
	 *
	 * A photo must be exists in db already
	 *
	 * @param int $id Photo id
	 */
	public function scheduleExtract(int $id) : void
	{
		$this->taskmgr->schedule(\Pholar\Tasks\ExtractMeta::class, ['id' => $id]);
	}

	/**
	 * Schedule a task to write back meta into file
	 *
	 * @param int $id Photo id
	 */
	public function scheduleWriteback(int $id) : void
	{
		$this->taskmgr->schedule(\Pholar\Tasks\WritebackMeta::class, ['id' => $id]);
	}

	/**
	 * Schedule a task to find and recognize faces in file
	 *
	 * @param int $id Photo $id
	 */
	public function scheduleFaces(int $id) : void
	{
		if ($this->settings->get('facetool', 'enabled') == false) {
			return;
		}
		$this->taskmgr->schedule(Tasks\FaceRecognition::class, ['id'=>$id]);
	}

	/**
	 * Update meta for record by id
	 *
	 * @throws PDOException
	 * @param int $id Photo id
	 * @return integer : photo id or null if photo is ignored
	 */
	public function updateById($id) : ?int
	{
		$photo = $this->byId($id);
		$filename = $this->getFullSourcePath($photo);
		return $this->update($filename);
	}

	/**
	 * Add or update meta for file
	 *
	 * an hidden file called ".$filename.ignore" will cause the update process to stop
	 * if a sidecar xmp file (named "$filename.xmp") is found, or if filename is a sidecar
	 * xmp file and the asset file exists, load info from sidecar xmp
	 * TODO: is the correct thing to do? or it's better to stop?
	 * 		 when inode watcher is on this could trigger and update as soon metas are
	 * 		 saved to xmp
	 *
	 * TODO: use pdfinfo to fetch extra pdf info (like page size)
	 *
	 * @throws PDOException
	 * @param  string $filename: absolute path to file
	 * @return integer : photo id or null if photo is ignored
	 */
	public function update(string $filename) : ?int
	{
		$this->logger->info("update", func_get_args());

		// ignore "hidden" files
		if ($filename[0] == ".") {
			$this->logger->debug("ignore hidden file");
			return null;
		}

		// Update media file when sidecar xmp is found
		if (Utils::endsWith($filename, ".xmp") && file_exists(substr($filename, 0, -4))) {
			$filename = substr($filename, 0, -4);
			$this->logger->info("update {$filename} from sidecar xmp file");
		}

		// Quit the update proceure if media file must be ignored
		$ignoremarker = dirname($filename) . "/." . basename($filename) . ".ignore";
		if (file_exists($ignoremarker)) {
			$this->logger->info("ignore $filename");
			return null;
		}

		$dbdata = $this->meta->get($filename);

		// location is alwais relative to storage dir
		$dbdata['location'] = $this->relativePath($dbdata['location']);

		// get tags and collections
		$tags = [];
		$collections = [];
		if (isset($dbdata['_tags'])) {
			$_tags = $dbdata['_tags'];
			unset($dbdata['_tags']);
			$_tags = explode(" ; ", $_tags);
			foreach ($_tags as $_t) {
				if (Utils::startsWith($_t, "collection:")) {
					$collections[] = str_replace("collection:", "", $_t);
				} else {
					$tags[] = $_t;
				}
			}
			$tags = array_unique($tags);
			$collections = array_unique($collections);
		}

		// format data for db
		$dbdata = $this->dbdatafmt($dbdata);

		// record update datetime
		$dbdata['recordmtime'] = \Pholar\Fields\DateTime::store(time());


		$id = $this->existsByPath($filename);
		if ($id === false) {
			$q = $this->db->insertInto('photo')->values($dbdata);
			$id = $q->execute();
		} else {
			$q = $this->db->update('photo', $dbdata, $id);
			$q->execute();
		}
		$dbdata['id'] = $id;

		// remove all tags and collections and reapply
		$this->setTags($dbdata, $tags);
		$this->setCollections($dbdata, $collections);


		return $id;
	}

	/**
	 * Look if photo exists given the absolute path
	 *
	 * @param string $filename Absolute path to file
	 * @return int|false       Row id if found
	 */
	public function existsByPath(string $filename) : int|false
	{
		$filename = $this->relativePath($filename);
		$fname = basename($filename);
		$dname = dirname($filename);
		$r = $this->db->from('photo')
			->select('id')
			->where([
				'filename' => $fname,
				'location' => $dname
			])->fetch();
		if ($r === false) {
			return $r;
		} else {
			return $r['id'];
		}
	}

	/**
	 * Get all photos
	 *
	 * @param string $orderby Order
	 * @return Select
	 */
	public function all(string $orderby = null) : Select
	{
		$query = $this->db->from($this->getTable());
		if (!is_null($orderby)) {
			$query->orderBy($orderby);
		}
		return $query;
	}

	/**
	 * Get photo by id
	 *
	 * @param ?int $id
	 * @return ?PhotoData Data or null
	 */
	public function byId(?int $id) : ?array
	{
		if (is_null($id)) {
			return null;
		}
		$u = $this->db->from($this->getTable(), $id)->fetch();
		return $u === false ? null : $u;
	}

	/**
	 * Filter photos by search query
	 *
	 * @param ?string $searchquery
	 * @param ?string $orderby
	 * @return Select
	 */
	public function filter(string $searchquery = null, string $orderby = null) : Select
	{
		if (is_null($searchquery) || trim($searchquery) === "") {
			return $this->all($orderby);
		}

		$searchquery = trim($searchquery);
		$photos = $this->all();
		return $this->filterphotos(
			$photos,
			$searchquery,
			$orderby,
			$this->getTable()
		);
	}

	/**
	 * Parse searchquery and filter photos
	 *
	 * @param Select $photos       photos to filter
	 * @param string $searchquery  search query to parse
	 * @param ?string $orderby     order
	 * @param string $phototable  table used to query photos. could be a view.
	 * @return Select
	 */
	private function filterphotos(Select $photos, string $searchquery, string $orderby = null, string $phototable = "photo") : Select
	{
		$tokens = Utils::tokenize($searchquery);
		// echo "<pre>"; var_dump($tokens); die();

		$searchableFields = Meta::searchableFields();
		$tagfilterquery = [];
		$tagfilterparams = [];
		$collectionfilterquery = [];
		$collectionfilterparams = [];
		$facefilterquery = [];
		$facefilterparams = [];
		$usedfields = [];
		$hasbeenordered = false;

		foreach ($tokens as $token) {
			if (is_array($token)) {
				list($field, $value) = $token;
				if ($field=="orderby") {
					$photos->orderBy($phototable . "." . $value);
					$hasbeenordered = true;
					continue;
				}

				$fieldmeta = Meta::fieldsMeta($field);
				if (is_null($fieldmeta)) {
					throw new SearchException("'$field' is not a valid field");
				}
				$filter = $fieldmeta['class']::filter($field, $value);
				if ($field=="tag") {
					list($f, $v) = $filter;
					$photos->leftJoin("photo_tag ON photo_tag.photo_id = $phototable.id");
					$photos->leftJoin("tag ON photo_tag.tag_id = tag.id");

					$tagfilterquery[]= "tag.$f = ?";
					$tagfilterparams[] = $v;
				} elseif ($field=="collection") {
					$f = "name";
					$v = $filter[1];
					$photos->leftJoin("photo_collection ON photo_collection.photo_id = $phototable.id");
					$photos->leftJoin("collection ON photo_collection.collection_id = collection.id");

					$collectionfilterquery[]= "collection.$f = ?";
					$collectionfilterparams[] = $v;
				} elseif ($field=="people") {
					$f = "name";
					$v = $filter[1];
					$photos->leftJoin("face ON face.photo_id = $phototable.id");

					$facefilterquery[]= "face.$f = ?";
					$facefilterparams[] = $v;
				} else {
					if (in_array($field, $usedfields)) {
						call_user_func_array([$photos, 'whereOr'], $filter);
					} else {
						$usedfields[] = $field;
						call_user_func_array([$photos, 'where'], $filter);
					}
				}
			} else {
				$query = [];
				$values = [];
				foreach ($searchableFields as $field) {
					$fieldmeta = Meta::fieldsMeta($field);
					$filter = $fieldmeta['class']::filter($field, $token);
					$query[] = $filter[0];
					$values[] = $filter[1];
				}
				$query = "(" . implode(" OR ", $query) . ")";
				$photos->where($query, $values);
			}
		}

		if (!$hasbeenordered && !is_null($orderby)) {
			$photos->orderBy($phototable . "." . $orderby);
		}

		$tagfiltercount = count($tagfilterparams);
		if ($tagfiltercount > 0) {
			$query = "(" . implode(" OR ", $tagfilterquery) . ")";
			$photos->where($query, $tagfilterparams);
			$photos->having("COUNT(tag.id) >= $tagfiltercount");
		}

		$collectionfiltercount = count($collectionfilterparams);
		if ($collectionfiltercount> 0) {
			$query = "(" . implode(" OR ", $collectionfilterquery) . ")";
			$photos->where($query, $collectionfilterparams);
			$photos->having("COUNT(collection.id) >= $collectionfiltercount");
		}

		$facefiltercount = count($facefilterparams);
		if ($facefiltercount> 0) {
			$query = "(" . implode(" OR ", $facefilterquery) . ")";
			$photos->where($query, $facefilterparams);
			$photos->having("COUNT(face.id) >= $facefiltercount");
		}
		
		$photos->groupBy("$phototable.id");

		#dd($photos->getquery(), $photos->getParameters());

		return $photos;
	}

	/**
	 * Create/Update user view over photo table based on user query
	 *
	 * @param PhotoData $user user data
	 */
	public function createViewForUser(array $user) : void
	{
		$viewname = "photo_" . $user['user'];
		$quotedviewname = $this->sqldriver->quote($viewname);


		$pdo = $this->db->getPdo();
		$pdo->prepare(sprintf('DROP VIEW IF EXISTS %s', $quotedviewname))
			 ->execute();

		if ($user['query'] !== "") {
			$photos = $this->db->from('photo');
			$photos = $this->filterphotos($photos, $user['query']);
			$select = $photos->getQuery();
			$params = $photos->getParameters();
			$query = sprintf(
				'CREATE VIEW %s AS %s',
				$quotedviewname,
				$select
			);
			// this monstruosity because I can't prepare a CREATE VIEW query
			// with params in SELECT...
			$count = -1;
			$query = preg_replace_callback(
				"|\s(\?)\s*|",
				function ($matches) use ($count, $params) {
					$count++;
					return str_replace($matches[1], "'" . $params[$count] . "'", $matches[0]);
				},
				$query
			);
			$pdo->prepare($query)->execute();
		}
	}

	/**
	 * get full path to source file for $photo
	 *
	 * @param int|PhotoData $photo Photo id or photo data
	 * @return string Absolute path to source file
	 */
	public function getFullSourcePath(int|array $photo) : string
	{
		if (!is_array($photo)) {
			$photo = $this->byId($photo);
		}
		$sourcefile = rtrim($photo['location'], '/') . "/" . $photo['filename'];
		return $this->absPath($sourcefile);
	}

	/**
	 * Get tags for photo id
	 *
	 * @param int $id Photo id
	 * @return Select
	 */
	public function getTagsFor(int $id) : Select
	{
		return $this->db->from('photo_tag')->select('tag.*')->where('photo_tag.photo_id', $id);
	}

	/**
	 * Get collections for photo id
	 *
	 * @param int $id Photo id
	 * @return Select
	 */
	public function getCollectionsFor(int $id) : Select
	{
		return $this->db->from('photo_collection')->select('collection.*')->where('photo_collection.photo_id', $id);
	}

	/**
	 * Set collections for photo
	 *
	 * @param PhotoData  $photo
	 * @param array<string> $collections List of collection names
	 */
	public function setCollections(array $photo, array $collections) : void
	{
		// remove all collections and reapply
		$this->db->delete('photo_collection')->where("photo_id", $photo['id'])->execute();

		foreach ($collections as $collection) {
			$this->addToCollection($photo, $collection);
		}
	}

	/**
	 * Set tags for photo
	 *
	 * @param PhotoData  $photo
	 * @param array<string> $tags List of tag names
	 */
	public function setTags(array $photo, array $tags) : void
	{
		// remove all tags and reapply
		$this->db->delete('photo_tag')->where("photo_id", $photo['id'])->execute();

		foreach ($tags as $tag) {
			$this->addTag($photo, $tag);
		}
	}

	/**
	 * Add a tag to the photo
	 *
	 * @param PhotoData  $photo
	 * @param string $tag
	 */
	public function addTag(array $photo, string $tag) : void
	{
		$tag = trim($tag);
		if ($tag === "") {
			return;
		}
		$r = $this->tagmgr->add($tag);
		try {
			$this->db->insertInto('photo_tag')
					->values(['photo_id'=>$photo['id'], 'tag_id'=>$r])
					->execute();
		} catch (\Exception $e) {
			if (strpos(
				$e->getMessage(),
				"UNIQUE constraint failed: photo_tag.photo_id, photo_tag.tag_id"
			) === false
			) {
				throw $e;
			}
		}
	}

	/**
	 * Add a photo to a collection
	 *
	 * @param PhotoData $photo   Photo to add
	 * @param string $collection Collection name
	 */
	public function addToCollection(array $photo, string $collection) : void
	{
		$collection = trim($collection);
		if ($collection === "") {
			return;
		}
		$r = $this->collectionmgr->add($collection);
		$this->collectionmgr->addPhoto($r, $photo['id']);
	}

	/**
	 * Delete photo
	 *
	 * delete photo from db and mark file to be ignored
	 *
	 * @param ?PhotoData  $photo
	 */
	public function delete(?array $photo) : void
	{
		if (is_null($photo)) {
			return;
		}
		$id = $photo['id'] ?? null;
		if (is_null($id)) {
			return;
		}
		$filename = $this->absPath($photo['location'] . "/" . $photo['filename']);
		if (file_exists($filename)) {
			// user want to remove photo but file is still in storage:
			// mark it as ignored and leave the file
			$ignoremarker = $photo['location'] . "/." . $photo['filename'] . ".ignore";
			$ignoremarker = $this->absPath($ignoremarker);
			touch($ignoremarker);
		}

		$this->db->deleteFrom("photo", $id)->execute();
	}

	/**
	 * Remove thumb images for photo by id
	 * TODO: this is not really the place for this. There should be a ThmubMgr
	 * which handles thumbs for photos, using Thumbnailer (which should be moved
	 * and split like "Meta").
	 * Anyway, here's the function..
	 *
	 * @param int $id Photo $id
	 */
	public function thumbDeleteById(int $id) : void
	{
		// remove cached thumbs
		$thumbglob = $this->settings->get('paths', 'public') . "/thumb/*/" . $id . ".jpg";
		$thumbfiles = glob($thumbglob);
		foreach ($thumbfiles as $thumbfile) {
			unlink($thumbfile);
		}
		// remove cached page thumbs
		$thumbglob = $this->settings->get('paths', 'public') . "/thumb/*/" . $id . "p*.jpg";
		$thumbfiles = glob($thumbglob);
		foreach ($thumbfiles as $thumbfile) {
			unlink($thumbfile);
		}
	}

	/**
	 * Format data for db
	 *
	 * @param PhotoData $data
	 * @return PhotoData Formatted data
	 */
	private function dbdatafmt(array $data) : array
	{
		$dbdatafmt = [];
		foreach ($data as $fieldname => $fieldvalue) {
			// skip null
			if (!is_null($fieldvalue)) {
				$field = Meta::fieldsMeta($fieldname);
				$kls = $field['class'];
				$dbdatafmt[$fieldname] = $kls::store($fieldvalue);
			}
		}
		return $dbdatafmt;
	}

	/**
	 * Save metadata in db and schedule job to write  back to file
	 *
	 * @param PhotoData $photo
	 */
	public function save(array $photo) : void
	{
		$dbquery = $this->db->update('photo')->where('id', $photo['id']);
		foreach ($photo as $fieldname => $fieldvalue) {
			$field = Meta::fieldsMeta($fieldname);
			if (!is_null($field) && $field['edit']) {
				$kls = $field['class'];
				$dbquery->set($fieldname, $kls::store($fieldvalue));
			}
		}
		// update gps position
		if (!is_null($photo['latitude']) && !is_null($photo['longitude'])) {
			$dbquery->set('position', sprintf("%s %s", $photo['latitude'], $photo['longitude']));
		}
		$dbquery->set("recordmtime", new Literal($this->sqldriver::NOW));
		$dbquery->execute();

		$this->scheduleWriteback($photo['id']);
	}
}
