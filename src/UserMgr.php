<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar;

use Psr\Log\LoggerInterface;
use Envms\FluentPDO\Query;

use Pholar\Exceptions\QueryException;
use Pholar\Settings;
use Pholar\Utils;

class UserMgr
{
	public const SESSION_USER_NAME = 'user';
	public const SESSION_PREV_NAME = 'prevuser';

	/** @var Settings */
	protected $settings;

	/** @var LoggerInterface */
	protected $logger;

	/** @var Query */
	protected $db;

	public function __construct(Settings $settings, LoggerInterface $logger, Query $db)
	{
		$this->settings = $settings;
		$this->logger = $logger;
		$this->db = $db;
	}

	/**
	 * @return array<string,string>
	 */
	public function getDefaultPermissions() : array
	{
		// This is same as default values in schema.sql
		return [
				'can_login' => '1',
				'can_edit' => '1',
				'can_delete' => '1',
				'can_share' => '1',
				'can_download' => '1',
				'can_upload' => '1',
				'is_admin' => ''
		];
	}

	/**
	 * Convert data from db
	 *
	 * @param false|UserData $arr
	 * @return ?UserData
	 */
	private function fromDb(false|array $arr) : ?array
	{
		if ($arr === false) {
			$arr = null;
		}
		if (!is_null($arr)) {
			foreach ($arr as $k => $v) {
				if (Utils::startsWith($k, "can_") || Utils::startsWith($k, "is_")) {
					$arr[$k] = ($v === "1" || $v === 1);
				}
			}
		}
		return $arr;
	}

	/**
	 * Convert data to db
	 *
	 * @param ?UserData $arr
	 * @return ?UserData
	 */
	private function toDb(?array $arr) : ?array
	{
		if (!is_null($arr)) {
			foreach ($arr as $k => $v) {
				if (Utils::startsWith($k, "can_") || Utils::startsWith($k, "is_")) {
					$arr[$k] = intval($v);
				}
			}
		}
		return $arr;
	}

	/**
	 * Get user by name
	 *
	 * @return ?UserData
	 */
	public function getByName(?string $name) : ?array
	{
		return $this->fromDb(
			$this->db->from('user')->where('user', $name)->fetch()
		);
	}

	/**
	 * save user
	 *
	 * @param UserData $user
	 */
	public function save(array $user) : void
	{
		$user = $this->toDb($user);

		$u = $this->getByName($user['user']);
		if (is_null($u)) {
			$query = $this->db->insertInto('user', $user); //->execute();
		} else {
			$query = $this->db->update('user', $user)->where('user', $user['user']); //->execute();
		}
		try {
			/** @throws \PDOException */
			$query->execute();
		} catch (\PDOException $e) {
			throw new QueryException("Query error {$e->getMessage()} '". $query->getQuery(false) ."'");
		}
	}

	/**
	 * delete user by username
	 *
	 * @param string $username
	 */
	public function delete(string $username) : void
	{
		$this->db->deleteFrom('user')->where('user', $username)->execute();
	}

	/**
	 * Check user and password and set user in session
	 *
	 * @param string $user
	 * @param string $pass
	 * @return ?UserData null on error
	 */
	public function auth(string $user, string $pass) : ?array
	{
		$u = $this->getByName($user);
		if (!is_null($u)) {
			if ($u['can_login'] && password_verify($pass, $u['password'])) {
				$this->login($user);
				return $u;
			}
		}

		unset($_SESSION[UserMgr::SESSION_USER_NAME]);
		return null;
	}

	/**
	 * Set user in session
	 */
	public function login(string $username) : void
	{
		$_SESSION[UserMgr::SESSION_USER_NAME] = $username;
	}

	/**
	 * Unset user from session
	 */
	public function logout() : void
	{
		unset($_SESSION[UserMgr::SESSION_USER_NAME]);
		unset($_SESSION['view']);
	}

	/**
	 * Get logged user in current session
	 *
	 * @return ?UserData null if no user in session
	 */
	public function getForSession() : ?array
	{
		$uname = $_SESSION[UserMgr::SESSION_USER_NAME] ?? $this->settings->get('app', 'autologin');
		$user = $this->getByName($uname);

		if (!is_null($user) && $user['query'] !== "") {
			$_SESSION['view'] = 'photo_' . $user['user'];
		} else {
			unset($_SESSION['view']);
		}
		return $user;
	}

	/**
	 * Set current user as 'guest', save previous user
	 */
	public function loginGuest() : void
	{
		$uname = $_SESSION[UserMgr::SESSION_USER_NAME] ?? null;
		if ($uname !== "guest") {
			$_SESSION[UserMgr::SESSION_PREV_NAME] = $uname;
			$this->login("guest");
		}
	}

	/**
	 * Reset current user as previous user
	 */
	public function logoutGuest() : void
	{
		$uname = $_SESSION[UserMgr::SESSION_USER_NAME] ?? null;
		if ($uname === "guest") {
			$pname = $_SESSION[UserMgr::SESSION_PREV_NAME] ?? null;
			$this->login($pname);
		}
	}
}
