<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar;

use Psr\Log\LoggerInterface;

use Envms\FluentPDO\Query;
use Envms\FluentPDO\Queries\Select;

use Pholar\SQL\SQLDriver;

class TagMgr
{
	/** @var SQLDriver */
	protected $sqldriver;

	/** @var LoggerInterface */
	protected $logger;

	/** @var Query */
	protected $db;

	public function __construct(
		LoggerInterface $logger,
		Query $db,
		SQLDriver $sqldriver,
	) {
		$this->logger = $logger;
		$this->db = $db;
		$this->sqldriver = $sqldriver;
	}
	
	/**
	 * Get all tags
	 *
	 * @return Select
	 */
	public function all() : Select
	{
		return $this->db->from("tag")->orderBy("tag");
	}
	
	/**
	 * Get all tags with number of photos per tag
	 *
	 * @return Select
	 */
	public function photoCount() : Select
	{
		return $this->db->from("photo_tag")
						->select("tag.*")
						->select("count(photo_tag.photo_id) as n_photos")
						->groupBy("tag.id");
	}

	/**
	 * Get a tag by id
	 *
	 * @param int $id Tag id
	 * @return ?TagData
	 */
	public function byId(int $id) : ?array
	{
		$u = $this->db->from('tag', $id)->fetch();
		return $u === false ? null : $u;
	}

	/**
	 * Get tag(s) by name(s)
	 *
	 * @param string|array<string> $name Tag name(s)
	 * @return Select
	 */
	public function byName(mixed $name) : Select
	{
		if (!is_array($name)) {
			$name = [$name];
		}

		return $this->all()->where('tag', $name);
	}
	
	/**
	 * Add a tag and return tag id
	 *
	 * @param string $tag Tag name
	 * @return int Tag id
	 */
	public function add(string $tag) : int
	{
		$r = $this->db->from('tag')
						->select('id')
						->where('tag', $tag)
						->fetch();
		if ($r === false) {
			$r = $this->db->insertInto('tag', ['tag' => $tag])->execute();
		} else {
			$r = $r['id'];
		}
		return $r;
	}

	/**
	 * Get photos for tag
	 *
	 * @param int $id Tag id
	 * @return Select
	 */
	public function getPhotosFor(int $id) : Select
	{
		$phototable = PhotoMgr::getTable();
		$quotedphototable = $this->sqldriver->quote($phototable);
		return $this->db->from('photo_tag')
						->select(sprintf('%s.*', $quotedphototable))
						->where('photo_tag.tag_id', $id);
	}


	/**
	 * Save a tag
	 *
	 * @param TagData  $tag
	 */
	public function save($tag) : void
	{
		$id = $tag['id'] ?? null;
		if (is_null($id)) {
			$this->db->insertInto('tag', $tag)->execute();
		} else {
			$this->db->update('tag', $tag, $id)->execute();
		}
	}

	/**
	 * Delete a tag
	 *
	 * @param int $id Tag id
	 */
	public function delete(int $id) : void
	{
		$this->db->deleteFrom('tag', $id)->execute();
		$this->db->deleteFrom('photo_tag')
					  ->where('tag_id', $id)
					  ->execute();
	}
}
