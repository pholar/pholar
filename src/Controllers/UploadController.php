<?php
/*
	Copyright (c) 2021 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Exception\HttpForbiddenException;
use Slim\Exception\HttpBadRequestException;

use Pholar\Utils;
use Pholar\Meta\Meta;

/**
 * Code related to photos: search, insert, remove, update
 */
class UploadController extends BaseController
{
	const DEST_FOLDER_FROM_META = -1;
	const DEST_FOLDER_FROM_CURRENT_DATE = -2;

	/**
	 * @httpmethod GET
	 * @param RouteData $args
	 */
	public function upload(Request $request, Response $response, array $args) : Response
	{
		if (is_null($this->user) || $this->user['can_upload'] === false) {
			throw new HttpForbiddenException($request, "Forbidden");
		}

		$params = $request->getQueryParams();
		$to = $params['to'] ??  "-1";

		$context = [
			'dirs' => new Utils\StorageDirectoryIterator($this->settings->get('app', 'storage')),
			'to' => $to,
		];

		return $this->view->render($response, "upload.twig", $context);
	}

	/**
	 * Return from an upload operation
	 *
	 * Return $message as a flash message in a redirect or a json (if $isjson is true) with status $code
	 */
	private function returnUpload(Response $response, string $redirect, bool $isjson, string $message, int $code = 200) : Response
	{
		if ($isjson) {
			return $this->toJson(
				$response->withStatus($code),
				[
					"message"=>$message
				]
			);
		} else {
			$type = ($code == 200) ? "success" : "error";
			$this->flash->addMessage($type, $message);
			return $response->withHeader('Location', $redirect)
				->withStatus(302);
		}
	}

	/**
	 * @httpmethod POST
	 * @param RouteData $args
	 */
	public function uploadPost(Request $request, Response $response, array $args) : Response
	{
		if (is_null($this->user) || $this->user['can_upload'] === false) {
			throw new HttpForbiddenException($request, "Forbidden");
		}

		/** @var ParsedData */
		$data = $request->getParsedBody();
		$redirect = $data['redirect'] ?? $this->routeParser->urlFor('search');
		$uploadto = $data['to'] ?? "-1";

		$files = $request->getUploadedFiles();
		$this->logger->info($this->user['user'] . " is uploading " . count($files) . " files to " . $uploadto);

		/** @var ParsedData */
		$data = $request->getParsedBody();
		$firstAccept = $request->getHeader('Accept')[0] ?? 'html';
		$isjson = strpos($firstAccept, 'json') !== false;

		if (count($files) == 0) {
			$message = "No file was uploaded.";
			return $this->returnUpload($response, $redirect, $isjson, $message, 400);
		}


		/* check for errors */
		foreach ($files['file'] as $file) {
			if ($file->getError() !== UPLOAD_ERR_OK) {
				switch ($file->getError()) {
					case UPLOAD_ERR_INI_SIZE:
						$message = sprintf(
							"The file '%s' size exceeds the maximum permitted size of %s.",
							$file->getClientFilename(),
							ini_get('upload_max_filesize')
						);
						$code = 400;
						break;
					case UPLOAD_ERR_FORM_SIZE:
						$message = sprintf(
							"The file '%s' size exceeds the maximum permitted size of %s.",
							$file->getClientFilename(),
							Utils::bytes2str((int) ($data['MAX_FILE_SIZE'] ?? null))
						);
						$code = 400;
						break;
					case UPLOAD_ERR_PARTIAL:
						$message = sprintf(
							"The file '%s' size was only partially uploaded.",
							$file->getClientFilename()
						);
						$code = 400;
						break;
					case UPLOAD_ERR_NO_FILE:
						$message = "No file was uploaded.";
						$code = 400;
						break;
					case UPLOAD_ERR_NO_TMP_DIR:
						$message = "An error occured uploading: Missing temporary folder.";
						$code = 500;
						break;
					case UPLOAD_ERR_CANT_WRITE:
						$message = "An error occured uploading: Failed to write file to disk.";
						$code = 500;
						break;
					default:
						$message = "An error occured uploading.";
						$code = 500;
				}
				$this->logger->info("File " . $file->getClientFilename() . " failed with error " . $file->getError());
				return $this->returnUpload($response, $redirect, $isjson, $message, $code);
			}
		}

		/* move each file in storage and update meta
		- file is moved in temporary posistion
		if $uploadto is DEST_FOLDER_FROM_META (-1):
			- metadata is extracted
			- datetime is used to create destination path in storage
				- if not present, current datetime is used
		elif $uploadto is DEST_FOLDER_FROM_CURRENT_DATE (-2):
			- current datetime is used to create destination path in storage
		else:
			- $uploadto is used to create destination path in storage
		- file is moved in storage
		- metadata are updated in db
		*/
		foreach ($files['file'] as $k => $file) {
			$clientname = $file->getClientFilename();
			$tmpfname = tempnam(sys_get_temp_dir(), 'UPL'.$k);
			$file->moveTo($tmpfname);

			switch ($uploadto) {
				case self::DEST_FOLDER_FROM_META:
					$meta = new Meta($this->settings, $this->logger);
					$data = $meta->get($tmpfname);

					$datetime  = $data['datetime'] ?? date('Y-m-d');
					$datetime = str_replace(":", "-", $datetime); // ?why date in datetime from meta is separated by ":"?
					$uploadto = str_replace("-", "/", explode(" ", $datetime)[0]);
					break;
				case self::DEST_FOLDER_FROM_CURRENT_DATE:
					$datetime = date('Y-m-d');
					$uploadto = str_replace("-", "/", explode(" ", $datetime)[0]);
					break;
			}

			$folder = $this->photomgr->absPath($uploadto);
			$this->logger->debug("creating destination folder $folder");

			if (!$this->photomgr->isValidAbsPath($folder)) {
				throw new HttpBadRequestException($request, "Invalid upload destination path");
			}
			if (!is_dir($folder) && !mkdir($folder, 0777, true)) {
				$message = "An error occured uploading: Unable to create destination folder.";
				return $this->returnUpload($response, $redirect, $isjson, $message, 500);
			}

			$destfile =  $folder . "/" . $clientname;
			$n = 1;
			while (is_file($destfile)) {
				$this->logger->info("'$destfile' exists");
				$destfile =  $folder . "/" . str_pad((string) $n, 4, "0", STR_PAD_LEFT) . "_" . $clientname;
				$n++;
			}


			// move uploaded file to destfile
			$this->logger->info("moving in $destfile");
			if (!rename($tmpfname, $destfile)) {
				$message = "An error occured uploading: Unable to move file in destination folder.";
				return $this->returnUpload($response, $redirect, $isjson, $message, 500);
			}


			$id = $this->photomgr->existsByPath($destfile);
			if ($id === false) {
				// file didn't exists prior upload, add it
				$id = $this->photomgr->add($destfile);
			}
		}

		$message = sprintf("Uploaded %d new files", count($files));
		return $this->returnUpload($response, $redirect, $isjson, $message, 200);
	}
}
