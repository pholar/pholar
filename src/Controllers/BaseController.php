<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Controllers;

use Psr\Log\LoggerInterface;
use Slim\Interfaces\RouteParserInterface;
use Psr\Http\Message\ResponseInterface as Response;

use Envms\FluentPDO\Query;
use Slim\Views\Twig;
use Slim\Flash\Messages;

use Pholar\Settings;
use Pholar\PhotoMgr;
use Pholar\TagMgr;
use Pholar\CollectionMgr;
use Pholar\ShareMgr;
use Pholar\UserMgr;
use Pholar\FaceMgr;
use Pholar\Thumbnailer;
use Pholar\TaskMgr;
use Pholar\SQL\SQLDriver;

class BaseController
{
	/**
	 * @var Settings
	 */
	protected $settings;

	/**
	 * @var LoggerInterface
	 */
	protected $logger;

	/**
	 * @var Query
	 */
	protected $db;

	/**
	 * @var Twig
	 */
	protected $view;

	/**
	 * @var Messages
	 */
	protected $flash;

	/**
	 * @var PhotoMgr
	 */
	protected $photomgr;

	/**
	 * @var TagMgr;
	 */
	protected $tagmgr;

	/**
	 * @var CollectionMgr
	 */
	protected $collectionmgr;

	/**
	 * @var ShareMgr
	 */
	protected $sharemgr;

	/**
	 * @var UserMgr
	 */
	protected $usermgr;

	/**
	 * @var FaceMgr
	 */
	protected $facemgr;

	/**
	 * @var Thumbnailer;
	 */
	protected $thumbnailer;

	/**
	 * @var TaskMgr
	 */
	protected $taskmgr;

	/**
	 * @var RouteParserInterface
	 */
	protected $routeParser;

	/**
	 * @var SQLDriver
	 */
	protected $sqldriver;

	/**
	 * @var ?UserData
	 */
	protected $user;

	public function __construct(
		Settings $settings,
		LoggerInterface $logger,
		Query $db,
		Twig $view,
		Messages $flash,
		PhotoMgr $photomgr,
		TagMgr $tagmgr,
		CollectionMgr $collectionmgr,
		ShareMgr $sharemgr,
		UserMgr $usermgr,
		FaceMgr $facemgr,
		Thumbnailer $thumbnailer,
		TaskMgr $taskmgr,
		SQLDriver $sqldriver,
		RouteParserInterface $routeParser
	) {
		$this->settings = $settings;
		$this->logger = $logger;

		$this->routeParser = $routeParser;
		$this->photomgr = $photomgr;
		$this->tagmgr = $tagmgr;
		$this->collectionmgr = $collectionmgr;
		$this->sharemgr = $sharemgr;
		$this->facemgr = $facemgr;
		$this->taskmgr = $taskmgr;
		$this->thumbnailer = $thumbnailer;
		$this->sqldriver = $sqldriver;

		$this->db = $db;
		$this->view = $view;
		$this->flash = $flash;

		$this->usermgr = $usermgr;
		$this->setUser();
	}

	protected function setUser() : void
	{
		$this->user = $this->usermgr->getForSession();
	}

	protected function toJson(Response $response, mixed $data) : Response
	{
		$response->getBody()->write(json_encode($data));
		return $response->withHeader('Content-Type', "application/json");
	}
}
