<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Pholar\Tasks\FaceTag;

/**
 * Code related to faces: edit, delete
 */
class FaceController extends BaseController
{
	/**
	 * Rename face
	 *
	 * @param array<string,string> $args
	 * @httpmethod POST
	 * @form name: new face name, redirect: url
	 * @args fid: face id
	 */
	public function rename(Request $request, Response $response, array $args) : Response
	{
		if (is_null($this->user) || !$this->user['can_edit']) {
			return $response->withStatus(403); // Forbidden
		}
		/** @var string|null */
		$id = $args['fid'];
		if (is_null($id)) {
			return $response->withStatus(400); // Bad Request
		}
		$id = (int) $id;
		
		$data = $request->getParsedBody();
		$name = $data['name'] ?? "";
		$name = trim($name);
		$redirect = $data['redirect'] ?? $this->routeParser->urlFor("search");

		$face = $this->facemgr->byId($id);
		if ($face === false) {
			$this->flash->addMessage('error', "Face not found");
			return $response->withHeader('Location', $redirect)
							->withStatus(302);
		}

		$oldname = $face['name'];
		$face['name'] = $name;

		try {
			$this->facemgr->save($face);
		} catch (\PDOException $e) {
			$this->logger->error("FaceController::rename save: " . $e->getMessage());
			$this->flash->addMessage('error', "Error saving face");
			return $response->withHeader('Location', $redirect)
							->withStatus(302);
		}

		if ($name != '' && $this->settings->get('facetool', 'enabled')) {
			$this->taskmgr->schedule(FaceTag::class, ['face_id' => $face['id']]);
		}
		// TODO: schedule metadata writeback

		// redirect query to new face name. sa di cacata, ma vabè
		$redirect = str_replace(urlencode('people:'.$oldname), urlencode('people:'.$name), $redirect);
		return $response->withHeader('Location', $redirect)
						->withStatus(302);
	}

	/**
	 * Delete face
	 *
	 * @param array<string,string> $args
	 * @httpmethod DELETE
	 * @args fid: face id
	 */
	public function delete(Request $request, Response $response, array $args) : Response
	{
		if (is_null($this->user) || !$this->user['can_edit']) {
			return $response->withStatus(403); // Forbidden
		}

		/** @var string|null */
		$id = $args['fid'];
		if (is_null($id)) {
			return $response->withStatus(400); // Bad Request
		}

		$data = $request->getParsedBody();
		$redirect = $data['redirect'] ?? $this->routeParser->urlFor("search");

		$id = (int) $id;
		$this->facemgr->delete($id);

		// TODO: schedule metadata writeback

		return $response->withHeader('Location', $redirect)
						->withStatus(302);
	}
}
