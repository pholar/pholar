<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Pholar\Utils;

class LoginController extends BaseController
{
	/**
	 * Login page
	 *
	 * @methd GET
	 *
	 * @param RouteData $args
	 */
	public function login(Request $request, Response $response, array $args) : Response
	{
		$params = $request->getQueryParams();
		$redirect = $params['redirect'] ?? null;
		if (is_null($redirect) || !Utils::startsWith($redirect, "/")) {
			$redirect = $this->routeParser->urlFor('search');
		}

		return $this->view->render($response, 'login.twig', [
			'redirect' => $redirect
		]);
	}


	/**
	 * Post login page
	 *
	 * @methd POST
	 *
	 * @param RouteData $args
	 */
	public function postlogin(Request $request, Response $response, array $args) : Response
	{
		/** @var ParsedData */
		$data = $request->getParsedBody();
		$redirect = $data['redirect'] ?? $this->routeParser->urlFor("search");
		$username = $data['username'] ?? null;
		$password = $data['password'] ?? null;

		$user = $this->usermgr->auth($username, $password);

		if (is_null($user)) {
			$this->flash->addMessage('error', 'Invalid login');
			$redirect = $this->routeParser->urlFor("login");
		}

		return $response->withHeader('Location', $redirect)
						->withStatus(302);
	}

	/**
	 * Logout
	 *
	 * @httpmethod POST
	 *
	 * @param RouteData $args
	 */
	public function logout(Request $request, Response $response, array $args) : Response
	{
		if ($this->settings->get('app', 'autologin')) {
			// if autologin is set, return to 'search'
			$redirect = $this->routeParser->urlFor('search');
		} else {
			// else return to login
			$redirect = $this->routeParser->urlFor('login');
		}
		$this->usermgr->logout();

		$this->flash->addMessage('success', 'Logged out');
		return $response->withHeader('Location', $redirect)
						->withStatus(302);
	}
}
