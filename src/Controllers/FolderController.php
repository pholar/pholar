<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Slim\Exception\HttpForbiddenException;
use Slim\Exception\HttpBadRequestException;

class FolderController extends BaseController
{
	/**
	 * create new folders
	 *
	 * Operates like "mkdir -p $root/$name"
	 *
	 * post param:
	 *  redirect
	 *  root    : path where create new folders
	 *  name    : folders to create
	 *
	 * @httpmethod POST
	 *
	 * @param RouteData $args
	 */
	public function create(Request $request, Response $response, array $args) : Response
	{
		if (is_null($this->user) || !$this->user['can_upload']) {
			throw new HttpForbiddenException($request); // Forbidden
		}
		/* get post data */
		/** @var ParsedData */
		$data = $request->getParsedBody();
		$root = $data['root'] ?? null;
		if (is_null($root)) {
			$this->logger->error("no root param");
			throw new HttpBadRequestException($request, 'Invalid parameters');
		}
		$name = $data['name'] ?? null;
		if (is_null($name)) {
			$this->logger->error("no name param");
			throw new HttpBadRequestException($request, 'Invalid parameters');
		}
		$redirect = $data['redirect'] ?? $this->routeParser->urlFor("search");

		/* sanity checks */
		if (strpos($root . "/" . $name, "..") !== false) {
			$this->logger->error("Invalid path '$root' / '$name'");
			$this->flash->addMessage('error', 'Invalid folder name');
			return $response->withHeader('Location', $redirect)
				->withStatus(302);
		}

		$absroot = $this->photomgr->absPath($root);
		if (!is_dir($absroot)) {
			$this->logger->error("Invalid root path '$root': not a folder");
			throw new HttpBadRequestException($request, 'Invalid parameters');
		}

		/* make dirs */
		$path = $absroot . "/" . $name;
		if (!@mkdir($path, 0777, true)) {
			$this->flash->addMessage('error', 'Error creating new subfolder');
		} else {
			$this->flash->addMessage('success', 'New subfolder created');
		}

		return $response->withHeader('Location', $redirect)
						->withStatus(302);
	}
}
