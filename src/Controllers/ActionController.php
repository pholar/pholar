<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Slim\Exception\HttpForbiddenException;
use Slim\Exception\HttpBadRequestException;

use Pholar\Utils;
use Pholar\Exceptions\ActionException;

use ZipStream;

class ActionController extends BaseController
{
	/**
	 * @param RouteData $args
	 */
	public function download(Request $request, Response $response, array $args) : Response
	{

		/** @var ParsedData */
		$data = $request->getParsedBody();

		$redirect = $data['redirect'] ?? $this->routeParser->urlFor("search");

		$ids = $data['photo'] ?? [];
		if (count($ids) === 0) {
			return $response->withHeader('Location', $redirect)
							->withStatus(302);
		}

		$photos = $this->photomgr->all()->where('id', $ids);

		$downloadfname = "Download-" . date('c') . ".zip";

		// create a new zipstream object
		$zip = new ZipStream\ZipStream(outputName: $downloadfname, sendHttpHeaders: true);

		foreach ($photos as $photo) {
			$sourcefile = $this->photomgr->getFullSourcePath($photo);
			if (file_exists($sourcefile)) {
				$zip->addFileFromPath($photo['filename'], $sourcefile);
			} else {
				$this->logger->error("No source '$sourcefile' for photo id {$photo['id']}");
			}
		}

		// finish the zip stream
		$zip->finish();

		// and die here.
		die();
	}

	/**
	 * @param RouteData $args
	 */
	public function batch(Request $request, Response $response, $args) : Response
	{
		$firstAccept = $request->getHeader('Accept')[0] ?? 'html';
		$isjson = strstr($firstAccept, 'json') > 0;

		if (is_null($this->user)) {
			throw new HttpForbiddenException($request); // Forbidden
		}
	
		/** @var ParsedData */
		$data = $request->getParsedBody();
		
		$action = $data['action'] ?? null;
		if (is_null($action)) {
			$this->logger->error("no batch action");
			throw new HttpBadRequestException($request, 'No action');
		}

		$redirect = $data['redirect'] ?? $this->routeParser->urlFor("search");

		/** @var ?string[] */
		$ids = $data['photo'] ?? null;

		if (is_null($ids)) {
			return $response->withHeader('Location', $redirect)
							->withStatus(302);
		}

		$this->logger->info("batch $action", $data);
		// TODO pass update url and data to poussetaches

		$successmessage = "Done.";
		try {
			switch ($action) {
				case "download":
					return $this->download($request, $response, $args);
				case "update":
					foreach ($ids as $id) {
						$this->doUpdate(null, (int) $id);
					}
					$successmessage = "Updated " . count($ids) . " photos";
					break;
				case "delete":
					$this->doDelete($ids);
					$successmessage = "Deleted " . count($ids) . " photos";
					break;
				case "addtags":
					$tagstr = $data['tagstr'];
					foreach ($ids as $id) {
						$this->doAddTag((int) $id, $tagstr);
					}
					$successmessage = "Tags added";
					break;
				case "addtocollection":
					switch ($data['subaction']) {
						case "appendto":
							$collectionid = $data['collectionid'];
							$successmessage = " added to collection";
							break;
						case "addnew":
							$collection = $data['collname'];
							$comment = $data['collcomment'];
							$collectionid = $this->collectionmgr->add($collection, $comment);
							$successmessage = " added to new collection";
							break;
						default:
							$estr = "invalid batch subaction '$action' '{$data['subaction']}'";
							throw new ActionException(400, $estr);
					}
					foreach ($ids as $id) {
						$this->doAddToCollection((int) $id, $collectionid);
					}
					$successmessage = count($ids) . " photos " . $successmessage;
					break;
				case "removefromcollection":
					$collectionid = $data['removefromcollectionid'];
					foreach ($ids as $id) {
						$this->doRemoveFromCollection((int) $id, (int) $collectionid);
					}
					$successmessage = count($ids) . " photos removed from collection";
					break;
				default:
					$estr = "invalid batch action '$action'";
					throw new ActionException(400, $estr);
			}
		} catch (ActionException $e) {
			$this->logger->error("batch error on '$action'", [$e->getMessage()]);
			$response = $response->withStatus($e->getHTTPCode());
			if ($isjson) {
				return $this->toJson($response, [
									"errorcode"=>$e->getHTTPCode(),
									"action"=>$action,
									"message"=>$e->getMessage()
								]);
			} else {
				$this->flash->addMessage('error', $e->getMessage());
				return $response->withHeader('Location', $redirect)
								->withStatus(302);
			}
		}

		$this->flash->addMessage('success', $successmessage);
		return $response->withHeader('Location', $redirect)
						->withStatus(302);
	}



	/**
	 *  Add a photo to a collection
	 *
	 *  @param int $photoid Photo id
	 *  @param int $collectionid Collection id
	 */
	private function doAddToCollection(int $photoid, int $collectionid) : void
	{
		$photo = $this->photomgr->byId($photoid);
		if (is_null($photo)) {
			throw new ActionException(404, "Photo $photoid not found");
		}

		$this->collectionmgr->addPhoto($collectionid, $photoid);
		$this->photomgr->save($photo);
	}

	/**
	 * Remove a photo from a collection
	 *
	 * @param int $photoid      Photo id
	 * @param int $collectionid Collection id
	 */
	private function doRemoveFromCollection(int $photoid, int $collectionid) : void
	{
		$photo = $this->photomgr->byId($photoid);
		if (is_null($photo)) {
			throw new ActionException(404, "Photo $photoid not found");
		}

		$this->collectionmgr->removePhoto($collectionid, $photoid);
		$this->photomgr->save($photo);
	}

	/**
	 * Add tags to a photo
	 *
	 * @param int $id      Photo id
	 * @param string $tags Tag names, comma separated
	 */
	private function doAddTag(int $id, string $tags) : void
	{
		$taglist = array_map('trim', explode(",", $tags));
		$photo = $this->photomgr->byId($id);
		if (is_null($photo)) {
			throw new ActionException(404, "Photo $id not found");
		}
		foreach ($taglist as $tag) {
			$this->photomgr->addTag($photo, $tag);
		}
		$this->photomgr->save($photo);
	}

	/**
	 * Insert or update one photo from storage dir or from photo id
	 *
	 * Pass only one parameter, set the other as null
	 * TODO: this should be done better
	 *
	 * @param ?string $filename Source file to scan
	 * @param ?int $id          Photo id to rescan
	 *
	 * @throws ActionException
	 */
	private function doUpdate(string $filename = null, int $id = null) : void
	{
		$this->logger->info("doUpdate", [$filename, $id]);
		if (!is_null($id)) {
			$photo = $this->photomgr->byId($id);
			if (is_null($photo)) {
				throw new ActionException(404, "Photo $id not found");
			}
			$filename = $photo['location'] . "/" . $photo['filename'];
			$filename = $this->photomgr->absPath($filename);
		}
		
		if (!Utils::startsWith($filename, $this->settings->get('app', 'storage'))) {
			$this->logger->error("doUpdate: File $filename not in STORAGEDIR");
			throw new ActionException(400, "File $filename not in storage");
		}
		
		if (!file_exists($filename)) {
			// TODO: should we check if we have the photo in db
			//       and delete it?
			$this->logger->error("doUpdate: File $filename does not exists");
			throw new ActionException(400, "File $filename does not exists");
		}

		try {
			$id = $this->photomgr->update($filename);
		} catch (\PDOException $e) {
			$this->logger->error("doUpdate: Query error " . $e->getMessage());
			throw new ActionException(500, "Internal Server Error");
		}

		if (is_null($id)) {
			// photo was ignored, return quietly
			return;
		}

		$this->photomgr->scheduleExtract($id);
		$this->photomgr->scheduleThumbs($id);
		$this->photomgr->scheduleFaces($id);
	}

	/**
	 * Delete a batch of records
	 *
	 * @param array<int|string> $ids
	 */
	private function doDelete(array $ids) : void
	{
		$photos = $this->photomgr->all()->where("id", $ids);

		try {
			foreach ($photos as $photo) {
				// TODO: delete photo in transaction
				$this->photomgr->delete($photo);
				$this->photomgr->thumbDeleteById($photo['id']);
			}
		} catch (\PDOException $e) {
			$this->logger->error("doDelete: Query error " . $e->getMessage());
			throw new ActionException(500, "Internal Server Error");
		}
	}


	/**
	 * @param RouteData $args
	 */
	public function update(Request $request, Response $response, array $args) : Response
	{
		// TODO: at least a fixed token auth to call this view from command line
		$firstAccept = $request->getHeader('Accept')[0] ?? 'html';
		$isjson = strpos($firstAccept, 'json') > 0;

		/** @var ParsedData */
		$data = $request->getParsedBody();
		$filename = null;
		$redirect = null;
		$id = $data['id'] ?? null;

		if (!is_null($id)) {
			$redirect = $this->routeParser->urlFor("details", ['id'=>$id]);
			$redirect = $data['redirect'] ?? $redirect;
		} else {
			$filename = $data['file'] ?? null;
			if (is_null($filename)) {
				$this->logger->error("update: No file name in request");
				return $response->withStatus(400); // invalid request
			}
		}

		try {
			$this->doUpdate($filename, (int) $id);
		} catch (ActionException $e) {
			$response = $response->withStatus($e->getHTTPCode());
			if ($isjson) {
				$response = $this->toJson($response, [
						"errorcode" => $e->getHTTPCode(),
						"message" => $e->getMessage()
					]);
			} else {
				$response->getBody()->write($e->getMessage());
			}
			return $response;
		}


		if (!is_null($redirect)) {
			return $response->withHeader('Location', $redirect)
							->withStatus(302);
		}

		if ($isjson) {
			$response->getBody()->write('"Ok"');
		}
		return $response;
	}
}
