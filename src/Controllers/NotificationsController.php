<?php
/*
	Copyright (c) 2020-2024 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Controllers;

use Pholar\Tasks\Status;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Code related to tags: edit, delete
 */
class NotificationsController extends BaseController
{
	/**
	 * Poll notifications
	 *
	 * @httpmethod GET
	 *
	 * @param RouteData $args
	 */
	public function poll(Request $request, Response $response, $args) : Response
	{
		$data = [];

		if ($this->user) {
			// get pending tasks
			$data['tasks'] = $tasks = $this->taskmgr->getTasks()
					->where('status', [
						Status::PENDING, Status::RUNNING
					])
					->fetchAll();
		}

		return $this->toJson($response, $data);
	}
}
