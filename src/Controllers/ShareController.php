<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Slim\Exception\HttpNotFoundException;

use Envms\FluentPDO\Queries\Select;

use Pholar\PhotoMgr;
use Pholar\Exceptions\HttpRedirect;

class ShareController extends PhotoController
{
	protected function setUser() : void
	{
		$this->usermgr->loginGuest();
		$this->user = $this->usermgr->getForSession();
	}

	/**
	 * @param RouteData $args
	 */
	protected function setup(Request $request, Response $response, array $args) : void
	{
		parent::setup($request, $response, $args);
		$sid = $args['sid'] ?? null;
		$this->ctx['shareid'] = $sid;
		$this->ctx['searchroute'] = 'sharesearch';
		$this->ctx['detailroute'] = 'sharedetails';
		$this->ctx['sourceroute'] = 'sharesource';

		$share = $this->sharemgr->byUID($sid);
		if (is_null($share)) {
			throw new HttpNotFoundException($request, "Share not found");
		}
		$this->ctx['share'] = $share;

		$now = new \DateTime();
		if (!is_null($share['valid_from']) && $now < $share['valid_from']) {
			throw new HttpNotFoundException($request, "Invalid share");
		}
		if (!is_null($share['valid_until']) && $now > $share['valid_until']) {
			throw new HttpNotFoundException($request, "Invalid share");
		}

		$can_view_share = $_SESSION[$sid] ?? false;
		if (!is_null($share['password']) && !$can_view_share) {
			$redirect = $this->routeParser->urlFor('sharelogin', ['sid' => $sid]);

			// HttpRedirect is catched in \Pholar\Middleware\HttpExceptionsMiddleware
			// (see comment there for more details)
			throw new HttpRedirect($redirect);
		}

		$coll = $this->collectionmgr->byId($share['collection_id']);
		if (is_null($coll)) {
			throw new HttpNotFoundException($request, "Shared collection not found");
		}
		$this->ctx['collection'] = $coll;

		// overwrite user permissions
		$this->user['can_download'] = $share['can_download'];
		$this->user['can_details'] = $share['can_details'];
	}

	/**
	 * List photos in current shared collection
	 */
	protected function getObjects(string $extraquery = "") : Select
	{
		$cid = $this->ctx['collection']['id'];

		$phototable = PhotoMgr::getTable();
		$quotedphototable = $this->sqldriver->quote($phototable);
		$photos = parent::getObjects($extraquery);
		$photos->leftJoin(sprintf('photo_collection ON photo_collection.photo_id = %s.id', $quotedphototable));
		$photos->where("collection_id", $cid);
		return $photos;
	}

	/**
	 * Get a photo by id in current shared collection
	 *
	 * @param RouteData $args
	 * @return PhotoData
	 */
	protected function getObject(Request $request, Response $response, array $args) : array
	{
		$cid = $this->ctx['collection']['id'];
		$id = intval($args['id']);
		$photo = $this->collectionmgr->photoById($cid, $id);
		if (is_null($photo)) {
			throw new HttpNotFoundException($request, "Photo not found");
		}

		return $photo;
	}

	/**
	 * Search in shared collection
	 *
	 * @httpmethod GET
	 *
	 * @param RouteData $args
	 */
	public function search(Request $request, Response $response, $args) : Response
	{
		$this->setup($request, $response, $args);
		return $this->_search($request, $response, $args);
	}

	/**
	 * Handle share password input
	 *
	 * @httpmethod POST
	 *
	 * @param RouteData $args
	 */
	public function password(Request $request, Response $response, $args) : Response
	{
		try {
			$this->setup($request, $response, $args);
		} catch (HttpRedirect $e) {
			// pass
		}
		unset($_SESSION[$this->ctx['shareid']]);
		
		$password = $request->getParsedBody()['password'] ?? null;
		if (is_null($password)) {
			return $this->view->render($response, 'share/password.twig', $this->ctx);
		}

		$can_view_share = $this->sharemgr->checkPassword($this->ctx['share'], $password);
		if ($can_view_share) {
			$_SESSION[$this->ctx['shareid']] = true;
			$redirect = $this->routeParser->urlFor('sharesearch', [
				'sid' => $this->ctx['shareid']
			]);
		} else {
			unset($_SESSION[$this->ctx['shareid']]);
			$redirect = $this->routeParser->urlFor('sharelogin', [
				'sid' => $this->ctx['shareid']
			]);
			$this->flash->addMessage('error', 'Invalid password');
		}

		return $response->withStatus(302)
			->withHeader('Location', $redirect);
	}
}
