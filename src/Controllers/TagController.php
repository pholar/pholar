<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Code related to tags: edit, delete
 */
class TagController extends BaseController
{
	/**
	 * Rename tag
	 *
	 * @httpmethod POST
	 * @form tag: new tag name, redirect: url
	 * @args tid: tag id
	 *
	 * @param RouteData $args
	 */
	public function rename(Request $request, Response $response, $args) : Response
	{
		if (is_null($this->user) || !$this->user['can_edit']) {
			$response = $response->withStatus(403); // Forbidden
		}
		/** @var ParsedData */
		$data = $request->getParsedBody();
		$id = $args['tid'] ?? null;
		$name = $data['tag'] ?? null;
		$redirect = $data['redirect'] ?? $this->routeParser->urlFor("search");

		if (is_null($id) || is_null($name)) {
			return $response->withStatus(400); // Bad Request
		}
		$id = (int) $id;

		$name = trim($name);
		if ($name === "") {
			$this->flash->addMessage('error', "Tag name can't be empty");
			return $response->withHeader('Location', $redirect)
							->withStatus(302);
		}

		$tag = $this->tagmgr->byId($id);
		if (is_null($tag)) {
			$this->flash->addMessage('error', "Tag not found");
			return $response->withHeader('Location', $redirect)
							->withStatus(302);
		}

		$oldname = $tag['tag'];
		$tag['tag'] = $name;

		try {
			$this->tagmgr->save($tag);
		} catch (\PDOException $e) {
			$this->flash->addMessage('error', "Error saving tag");
			return $response->withHeader('Location', $redirect)
							->withStatus(302);
		}

		// Save photos
		// TODO: poussetache this
		foreach ($this->tagmgr->getPhotosFor($tag['id']) as $photo) {
			$this->photomgr->save($photo);
		}


		// redirect query to new tag name. sa di cacata, ma vabè
		$redirect = str_replace(urlencode('tag:"'.$oldname), urlencode('tag:"'.$name), $redirect);
		$redirect = str_replace(urlencode('tag:'.$oldname), urlencode('tag:'.$name), $redirect);
		return $response->withHeader('Location', $redirect)
						->withStatus(302);
	}

	/**
	 * Rename tag
	 *
	 * @httpmethod DELETE
	 * @args tid: tag id
	 *
	 * @param RouteData $args
	 */
	public function delete(Request $request, Response $response, $args) : Response
	{
		if (is_null($this->user) || !$this->user['can_edit']) {
			$response = $response->withStatus(403); // Forbidden
		}

		/** @var ParsedData */
		$data = $request->getParsedBody();
		$redirect = $data['redirect'] ?? $this->routeParser->urlFor("search");

		$id = $args['tid'] ?? null;
		if (is_null($id) || !is_numeric($id)) {
			return $response->withStatus(400); // Bad Request
		}
		$id = (int) $id;
		$photos = $this->tagmgr->getPhotosFor($id)->fetchAll();

		$this->tagmgr->delete($id);

		// Save photos
		// TODO: poussetache this
		foreach ($photos as $photo) {
			$this->photomgr->save($photo);
		}

		return $response->withHeader('Location', $redirect)
						->withStatus(302);
	}
}
