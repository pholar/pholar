<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Controllers\Admin;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Symfony\Component\Process\ExecutableFinder;
use Symfony\Component\Process\Exception\ProcessFailedException;

use Pholar\Controllers\BaseController;

use Pholar\Utils;

class AdminController extends BaseController
{

	/**
	 * Indev view of admin panel
	 *
	 * @httpmethod GET
	 * @param RouteData $args
	 */
	public function index(Request $request, Response $response, array $args) : Response
	{
		// some numbers
		$photos = $this->db->from('photo')->count();
		$tags = $this->db->from('tag')->count();
		$collections = $this->db->from('collection')->count();
		$sharedcolls = $this->db->from('share')
					->where('valid_until is NULL')
					->whereOr('valid_until > date("now")')
					->count();
		$users = $this->db->from('user')->count();

		if ($this->settings->get('dsn', 'scheme') == "sqlite") {
			$dbsize = filesize($this->settings->get('dsn', 'path'));
			$dbsize = \Pholar\Fields\Bytes::display('db size', $dbsize, $this->view->getEnvironment());
		} else {
			$dbsize = "<i>TODO</i>"; # TODO
		}
		$storesize = $this->db->from('photo')->select('SUM(filesize)')->fetch('SUM(filesize)');
		$storesize = \Pholar\Fields\Bytes::display('store size', $storesize, $this->view->getEnvironment());

		$lastupdate = file_get_contents($this->settings->get('lastupdatefile'));

		# TODO: replace this with proper settings page
		$settings_env = $this->settings->getEnv();
		$settings = $this->settings->get('app');
		$settings['db driver'] = $this->settings->get('dsn')['scheme'];
		
		$phpinfo = [];
		$phpinfo['version'] = phpversion();
		$requiredexts = ['gd','imagick', 'pdo', 'pdo_sqlite', 'xml', 'curl', 'mbstring', 'zip', 'json', 'inotify'];
		foreach ($requiredexts as $ext) {
			$phpinfo[$ext] = phpversion($ext);
		}
		$phpinfo['max upload'] = ini_get('upload_max_filesize');
		$phpinfo['max post'] = ini_get('post_max_size');
		$phpinfo['memory limit'] = ini_get('memory_limit');
		

		$exeinfo = [];
		$externalexes = [
			['perl', '--version'],
			['sqlite3', '--version'],
			['magick', '--version'],
			['file', '--version'],
			['ffmpeg', '-version'],
			['soffice', '--version'],
			['gs', '--version'],
		];
		$exefinder = new ExecutableFinder();
		foreach ($externalexes as $extexe) {
			[$exe, $opt] = $extexe;
			$bin = $exefinder->find($exe);
			if (is_null($bin)) {
				$exeinfo[$exe] = "not found";
			} else {
				$ver = explode("\n", Utils::runcmd([$bin, $opt]))[0];
				$exeinfo[$bin] = $ver;
			}
		}

		// facetool
		if ($this->settings->get('facetool', 'enabled')) {
			$ftcmd = $this->settings->get('facetool', 'commandline');
			$ftxists = $ftcmd[0][0] == "/"
						? file_exists($ftcmd[0])
						: !is_null($exefinder->find($ftcmd[0]));
			if ($ftxists) {
				try {
					$ver = explode("\n", Utils::runcmd(array_merge($ftcmd, ["--version"])))[0];
					$exeinfo['facetool'] = $ver;
				} catch (ProcessFailedException $e) {
					$exeinfo['facetool'] = explode("\n", $e->getMessage())[0];
				}
			} else {
				$exeinfo['facetool'] = "not found";
			}
		} else {
			$exeinfo['facetool'] = "disabled";
		}



		return $this->view->render($response, 'admin/admin.twig', [
			'adminpage' => 'index',
			'photos' => $photos,
			'tags' => $tags,
			'collections' => $collections,
			'shares' => $sharedcolls,
			'users' => $users,
			'dbsize' => $dbsize,
			'storesize' => $storesize,
			'lastupdate' => $lastupdate,
			'settings_env' => $settings_env,
			'settings' => $settings,
			'version' => \Pholar\App::getVersion(),
			'phpinfo' => $phpinfo,
			'exeinfo' => $exeinfo,
		]);
	}


	/**
	 * Raw SQL select
	 *
	 * @httpmethod POST
	 * @param RouteData $args
	 */
	public function advancedQuery(Request $request, Response $response, array $args) : Response
	{
		$params = $request->getParsedBody();
		$erromessages = [];
		$q = "SELECT * FROM photo";
		if (isset($params['q'])) {
			$q = trim($params['q'], ";\t ");
			if (!Utils::startsWith(strtolower($q), "select")) {
				$this->flash->addMessageNow('error', 'Only SELECT queries allowed');
				$q = "SELECT * FROM photo";
			}
		}

		if (strpos(strtolower($q), "limit") === false) {
			$q = $q . " LIMIT 100";
		}

		$sqlerror = false;
		$headers = [];

		$timerstart = microtime(true);
		$photos = [];
		try {
			$photos = $this->db->getPdo()->query($q, \PDO::FETCH_ASSOC);
		} catch (\PDOException $e) {
			$sqlerror = $e->getMessage();
		}
		$querytime = number_format(microtime(true) - $timerstart, 4);

		if ($sqlerror === false) {
			try {
				for ($k = 0; $k < $photos->columnCount(); $k++) {
					$m = $photos->getColumnMeta($k);

					if (isset($m['table'])) {
						$headers[$k] = $m['table'] . "." . $m['name'];
					} else {
						$headers[$k] = $m['name'];
					}
				}
			} catch (\PDOException $e) {
				$sqlerror = $e->getMessage();
			}
		}
		//dd($q, $photos->columnCount(), $headers);

		return $this->view->render($response, 'admin/advanced.twig', [
			'adminpage' => 'sql',
			'where' => $q,
			'querytime' => $querytime,
			'sqlerror' => $sqlerror,
			'headers' => $headers,
			'photos' => $photos
		]);
	}
}
