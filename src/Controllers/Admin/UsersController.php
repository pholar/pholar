<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Controllers\Admin;

use Pholar\Controllers\BaseController;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class UsersController extends BaseController
{
	/**
	 * List users
	 *
	 * @httpmethod GET
	 * @param RouteData $args
	 */
	public function list(Request $request, Response $response, array $args) : Response
	{
		$objects = $this->db->from('user')->orderBy('user ASC')->execute();

		$headers = [];
		for ($k = 0; $k < $objects->columnCount(); $k++) {
			$m = $objects->getColumnMeta($k);
			if ($m['name'] !== "password") {
				$headers[$k] = $m['name'];
			}
		}

		return $this->view->render($response, 'admin/users.list.twig', [
			'adminpage' => 'users',
			'headers' => $headers,
			'objects' => $objects
		]);
	}

	/**
	 * User edit page
	 *
	 * @metod GET
	 * @param RouteData $args
	 */
	public function edit(Request $request, Response $response, array $args) : Response
	{
		$user = $this->usermgr->getByName($args['user'] ?? null);

		if (is_null($user)) {
			$this->flash->addMessage('error', "Unkown user '" . $args['user'] . "'");
			return $response->withHeader('Location', $this->routeParser->urlFor('admin.users'))
							->withStatus(302);
		}

		return $this->view->render($response, 'admin/users.edit.twig', [
			'adminpage' => 'users',
			'title' => $user['user'],
			'action' => 'update',
			'object' => $user
		]);
	}

	/**
	 * New user page
	 *
	 * @httpmethod GET
	 * @param RouteData $args
	 */
	public function new(Request $request, Response $response, array $args) : Response
	{
		return $this->view->render($response, 'admin/users.edit.twig', [
			'adminpage' => 'users',
			'title' => 'New user',
			'action' => 'create',
			'object' => $this->usermgr->getDefaultPermissions()
		]);
	}

	/**
	 * Save user
	 *
	 * @httpmethod POST
	 */
	public function save(Request $request, Response $response) : Response
	{
		/** @var ParsedData */
		$data = $request->getParsedBody();

		$action = $data['action'];
		unset($data['action']);

		$data['user'] = trim($data['user']);

		try {
			if ($action === 'delete') {
				// Delete user
				$this->usermgr->delete($data['user']);
				$redirect = $this->routeParser->urlFor('admin.users');
				$this->flash->addMessage('success', "User deleted");
			} else {
				// Create or Edit user

				// set user permissions
				// (if exists in $data['can_...'] -> $user['can_....] = true)
				foreach ($this->usermgr->getDefaultPermissions() as $k => $v) {
					$data[$k] = array_key_exists($k, $data);
				}

				// check if two password imputs match
				if ($data['password'] !== $data['password2']) {
					// if not, redirect to correct view with error message
					if ($action == "create") {
						$redirect = $this->routeParser->urlFor('admin.users.new');
					} else {
						$redirect = $this->routeParser->urlFor('admin.users.edit', ['user'=>$data['user']]);
					}
					$this->flash->addMessage('error', "Passwords must match!");
					return $response->withHeader('Location', $redirect)
									->withStatus(302);
				}
				// remove password2
				// hash password if set, if not, remove it (we don't want to update password to null)
				unset($data['password2']);
				if (trim($data['password']) !== "") {
					$data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
				} else {
					unset($data['password']);
				}
				$this->usermgr->save($data);

				// create user view on photo table
				$this->photomgr->createViewForUser($data);

				// succes!
				$redirect = $this->routeParser->urlFor('admin.users.edit', ['user'=>$data['user']]);
				$this->flash->addMessage('success', "User saved");
			}
		} catch (\Exception $e) {
			$this->logger->error($e);
			$this->flash->addMessage('error', "An error occured while saving");
			return $response->withHeader('Location', $this->routeParser->urlFor('admin.users'))
							->withStatus(302);
		}

		return $response->withHeader('Location', $redirect)
						->withStatus(302);
	}
}
