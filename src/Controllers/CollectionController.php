<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Slim\Exception\HttpNotFoundException;
use Envms\FluentPDO\Queries\Select;

use Pholar\PhotoMgr;

class CollectionController extends PhotoController
{
	/**
	 * @param RouteData $args
	 */
	protected function setup(Request $request, Response $response, array $args) : void
	{
		parent::setup($request, $response, $args);
		$cid = $args['cid'] ?? null;
		$this->ctx['collectionid'] = $cid;
		if (is_null($cid) || !is_numeric($cid)) {
			throw new HttpNotFoundException($request, "Collection not found");
		}
		$this->ctx['searchroute'] = 'collectionsearch';
		$this->ctx['detailroute'] = 'collectiondetails';
		$collection = $this->collectionmgr->byId((int) $cid);
		if (is_null($collection)) {
			throw new HttpNotFoundException($request, "Collection not found");
		}
		$this->ctx['collection'] = $collection;
	}

	/**
	 * Get photos in this collection
	 */
	protected function getObjects(string $extraquery = "") : Select
	{
		$cid = $this->ctx['collectionid'];
		$shares = $this->sharemgr->getForCollection($cid);

		if (!$this->user['is_admin']) {
			$shares = $shares->where('user', $this->user['user']);
		}
		$this->ctx['shares'] = $shares;

		$phototable = PhotoMgr::getTable();
		$quotedphototable = $this->sqldriver->quote($phototable);

		$photos = parent::getObjects($extraquery);
		$photos->leftJoin(sprintf('photo_collection ON photo_collection.photo_id = %s.id', $quotedphototable));
		$photos->where("collection_id", $cid);
		return $photos;
	}

	/**
	 * Get single photo by id in this collection
	 *
	 * @param RouteData $args
	 * @return PhotoData
	 */
	protected function getObject(Request $request, Response $response, array $args) : array
	{
		$cid = intval($args['cid']);
		$id = intval($args['id']);
		$photo = $this->collectionmgr->photoById($cid, $id);
		if (is_null($photo)) {
			throw new HttpNotFoundException($request, "Photo not found");
		}

		return $photo;
	}

	/**
	 * View photos in this collection
	 *
	 * @httpmethod GET
	 * @param RouteData $args
	 */
	public function search(Request $request, Response $response, array $args) : Response
	{
		$this->setup($request, $response, $args);
		return $this->_search($request, $response, $args);
	}

	/**
	 * Edit collection details
	 *
	 * @httpmethod POST
	 * @form action: 'editcollection'|'deletecollection', collname: collection name, collcomment: collection comment
	 *
	 * @param RouteData $args
	 */
	public function edit(Request $request, Response $response, array $args) : Response
	{
		if (is_null($this->user) || !$this->user['can_edit']) {
			return $response->withStatus(403); // Forbidden
		}
		$this->setup($request, $response, $args);
		$collection = $this->collectionmgr->byId($this->ctx['collectionid']);

		if (is_null($collection)) {
			return $response->withStatus(404); // not found
		}

		/** @var ParsedData */
		$data = $request->getParsedBody();
		$action = $data['action'];
		$redirect = $data['redirect'];
		switch ($action) {
			case "editcollection":
				$oldname = $collection['name'];
				$collection['name'] = $data['collname'] ?? $collection['name'];
				$collection['comment'] = $data['collcomment'] ?? $collection['comment'];
				$this->collectionmgr->save($collection);

				if ($collection['name'] != $oldname) {
					// we store collection name as a tag in photos, so if name change, we need
					// to save all photos in collection
					foreach ($this->getObjects() as $photo) {
						$this->photomgr->save($photo);
					}
				}

				$this->flash->addMessage('success', 'Collection saved');
				break;
			case "deletecollection":
				$this->collectionmgr->delete($collection['id']);
				$this->flash->addMessage('success', 'Collection deleted');
				$redirect = $this->routeParser->urlFor('search');
				break;
			default:
				$this->flash->addMessage('error', 'Invalid action');
		}


		return $response->withHeader('Location', $redirect)
						->withStatus(302);
	}

	/**
	 * Create a share from this collection
	 *
	 * TODO: share views should be in separated controller (but not ShareController.. ShareEditController is better)
	 *
	 * @httpmethod POST
	 *
	 * @param RouteData $args
	 */
	public function createShare(Request $request, Response $response, array $args) : Response
	{
		if (is_null($this->user) || !$this->user['can_share']) {
			return $response->withStatus(403); // Forbidden
		}

		$cid = $args['cid'] ?? null;

		if (is_null($cid) || !is_numeric($cid)) {
			throw new HttpNotFoundException($request, "Collection not found");
		}
		$cid = (int) $cid;
		$collection = $this->collectionmgr->byId($cid);
		if (is_null($collection)) {
			return $response->withStatus(404); // not found
		}

		/** @var ParsedData */
		$data = $request->getParsedBody();


		$password = $data['password'] ?? '';
		if ($password === '') {
			$password = null;
		}

		$valid_from = $data['valid_from'] ?? '';
		$valid_until = $data['valid_until'] ?? '';
		$valid_from = ($valid_from === '') ? null : \DateTime::createFromFormat('!Y-m-d', $valid_from);
		$valid_until = ($valid_until === '') ? null : \DateTime::createFromFormat('!Y-m-d', $valid_until);

		$can_download = ($data['can_download'] ?? null) === "on";
		$can_details = ($data['can_details'] ?? null) === "on";

		$shareuid = $this->sharemgr->create(
			$this->user['user'],
			$cid,
			$valid_from,
			$valid_until,
			$can_download,
			$can_details,
			$password
		);

		$redirect = $data['redirect'] . "#share-" . $shareuid;
		return $response->withHeader('Location', $redirect)
						->withStatus(302);
	}

	/**
	 * Edit collectio share
	 *
	 * @methos POST
	 *
	 * @param RouteData $args
	 */
	public function editShare(Request $request, Response $response, array $args) : Response
	{
		if (is_null($this->user) || !$this->user['can_share']) {
			return $response->withStatus(403); // Forbidden
		}

		$shareuid = $args['suid'];
		/** @var ParsedData */
		$data = $request->getParsedBody();
		switch ($data['action'] ?? null) {
			case "remove":
				$this->sharemgr->delete($shareuid);
				$this->flash->addMessage('success', "Share removed");
				break;
			case "edit":
				$share = $this->sharemgr->byUID($shareuid);
				if (is_null($share)) {
					// TODO: should this raise a 404?
					$this->flash->addMessage('error', "Invalid share");
				} else {
					$valid_from = $data['valid_from'] ?? '';
					$valid_until = $data['valid_until'] ?? '';
					$share['valid_from'] = ($valid_from === '')
										 ? null
										 : \DateTime::createFromFormat('!Y-m-d', $valid_from);
					$share['valid_until'] = ($valid_until === '')
										  ? null
										  : \DateTime::createFromFormat('!Y-m-d', $valid_until);
					$share['can_download'] = ($data['can_download'] ?? null) === "on";
					$share['can_details'] = ($data['can_details'] ?? null) === "on";

					$this->sharemgr->save($share);

					$this->flash->addMessage('success', "Share edited");
				}

				break;
		}


		$redirect = $data['redirect'];
		return $response->withHeader('Location', $redirect)
						->withStatus(302);
	}
}
