<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Psr7\Stream;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpForbiddenException;

use Envms\FluentPDO\Queries\Select;

use Pholar\PhotoMgr;
use Pholar\Pagination;
use Pholar\Utils;
use Pholar\Exceptions\SearchException;

/**
 * Code related to photos: search, insert, remove, update
 */
class PhotoController extends BaseController
{
	/** @var Context */
	protected $ctx = [];

	/**
	 * @param RouteData $args
	 */
	protected function setup(Request $request, Response $response, array $args) : void
	{
		$params = $request->getQueryParams();
		$q = $params['q'] ?? null;
		$p = $params['p'] ?? null;
		$o = $params['o'] ?? null;
		$d = $params['d'] ?? null;
		if (!is_null($o) && !is_null($d)) {
			$o = $o . " " . $d;
		}

		$oextra = explode(' ', $o ?? $this->settings->get('app', 'orderby'));

		$this->ctx = [
			// context dependant routes
			'searchroute' => 'search',
			'detailroute' => 'details',
			'sourceroute' => 'source',
			// state
			'query' => $q,
			'page' => $p,
			'order' => $o,
			'order_field' => $oextra[0],
			'order_direction' => $oextra[1],
		];
	}

	/**
	 * Render template $template with $this->ctx merged with $ctx
	 *
	 * @param string $template Template name
	 * @param Context $ctx Extra context
	 */
	protected function render(Response $response, string $template, array $ctx = []) : Response
	{
		$response = $response->withHeader('Content-Type', "text/html");
		$context = array_merge($this->ctx, $ctx);
		return $this->view->render($response, $template, $context);
	}

	/**
	 * Get objects
	 */
	protected function getObjects(string $extraquery = "") : Select
	{
		$query = $this->ctx['query'];
		$query .= " " . $extraquery;

		$phototable = PhotoMgr::getTable();
		$quotedphototable = $this->sqldriver->quote($phototable);

		$orderby = $this->ctx['order'] ?? $this->settings->get('app', 'orderby');
		$orderby = "$quotedphototable.$orderby";

		return $this->photomgr->filter($query, $orderby);
	}

	/**
	 * @param RouteData $args
	 * @return PhotoData
	 */
	protected function getObject(Request $request, Response $response, array $args) : array
	{
		$id = intval($args['id']);
		$photo = $this->photomgr->byId($id);
		if (is_null($photo)) {
			throw new HttpNotFoundException($request, "Photo not found");
		}

		return $photo;
	}

	/**
	 * search page
	 *
	 * @httpmethod GET
	 * @param RouteData $args
	 */
	public function search(Request $request, Response $response, array $args) : Response
	{
		$this->setup($request, $response, $args);

		$valid_widgets = ['folders', 'tags', 'people'];
		/* widgets are only on plain search page */
		$widgets = [];
		foreach ($this->settings->get('app', 'widgets') as $widget) {
			if (in_array($widget, $valid_widgets)) {
				$widgets[$widget] = $this->widget($widget, $request);
			}
		};
		$this->ctx['widgets'] = $widgets;

		return $this->_search($request, $response, $args);
	}

	/**
	 * shared "search" route logic
	 *
	 * @param RouteData $args
	 */
	protected function _search(Request $request, Response $response, array $args) : Response
	{
		try {
			$photos = $this->getObjects();
		} catch (SearchException $e) {
			$photos = $this->photomgr->all()->where('id', -1);
			$this->flash->addMessageNow('error', $e->getMessage());
		}

		#dd($photos->getQuery(), $photos->getParameters());
		$pagination = new Pagination($photos, $request);


		// extract tags from query
		$tags = [];
		$tokens = Utils::tokenize($this->ctx['query']);
		foreach ($tokens as $tok) {
			if (is_array($tok) && $tok[0]=='tag') {
				$tags[] = $tok[1];
			}
		}
		if (count($tags) > 0) {
			$tags = $this->tagmgr->byName($tags)->fetchAll();
		}

		return $this->render($response, 'photos.twig', [
			'pagination' => $pagination,
			'photos' => $photos,
			'tags' => $tags
		]);
	}

	/**
	 * @param RouteData $args
	 */
	public function details(Request $request, Response $response, array $args) : Response
	{
		$this->setup($request, $response, $args);

		if (is_null($this->user) || $this->user['can_details'] === false) {
			// if 'can_details' permission is set and is exactly false
			// means this is a share with explicitly uncheck 'can see details'
			// so we stop here
			throw new HttpForbiddenException($request, "Forbidden");
		}

		$photo = $this->getObject($request, $response, $args);

		###################### START prev / next
		$pdo = $this->db->getPdo();

		// get original query and parameters form `$this->getObjects()`
		$photos = $this->getObjects();
		$q = $photos->getQuery();
		$p = $photos->getParameters();

		// build the query. We sprintf-in the photo id because we are quite sure it's safe
		$querypn = $this->sqldriver->prevnextQuery($q, $photo['id']);
		$prev = null;
		$next = null;
		if (!is_null($querypn)) {
			$sth = $pdo->prepare($querypn);
			$sth->execute($p);
			$pn = $sth->fetch(\PDO::FETCH_ASSOC);
			// TODO: Check why $pn is null?
			// on sqlite, request 'photo/6431?q=latitude:"0.0"'
			if ($pn !== false) {
				$prev = $pn['prev'] == $photo['id'] ? null : $pn['prev'];
				$next = $pn['next'] == $photo['id'] ? null : $pn['next'];
			}
		}
		###################### END prev / next


		list($mimemaj, $mimemin) = explode("/", $photo['format']);
		$template = "details.twig";
		switch ($mimemaj) {
			case "image":
				$template = "image.twig";
				break;
			case "video":
				$template = "video.twig";
				break;
			case "audio":
				$template = "audio.twig";
				break;
		}
		if ($photo['format'] === "application/pdf") {
			$template = "pdf.twig";
		}
		
		$sourcefile = $this->photomgr->getFullSourcePath($photo);
		# external path to source file
		$extpath = $this->settings->get('app', 'expath') ?? "";
		if ($extpath !== "") {
			$loc = trim($photo['location'], '/');
			$loc = ($loc === "") ? "/" :  "/$loc/";
			$extpath = $extpath . $loc . $photo['filename'];
		}

		# external path to source file for windows (if defined)
		$extpath_win = $this->settings->get('app', 'expath_win');
		if (!is_null($extpath_win)) {
			$loc = trim($photo['location'], '/');
			$loc = ($loc === "") ? "/" :  "/$loc/";
			$extpath_win = $extpath_win . $loc . $photo['filename'];
			$extpath_win = str_replace('/', '\\', $extpath_win);
		}


		# similar photos
		if ($photo['hash'] != "") {
			$querysp = sprintf(
				"SELECT id, filename, hashdistance('%s', hash) as distance
					FROM photo
					WHERE
						id != %d
						AND hash != ''
					GROUP BY id
					HAVING distance < 10
					ORDER BY distance ASC;",
				$photo['hash'],
				$photo['id']
			);
			$similar = $pdo->query($querysp)->fetchAll(\PDO::FETCH_ASSOC);
		} else {
			$similar = [];
		}

		# faces
		# TODO: serve un FaceMgr
		$faces = $this->facemgr->getFacesFor($photo['id'])->fetchAll();
		foreach ($faces as &$f) {
			$f['rect'] = json_decode($f['rect'], true);
		}

		# get preview size
		$psize = $this->settings->get('app', 'sizes')['preview'];
		$preview_size = ['width' => $psize, 'height' => $psize];
		if (!is_null($photo['width']) && !is_null($photo['height'])) {
			$w = $photo['width'];
			$h = $photo['height'];
			if ($w < $psize && $h < $psize) {
				$preview_size = ['width' => $w, 'height' => $h];
			} elseif ($w > $h) {
				$preview_size['height'] = $psize * ($h / $w);
			} elseif ($w < $h) {
				$preview_size['width'] = $psize * ($w / $h);
			}
		}

		$thumbpage = $request->getQueryParams()['tp'] ?? 1;

		return $this->render($response, $template, [
			'photo' => $photo,
			'tags' => $this->photomgr->getTagsFor($photo['id'])->fetchAll(),
			'faces' => $faces,
			'preview' => [
				'size' => $preview_size,
			],
			'prev' => $prev,
			'next' => $next,
			'sourcefile' => $sourcefile,
			'extpath' => $extpath,
			'extpath_win' => $extpath_win,
			'similar' => $similar,
			'thumbpage' => $thumbpage,
		]);
	}

	/**
	 * Edit photo metadata
	 *
	 * @httpmethod POST
	 * @param RouteData $args
	 * @return Response
	 */
	public function edit(Request $request, Response $response, array $args) : Response
	{
		if (is_null($this->user) || !$this->user['can_edit']) {
			return $response->withStatus(403); // Forbidden
		}
		$this->setup($request, $response, $args);

		$id = intval($args['id']);
		$photo = $this->photomgr->byId($id);
		if (is_null($photo)) {
			$response->getBody()->write("404 - not found");
			return $response->withStatus(404); // Not Found
		}

		/** @var ParsedData */
		$data = $request->getParsedBody();

		$redirect = $data['redirect'] ?? $this->routeParser->urlFor("search");

		$tags = $data['tags'];
		// split by "," , trim elements, remove empty ones, remove dups
		$tags = array_unique(
			array_filter(
				array_map(
					"trim",
					explode(",", $tags)
				)
			)
		);
		unset($data['tags']);
		$this->photomgr->setTags($photo, $tags);

		foreach ($data as $fieldname => $fieldvalue) {
			if (array_key_exists($fieldname, $photo)) {
				// TODO: here we should validate user input per field type
				$photo[$fieldname] = $fieldvalue;
			}
		}

		$this->photomgr->save($photo);

		return $response->withHeader('Location', $redirect)
						->withStatus(302);
	}

	/**
	 * Delete photo metadata
	 *
	 * @httpmethod POST
	 * @param RouteData $args
	 */
	public function delete(Request $request, Response $response, array $args) : Response
	{
		if (is_null($this->user) || !$this->user['can_delete']) {
			return $response->withStatus(403); // Forbidden
		}
		$this->setup($request, $response, $args);

		$id = intval($args['id']);
		$photo = $this->photomgr->byId($id);
		if (is_null($photo)) {
			$response->getBody()->write("404 - not found");
			return $response->withStatus(404); // Not Found
		}

		/** @var ParsedData */
		$data = $request->getParsedBody();
		$redirect = $data['redirect'] ?? $this->routeParser->urlFor("search");

		$this->photomgr->delete($photo);

		$this->photomgr->thumbDeleteById($id);

		return $response->withHeader('Location', $redirect)
						->withStatus(302);
	}

	/**
	 * @param RouteData $args
	 */
	public function source(Request $request, Response $response, array $args) : Response
	{
		$this->setup($request, $response, $args);
	
		if (is_null($this->user) || !$this->user['can_download']) {
			return $response->withStatus(403); // Forbidden
		}

		$id = intval($args['id']);
		$photo = $this->photomgr->byId($id);
		if (is_null($photo)) {
			return $response->withStatus(404); // Not Found
		}
		
		$sourcefile = $this->photomgr->getFullSourcePath($photo);
		
		if (!file_exists($sourcefile)) {
			// TODO: should we remove photo from db here?
			$this->logger->error("No source '$sourcefile' for photo id $id");
			return $response->withStatus(404); // Not Found
		}

		$params = $request->getQueryParams();
		$inline = $params['inline'] ?? false;
		$disposition = ($inline === false) ? 'attachment' : 'inline';

		$fh = fopen($sourcefile, 'rb');
		$file_stream = new Stream($fh);
		
		return $response->withBody($file_stream)
			->withHeader('Content-Disposition', $disposition . '; filename="' . $photo['filename'] . '";')
			->withHeader('Content-Type', $photo['format'])
			->withHeader('Content-Length', strval($photo['filesize']))
			->withHeader('Accept-Ranges', strval($photo['filesize']))
			->withHeader('Expires', '0');
	}

	/**
	 * @param string $widget Widget name
	 * @param Request $request
	 * @return Context Widget template renter context
	 */
	protected function widget(string $widget, Request $request) : array
	{
		$widget[0] = strtoupper($widget[0]);
		return call_user_func([$this, "doWidget" . $widget], $request);
	}

	/**
	 * Folder Widget render context
	 *
	 * @return Context
	 */
	protected function doWidgetFolders(Request $request) : array
	{
		// recursion root
		// eg: if $root = "/folder", then widget will show subfolders of "/folder"
		$root = "/";

		// Filter folders by 'location:' query in user query
		// if value starts with "/", set recursion root.
		// Filter is a regular expresion that will be match against folders path.
		//
		// TODO: this method doesn't wokrks well when the filter matches only a subfolder.
		//		 in this case, the subfolder is never shown:
		//		if $filer = "sub" and path is "/folder/sub", as "/folder" will not match,
		//		we never see "/folder/sub"
		$filter = null;
		if (!is_null($this->user) && strpos($this->user['query'], "location:") !== false) {
			$tokens = Utils::tokenize($this->user['query']);
			$filter = [];
			foreach ($tokens as $token) {
				if ($token[0] == "location") {
					$f = $token[1];
					if (Utils::startsWith($f, "/")) {
						$root = $f;
						$f = "^" . $f;
					}
					$filter[] = $f;
				}
			}
			$filter = "/" . str_replace("/", "\/", implode("|", $filter)) . "/";
		}


		// start iterator from location in query, if location value starts with "/"
		// eg: 'location:/folder" -> $root = "/folder"
		//
		// value must be subfolder of currently set $root value, wich could be from user query
		// so limiting user view of tree
		$params = $request->getQueryParams();
		$q = $params['q'] ?? null;
		$tokens = Utils::tokenize($q);
		foreach ($tokens as $token) {
			if ($token[0] == "location" && Utils::startsWith($token[1], "/")) {
				if (Utils::startsWith($token[1], $root)) {
					$root = $token[1];
				}
				break;
			}
		}

		#$root = dirname($root);
		$directories = new Utils\StorageDirectoryIterator(
			$this->settings->get('app', 'storage'),
			$root,
			$filter,
			$this->settings->get('folders', 'limit')
		);
		
		return [
			'title' => "Folders",
			'root' => $root,
			'dirs' => $directories,
		];
	}

	/**
	 * Tags Widget render context
	 *
	 * @return Context
	 */
	protected function doWidgetTags(Request $request) : array
	{
		$tags = $this->tagmgr->photoCount()
					->orderBy("n_photos DESC")
					->limit(50)
					->fetchAll();
		
		$min = array_reduce(
			$tags,
			function ($v, $i) {
				return min($v, (int)$i['n_photos']);
			},
			INF
		);
		$max = array_reduce(
			$tags,
			function ($v, $i) {
				return max($v, (int)$i['n_photos']);
			},
			0
		);

		/* font size between $min and $max values is 8 - 28 pt*/
		$tags = array_map(
			function ($item) use ($min, $max) {
				$item['fontsize'] = ((($item['n_photos'] - $min) / $max) * 20) + 8;
				return $item;
			},
			$tags
		);

		/* resort by name because is prettier */
		usort(
			$tags,
			function ($a, $b) {
				return strcasecmp($a['tag'], $b['tag']);
			}
		);

		return [
			'title' => 'Top Tags',
			'tags' => $tags,
		];
	}
	
	/**
	 * People Widget render context
	 *
	 * @return Context
	 */
	protected function doWidgetPeople(Request $request) : array
	{
		$names = $this->db->from('face')
					->select("COUNT(id) as n_photos")
					->groupBy("name")
					->orderBy("n_photos DESC")
					->limit(50)
					->fetchAll();
		
		$min = array_reduce(
			$names,
			function ($v, $i) {
				return min($v, (int)$i['n_photos']);
			},
			INF
		);
		$max = array_reduce(
			$names,
			function ($v, $i) {
				return max($v, (int)$i['n_photos']);
			},
			0
		);

		/* font size between $min and $max values is 8 - 28 pt*/
		$names = array_map(
			function ($item) use ($min, $max) {
				$item['fontsize'] = ((($item['n_photos'] - $min) / $max) * 20) + 8;
				return $item;
			},
			$names
		);

		/* resort by name because is prettier */
		usort(
			$names,
			function ($a, $b) {
				return strcasecmp($a['name'], $b['name']);
			}
		);

		return [
			'title' => 'People',
			'people' => $names,
		];
	}
}
