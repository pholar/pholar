<?php
/*
	Copyright (c) 2021 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar\Utils;

use ArrayIterator;
use RecursiveIterator;
use SplFileInfo;
use Pholar\Utils;

/**
 * Remplementation of \RecursiveDirectoryIterator which order files
 */
class RecursiveDirIterator implements RecursiveIterator
{
	/** @var ArrayIterator */
	private $dir;

	/** @var string */
	private $basepath;

	public function __construct(string $path)
	{
		$path = rtrim($path, "/");
		$this->basepath = $path;
		$this->dir = new ArrayIterator(scandir($path));
	}

	private function fullPath() : string
	{
		return $this->basepath . "/" . $this->dir->current();
	}

	private function skipDots() : void
	{
		if ($this->dir->current() !== null && Utils::endsWith($this->dir->current(), ".")) {
				$this->next();
		}
	}

	public function current() : SplFileInfo
	{
		return new SplFileInfo($this->fullPath());
	}

	public function next() : void
	{
		$this->dir->next();
		$this->skipDots();
	}

	public function key() : ?int
	{
		return $this->dir->key();
	}

	public function valid() : bool
	{
		return $this->dir->valid();
	}

	public function rewind() : void
	{
		$this->dir->rewind();
		$this->skipDots();
	}

	public function hasChildren() : bool
	{
		return is_dir($this->fullPath());
	}

	public function getChildren() : self
	{
		return new RecursiveDirIterator($this->fullPath());
	}
}
