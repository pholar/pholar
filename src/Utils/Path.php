<?php
/*
	Copyright (c) 2021 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar\Utils;

class Path
{

	/**
	 * From https://www.php.net/manual/en/function.realpath.php#124254
	 * @param string $path
	 * @return string
	 */
	public static function getAbsolute(string $path): string
	{
		// Cleaning path regarding OS
		$path = mb_ereg_replace('\\\\|/', DIRECTORY_SEPARATOR, $path, 'msr');
		// Check if path start with a separator (UNIX)
		$startWithSeparator = $path[0] === DIRECTORY_SEPARATOR;
		// Check if start with drive letter
		preg_match('/^[a-z]:/', $path, $matches);
		$startWithLetterDir = isset($matches[0]) ? $matches[0] : false;
		// Get and filter empty sub paths
		$subPaths = array_filter(explode(DIRECTORY_SEPARATOR, $path), 'mb_strlen');

		$absolutes = [];
		foreach ($subPaths as $subPath) {
			if ('.' === $subPath) {
				continue;
			}
			// if $startWithSeparator is false
			// and $startWithLetterDir
			// and (absolutes is empty or all previous values are ..)
			// save absolute cause that's a relative and we can't deal with that and just forget that we want go up
			if ('..' === $subPath
				&& !$startWithSeparator
				&& !$startWithLetterDir
				&& empty(array_filter($absolutes, function ($value) {
					return !('..' === $value);
				}))
			) {
				$absolutes[] = $subPath;
				continue;
			}
			if ('..' === $subPath) {
				array_pop($absolutes);
				continue;
			}
			$absolutes[] = $subPath;
		}

		return
			(($startWithSeparator ? DIRECTORY_SEPARATOR : $startWithLetterDir) ?
				$startWithLetterDir.DIRECTORY_SEPARATOR : ''
			).implode(DIRECTORY_SEPARATOR, $absolutes);
	}
}
