<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Utils\Stats;

use Generator;

/**
 * A Stupid class to register stats
 *
 * Use it as a singleton calling `StatsBag::instance()`
 */
class StatsBag
{
	/** @var StatsBag */
	private static $instance = null;

	/** @var array<Stat> */
	private $stats = [];

	/** @var array<string,Stat> */
	private $counter = [];

	public static function instance() : StatsBag
	{
		if (self::$instance == null) {
			self::$instance = new StatsBag;
		}
		return self::$instance;
	}

	/**
	 * 'Context Manager' version of 'set', automatically set 'time'
	 *
	 * call as
	 * ```php
	 * foreach ($bag->timed($domain, $data) as $t) {
	 *   .. things to be timed ...
	 * }
	 *
	 * @param string $domain  A name of what the stat is about
	 * @param ?mixed $data    Optional details about operation
	 * @return Generator<float>
	 */
	public function timed(string $domain, mixed $data = null) : Generator
	{
		$date = (new \DateTime())->format('c');
		/** @var float */
		$t = microtime(true);

		yield $t;

		/** @var float */
		$time = microtime(true) - $t;
		$this->set($domain, $time, $data, $date);
	}

	/**
	 * Add a new entry to stats
	 *
	 * @param string $domain   A name of what the stat is about (e.g. 'DB' or 'request')
	 * @param float $time      How log the operation took, in secs
	 * @param mixed $data     Optional details about the opreration
	 * @param ?string $date	   Optional ISO8601 date and time when event started (default on when `set()` is called)
	 */
	public function set(string $domain, float $time, mixed $data = null, string $date = null) : void
	{
		$this->counter[$domain] ??= new Stat($domain, 0, 0, "", null);
		$this->counter[$domain]->counter++;
		$this->counter[$domain]->time += $time;
		
		$this->stats[] = new Stat(
			$domain,
			$this->counter[$domain]->counter,
			$time,
			$date ?? (new \DateTime())->format('c'),
			$data
		);
	}

	/**
	 * @return array<Stat>
	 */
	public function get() : array
	{
		return $this->stats;
	}

	/**
	 * @return array<string,Stat>
	 */
	public function getCounter() : array
	{
		return $this->counter;
	}

	public function render() : string
	{
		$t = microtime(true);
		$out = "";
		$out .= "<table><thead><tr><th colspan='4'>Stats Panel</th></tr>";
		$out .= "<tr><th>Datetime</th><th>Domain</th><th>Duration</th><th>Data</th></tr></thead>";
		$out .= "<tbody>";
		foreach ($this->get() as $s) {
			$out .= "<tr><td>{$s->date}</td>";
			$out .= "<td>{$s->domain}&nbsp;#{$s->counter}</td>";
			$out .= "<td>" . number_format($s->time, 4) . "s</td>";
			if (!is_null($s->data)) {
				$out .= "<td><pre>" . print_r($s->data, true) . "</td>";
			} else {
				$out .= "<td></td>";
			}
			$out .= "</tr>";
		}
		$out .= "</tbody></table>";
		$out .= "<table><thead><tr><th>Domain</th><th>#</th><th>Tot. Time</th></thead><tbody>";
		foreach ($this->getCounter() as $s) {
			$out .= "<tr><td>{$s->domain}</td><td>{$s->counter}</td><td>". number_format($s->time, 4) ."</td></tr>";
		}
		$out .= "</tbody></table>";
		$out .= "<p>stats: " .  number_format(microtime(true) - $t, 4) . "s</p>";
		return $out;
	}
}
