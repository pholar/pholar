<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Utils\Stats;

use \PDOStatement;

/**
 * PDOStatement Timed
 *
 * Register execution time of `execute()`
 */
class PDOTStatement extends PDOStatement
{
	/** @var StatsBag */
	private $stats;

	protected function __construct()
	{
		$this->stats = StatsBag::instance();
	}

	private function dbg() : string
	{
		  ob_start();
		  $this->debugDumpParams();
		  $r = ob_get_contents();
		  ob_end_clean();
		  return preg_replace('|SQL: \[[0-9]*\] |', '', explode("Params:", $r)[0]);
	}

	/**
	 * @param array<mixed> $params
	 */
	public function execute(array $params = null) : bool
	{
		$q = $this->dbg();
		$r = false; // initialize because stan don't know that the foreach will always run one time
		foreach ($this->stats->timed('PDOSTM', ['query' => $q, 'params' => $params]) as $t) {
			$r = parent::execute($params);
		}
		return $r;
	}
}
