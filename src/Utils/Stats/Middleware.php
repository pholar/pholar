<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Utils\Stats;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as Handler;
use Psr\Http\Message\ResponseInterface;

/**
 * Register how log it takes to execute the request (after routing?)
 * and display stats on page
 *
 * This middleware is registered as last, so it's the first executed.
 * It append stats report at the end of any 'text/html' respose it see
 */
class Middleware
{
	public function __invoke(Request $request, Handler $handler) : ResponseInterface
	{
		$stats = StatsBag::instance();
		$resp = null; // initialize because stan don't know that the foreach will always run one time
		foreach ($stats->timed('REQUEST') as $t) {
			$resp = $handler->handle($request);
		}

		$contenttype = strtolower($resp?->getHeaderLine('content-type'));
		if (strpos($contenttype, 'text/html') !== false) {
			$body = $resp->getBody();
			if ($body->isWritable()) {
				$body->write($stats->render());
			}
		}
		return $resp;
	}
}
