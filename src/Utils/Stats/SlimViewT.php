<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Utils\Stats;

use Psr\Http\Message\ResponseInterface;
use Slim\Views\Twig;
use Twig\Loader\FilesystemLoader;
use Twig\Loader\LoaderInterface;
use Twig\Error\LoaderError;

/**
 * Slim Twig Timed
 *
 * Regster how long it takes to render a template
 */
class SlimViewT extends Twig
{
	/** @var StatsBag */
	private $stats;

	/**
	 * As OO in php is quite a joke yet, I need to copy this method.
	 *
	 * @param string|string[]      $path     Path(s) to templates directory
	 * @param array<string, mixed> $settings Twig environment settings
	 *
	 * @throws LoaderError When the template cannot be found
	 *
	 * @return Twig
	 */
	public static function create($path, array $settings = []): Twig
	{
		$loader = new FilesystemLoader();

		$paths = is_array($path) ? $path : [$path];
		foreach ($paths as $namespace => $path) {
			if (is_string($namespace)) {
				$loader->setPaths($path, $namespace);
			} else {
				$loader->addPath($path);
			}
		}

		return new self($loader, $settings);
	}

	public function __construct(LoaderInterface $loader, array $settings = [])
	{
		parent::__construct($loader, $settings);
		$this->stats = StatsBag::instance();
	}

	public function render(ResponseInterface $response, string $template, array $data = []) : ResponseInterface
	{
		$r = null; // initialize because stan don't know that the foreach will always run one time
		foreach ($this->stats->timed('TWIG', ['template' => $template]) as $t) {
			$r = parent::render($response, $template, $data);
		}
		return $r;
	}
}
