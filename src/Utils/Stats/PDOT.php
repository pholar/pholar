<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Utils\Stats;

use \PDO;

/**
 * PDO Timed
 *
 * Register execution time of `exec()`
 */
class PDOT extends PDO
{
	/** @var StatsBag */
	private $stats;

	/**
	 * @param array<string,mixed> $driver_options
	 */
	public function __construct(string $dsn, string $user = null, string $pass = null, array $driver_options = null)
	{
		parent::__construct($dsn, $user, $pass, $driver_options);
		$this->setAttribute(\PDO::ATTR_STATEMENT_CLASS, [PDOTStatement::class, []]);
		$this->stats = StatsBag::instance();
	}

	public function exec(string $statement) : int|false
	{
		$r = false; // initialize because stan don't know that the foreach will always run one time
		foreach ($this->stats->timed('PDO', ['query' => $statement]) as $t) {
			$r = parent::exec($statement);
		}
		return $r;
	}
}
