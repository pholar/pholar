<?php
/*
	Copyright (c) 2021 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Utils;

use \RecursiveIteratorIterator;
use \RecursiveArrayIterator;
use \RecursiveCallbackFilterIterator;
use \UnexpectedValueException;
use \ReturnTypeWillChange;

class StorageDirectoryIterator extends RecursiveIteratorIterator
{
	/** @var ?string */
	private $filter;

	/** @var ?int */
	private $limit;

	/** @var int */
	private $deep;

	/** @var string */
	private $chdir;

	/**
	 * Iterate over storage folders
	 *
	 * # TODO: $chdir replaces $_ENV['STORAGEDIR'], but it's ugly: need a better way to work
	 */
	public function __construct(
		string $chdir,
		string $path = "/",
		?string $refilter = null,
		?int $limit = null,
		\RecursiveIterator $iter = null,
		int $deep = 0
	) {
		$this->filter = $refilter;
		$this->limit = $limit;
		$this->deep = $deep;
		$this->chdir = $chdir;
		if (is_null($iter)) {
			$base = $chdir . $path;

			try {
				$iter = new RecursiveDirIterator($base);
			} catch (UnexpectedValueException $e) {
				// failed to open dir: Permission denied
				$iter = new \RecursiveArrayIterator();
			}
			$iter = new RecursiveCallbackFilterIterator($iter, function ($current, $key, $iterator) {
				return  $current->isDir();
			});
			// TODO: cosi' non viene applicato ai children
			if (!is_null($refilter)) {
				$iter = new RecursiveCallbackFilterIterator($iter, function ($current, $key, $iterator) use ($chdir, $refilter) {
					$path = str_replace($chdir, "", $current->getPathname());
					return preg_match($refilter, $path) == 1;
				});
			}
		}
		if (!is_null($limit) && $deep >= $limit) {
			$iter = new RecursiveArrayIterator();
		}
		parent::__construct($iter, RecursiveIteratorIterator::CATCH_GET_CHILD);
	}

	#[ReturnTypeWillChange]
	public function callGetChildren(): StorageDirectoryIterator
	{
		try {
			$iter = parent::callGetChildren();
		} catch (\UnexpectedValueException $e) {
			// failed to open dir: Permission denied
			$iter = new \RecursiveArrayIterator();
		}
		return new StorageDirectoryIterator($this->chdir, "", $this->filter, $this->limit, $iter, $this->deep+1);
	}

	/**
	 * @return array{realpath: string}
	 */
	public function current(): array
	{
		$value = parent::current();
		$perms = fileperms($value->getPathname());
		$permr = ($perms & 0x0100) || ($perms & 0x0020) || ($perms & 0x0004);
		$permw = ($perms & 0x0080) || ($perms & 0x0010) || ($perms & 0x0002);
		$permx = ($perms & 0x0040) || ($perms & 0x0008) || ($perms & 0x0001); # not fully correct

		return [
			'realpath' => $value->getPathname(),
			'path' => str_replace($this->chdir, "", $value->getPathname()),
			'name' => $value->getFilename(),
			'can_enter' => $permr && $permx,
			'can_write' => $permr && $permx && $permw,
		];
	}
}
