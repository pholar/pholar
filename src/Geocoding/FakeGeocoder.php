<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Geocoding;

use Geocoder\Collection;
use Geocoder\Geocoder;
use Geocoder\Query\GeocodeQuery;
use Geocoder\Query\ReverseQuery;

class FakeGeocoder implements Geocoder
{
	public function geocodeQuery(GeocodeQuery $query): Collection
	{
		return new FakeAddressMap();
	}

	public function reverseQuery(ReverseQuery $query): Collection
	{
		return new FakeAddressMap();
	}

	public function getName(): string
	{
		return "FakeGeocoder";
	}

	public function geocode(string $value): Collection
	{
		return new FakeAddressMap();
	}

	public function reverse(float $latitude, float $longitude): Collection
	{
		return new FakeAddressMap();
	}
}
