<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Geocoding;

use ArrayIterator;
use Geocoder\Collection;
use Geocoder\Location;
use Geocoder\Exception\CollectionIsEmpty;
use Geocoder\Exception\OutOfBounds;
use Traversable;

/**
 * An empty query result from FakeGeocoder
 */
class FakeAddressMap implements Collection
{
	public function count(): int
	{
		return 0;
	}

	public function getIterator(): Traversable
	{
		return new ArrayIterator();
	}

	public function first(): Location
	{
		throw new CollectionIsEmpty();
	}

	public function isEmpty(): bool
	{
		return true;
	}

	/**
	 * @return Location[]
	 */
	public function slice(int $offset, int $length = null) : array
	{
		return [];
	}

	public function has(int $index): bool
	{
		return false;
	}

	public function get(int $index): Location
	{
		throw new OutOfBounds();
	}

	/**
	 * @return Location[]
	 */
	public function all(): array
	{
		return [];
	}
}
