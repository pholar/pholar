<?php
namespace Pholar;

use Envms\FluentPDO\Query;

/**
 * This class will try to load (and save) settings in db,
 * falling back on SystemSettings
 */
class Settings extends SystemSettings
{
	/** @var Query */
	protected $db;

	public function __construct(Query $db)
	{
		parent::__construct();
		$this->db = $db;
	}

	public function get(string $f, string $k = null) : mixed
	{
		# TODO: load from db
		return parent::get($f, $k);
	}

	public function put(string $f, mixed $k, mixed $v = null) : void
	{
		# TODO: write to db
		parent::put($f, $k, $v);
	}
}
