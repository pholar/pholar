<?php
/*
	Copyright (c) 2020-2021 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar\Console;

use Psr\Log\LoggerInterface;
use Envms\FluentPDO\Query;

use Pholar\SQL\SQLDriver;
use Pholar\Settings;
use Pholar\PhotoMgr;
use Pholar\TagMgr;
use Pholar\CollectionMgr;
use Pholar\Utils;

/**
 * This class is not a command, but contains shared
 * code between different updating commands
 */
class UpdatingBase extends Command
{
	/** @var Query */
	protected $db;
	
	/** @var SQLDriver */
	protected $sqldriver;

	/** @var PhotoMgr */
	protected $photomgr;

	/** @var TagMgr */
	protected $tagmgr;

	/** @var CollectionMgr */
	protected $collectionmgr;

	public function __construct(
		LoggerInterface $logger,
		Settings $settings,
		Query $db,
		SQLDriver $sqldriver,
		PhotoMgr $photomgr,
		TagMgr $tagmgr,
		CollectionMgr $collectionmgr,
	) {
		parent::__construct($logger, $settings);
		$this->db = $db;
		$this->sqldriver = $sqldriver;
		$this->photomgr = $photomgr;
		$this->tagmgr = $tagmgr;
		$this->collectionmgr = $collectionmgr;
	}

	/**
	 * check filename against a regexp list to ignore
	 *
	 * @param string $filename Filename to check
	 * @return bool True if filename match
	 */
	protected function isIgnorable(string $filename) : bool
	{
		$filename = basename($filename);

		$ignore_file_patterns = [
			'^\.',				// hidden files
			'.*exiftool_tmp',	// exiftool temp file
			'\.part$',			// dwl/upl temp file
		];

		foreach ($ignore_file_patterns as $p) {
			if (preg_match("|$p|", $filename)) {
				$this->logger->debug("Ignore '$filename' per |$p|");
				return true;
			}
		}
		return false;
	}

	/**
	 * handle new ignore file
	 *
	 * @param string $filename Filename to handle
	 * @return bool True if $filename has been handled
	 */
	protected function handleIgnoreFileAdd(string $filename) : bool
	{
		if (Utils::endsWith($filename, ".ignore")) {
			$dirname = dirname($filename);
			$filename = basename($filename);
			$filename = preg_replace('/\.(.*)\.ignore/m', '$1', $filename);
			$filename = $dirname . "/" . $filename;
			if (file_exists($filename)) {
				$this->logger->debug("Soft-delete $filename");
				$photoid = $this->photomgr->existsByPath($filename);
				if ($photoid !== false) {
					$photo = $this->photomgr->byId($photoid);
					$this->photomgr->delete($photo);
					$this->photomgr->thumbDeleteById($photoid);
				}
			}
			return true;
		}

		return false;
	}


	/**
	 * handle deletion of ignore file
	 *
	 * @param string $filename Filename to handle
	 * @return bool True if $filename has been handled
	 */
	protected function handleIgnoreFileDelete(string $filename) : bool
	{
		if (Utils::endsWith($filename, ".ignore")) {
			// is an ignore file has been deleted, we try to update the
			// once ignored asset
			$dirname = dirname($filename);
			$filename = basename($filename);
			$filename = preg_replace('/\.(.*)\.ignore/m', '$1', $filename);
			$filename = $dirname . "/" . $filename;
			if (file_exists($filename)) {
				$this->logger->info("Restore deleted $filename");
				$this->addAndUpdate($filename);
			}
			return true;
		}
		return false;
	}

	/**
	 * add or update file
	 *
	 * @param string $filename  Full path to file
	 * @param bool $metadata    If true, full update is scheduled for existing assets
	 * @return int|null         Photo id if handled, null if ignored
	 */
	protected function addAndUpdate(string $filename, bool $metadata = false) : ?int
	{
		$id = $this->photomgr->existsByPath($filename);
		if ($id === false) {
			// file didn't exists prior upload, add it
			$id = $this->photomgr->add($filename);
		} else {
			if ($metadata) {
				$this->photomgr->scheduleThumbs($id);
				$this->photomgr->scheduleExtract($id);
				$this->photomgr->scheduleFaces($id);
			}
		}
		return $id;
	}
}
