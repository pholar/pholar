<?php
/*
	Copyright (c) 2020-2021 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar\Console;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Psr\Log\LoggerInterface;

use Phinx\Config\Config;
use Phinx\Migration\Manager;

use Pholar\Settings;

class MigrateCommand extends Command
{
	// the name of the command (the part after "bin/console")
	protected static $defaultName = 'migrate';

	/** @var Config */
	protected $phinxconfig;

	public function __construct(
		LoggerInterface $logger,
		Settings $settings,
		Config $phinxconfig
	) {
		parent::__construct($logger, $settings);
		$this->phinxconfig = $phinxconfig;
	}

	protected function configure() : void
	{
		$this
			->setDescription('Migrate the database')
			->setHelp(
				"The <info>migrate</info> command runs all available migrations"
			);
	}

	protected function execute(InputInterface $input, OutputInterface $output) : ?int
	{
		$env = $this->settings->getEnv();

		$manager = new Manager($this->phinxconfig, $input, $output);
		$manager->migrate($env);
		#$manager->seed($env);
		return 0;
	}
}
