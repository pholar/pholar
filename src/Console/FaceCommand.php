<?php
/*
	Copyright (c) 2020-2021 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar\Console;

use Symfony\Component\Console\Input\InputArgument;
#use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;

use Psr\Log\LoggerInterface;
use Envms\FluentPDO\Query;

use Pholar\Settings;
use Pholar\Utils;
use Pholar\SQL\SQLDriver;
use Pholar\PhotoMgr;
use Pholar\TagMgr;
use Pholar\CollectionMgr;
use Pholar\TaskMgr;

use Pholar\Tasks\FaceRecognition;

class FaceCommand extends UpdatingBase
{
	// the name of the command (the part after "bin/console")
	protected static $defaultName = 'face:detect';

	/** @var TaskMgr */
	protected $taskmgr;

	public function __construct(
		LoggerInterface $logger,
		Settings $settings,
		Query $db,
		SQLDriver $sqldriver,
		PhotoMgr $photomgr,
		TagMgr $tagmgr,
		CollectionMgr $collectionmgr,
		TaskMgr $taskmgr,
	) {
		parent::__construct($logger, $settings, $db, $sqldriver, $photomgr, $tagmgr, $collectionmgr);
		$this->taskmgr = $taskmgr;
	}

	protected function configure() : void
	{
		$this
			->setDescription('Detect faces in photos')
			->setHelp("'face:detect' schedule tasks to detect faces in specified photo or all photo under a folder.")
			->addArgument('path', InputArgument::REQUIRED, "File or folder where search for faces. Must be inside storage dir.")
			;
	}

	protected function execute(InputInterface $input, OutputInterface $output) : int
	{
		if ($this->settings->get('facetool', 'enabled') == false) {
			$this->logger->error("Facetool not enabled in settings.");
			return 255;
		}

		$STORAGEDIR = $this->settings->get('app', 'storage');
		
		$showpb = !$input->getOption('quiet');
		$verbose = $output->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE;
		if ($verbose) {
			// verbosity disable the progressbar, so $showpb must be false
			$showpb = false;
		}

		$path = $input->getArgument('path');

		if (is_null($path)) {
			$this->logger->error("Invalid path");
			return -1;
		}
		if ($path[0] !== "/") {
			$path = realpath($this->cwd . "/" . $path);
		}
		$this->logger->info("Updating '$path'");

		if (!Utils::startsWith($path, $STORAGEDIR)) {
			$this->logger->error("'$path' is not in storage dir '$STORAGEDIR'");
			return -1;
		}

		// update single file
		if (is_file($path)) {
			if ($this->isIgnorable($path)) {
				$output->writeln("<info>File ignored</>");
				return 0;
			}


			$photo_id = $this->photomgr->existsByPath($path);
			if ($photo_id === false) {
				$this->logger->error("'$path' is not indexed");
				return -1;
			}
			if ($output->isVerbose()) {
				$output->write("Scheduling ". basename($path) . " ($photo_id)...");
			}

			$this->taskmgr->schedule(FaceRecognition::class, ['id' => $photo_id]);
			if ($output->isVerbose()) {
				$output->writeln(" ...done.");
			}
			return 0;
		}

		// update recursively
		$this->logger->info("Updating folder recursively");

		// setup iterator
		$directory = new \RecursiveDirectoryIterator(
			$path,
			\FilesystemIterator::SKIP_DOTS | \FilesystemIterator::FOLLOW_SYMLINKS
		);

		$iterator = new \RecursiveIteratorIterator($directory, \RecursiveIteratorIterator::LEAVES_ONLY);
		$filecount = iterator_count($iterator);
		$iterator->rewind();

		if ($filecount == 0) {
			$this->logger->info("No file to update.");
		} else {
			if ($showpb) {
				ProgressBar::setFormatDefinition('custom', '%current%/%max% |%bar%| %message%');
				$progressBar = new ProgressBar($output, $filecount);
				$progressBar->setFormat('custom');
				$progressBar->start();
			}

			foreach ($iterator as $file) {
				$fullpathname = $file->getPathname();
				$relpathname = str_replace($STORAGEDIR . "/", "", $fullpathname);
				if ($showpb) {
					$progressBar->setMessage($relpathname);
					$progressBar->advance();
				} else {
					$output->write($relpathname . "...");
				}

				if ($this->handleIgnoreFileAdd($fullpathname)) {
					$this->logger->info("File soft-deleted");
				} elseif ($this->isIgnorable($fullpathname)) {
					$this->logger->info("File ignored");
				} else {
					$photo_id = $this->photomgr->existsByPath($fullpathname);
					if ($photo_id === false) {
						$this->logger->info("File not indexed");
					}
					$this->taskmgr->schedule(FaceRecognition::class, ['id' => $photo_id]);
				}

				if ($showpb) {
				} else {
					$output->writeln("done.");
				}
			}

			if ($showpb) {
				$progressBar->setMessage("");
				$progressBar->finish();
				$output->writeln("");
			}
		}
		return 0;
	}
}
