<?php
/*
	Copyright (c) 2020-2021 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar\Console;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;

use Pholar\Utils;

class UpdateCommand extends UpdatingBase
{
	// the name of the command (the part after "bin/console")
	protected static $defaultName = 'update';

	protected function configure() : void
	{
		$this
			->setDescription('Update stored metadata.')
			->setHelp("'update' will find changes in storage dir and update database accordly")
			->addArgument('path', InputArgument::OPTIONAL, "File or folder to update in db. Must be inside storage dir.")
			->addOption('force', 'f', InputOption::VALUE_NONE, "Update all files, regardless of the modification date")
			->addOption('metadata', 'm', InputOption::VALUE_NONE, "Reload metadata from file already in database")
			;
	}

	protected function execute(InputInterface $input, OutputInterface $output) : ?int
	{
		$STORAGEDIR = $this->settings->get('app', 'storage');
		$LASTUPDATEFILE = $this->settings->get('lastupdatefile');

		
		$showpb = !$input->getOption('quiet');
		$force = $input->getOption('force');
		$metadata = $input->getOption('metadata');
		$verbose = $output->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE;
		if ($verbose) {
			// verbosity disable the progressbar, so $showpb must be false
			$showpb = false;
		}

		$path = $input->getArgument('path');
		if (is_null($path)) {
			$path = $STORAGEDIR;
		} else {
			if ($path[0] !== "/") {
				$path = realpath($this->cwd . "/" . $path);
			}
		}

		if (is_null($path)) {
			$this->logger->error("Invalid path");
			return -1;
		}

		$this->logger->info("Updating '$path'");

		if (!Utils::startsWith($path, $STORAGEDIR)) {
			$this->logger->error("'$path' is not in storage dir '$STORAGEDIR'");
			return -1;
		}

		// update single file
		if (is_file($path)) {
			if ($this->isIgnorable($path)) {
				$output->writeln("<info>File ignored</>");
				return 0;
			}

			if ($output->isVerbose()) {
				$output->write("Updating ". basename($path) . "...");
			}

			$id = $this->addAndUpdate($path, $metadata);
			if (is_null($id)) {
				$output->writeln("<info>File ignored</>");
			}
			if ($output->isVerbose()) {
				$output->writeln(" ...done.");
			}
			return 0;
		}

		// update recursively
		$this->logger->info("Updating folder recursively");
		if (!file_exists($LASTUPDATEFILE)) {
			$this->logger->debug("Set last update to 'never'");
			file_put_contents($LASTUPDATEFILE, "never");
			touch($LASTUPDATEFILE, 0);
			$lastupdatetime = 0;
		} else {
			$lastupdatetime = stat($LASTUPDATEFILE)['mtime'];
		}
		$this->logger->info("Last updated: " . (new \DateTime("@$lastupdatetime"))->format("c"));

		// setup iterator
		$directory = new \RecursiveDirectoryIterator(
			$path,
			\FilesystemIterator::SKIP_DOTS | \FilesystemIterator::FOLLOW_SYMLINKS
		);
		if ($force) {
			$this->logger->info("Forced update.");
		} else {
			// get only files newer than $lastupdatetime
			$this->logger->info("Find files modified after last update");
			$directory = new \RecursiveCallbackFilterIterator($directory, function ($current, $key, $iterator) use ($lastupdatetime) {
				return  $current->isDir() || ($current->getMTime() > $lastupdatetime);
			});
		}

		$iterator = new \RecursiveIteratorIterator($directory, \RecursiveIteratorIterator::LEAVES_ONLY);
		$filecount = iterator_count($iterator);
		$iterator->rewind();

		if ($filecount == 0) {
			$this->logger->info("No file to update.");
		} else {
			if ($showpb) {
				ProgressBar::setFormatDefinition('custom', '%current%/%max% |%bar%| %message%');
				$progressBar = new ProgressBar($output, $filecount);
				$progressBar->setFormat('custom');
				$progressBar->start();
			}

			foreach ($iterator as $file) {
				$fullpathname = $file->getPathname();
				$relpathname = str_replace($STORAGEDIR . "/", "", $fullpathname);
				if ($showpb) {
					$progressBar->setMessage($relpathname);
					$progressBar->advance();
				} else {
					$output->write($relpathname . "...");
				}

				if ($this->handleIgnoreFileAdd($fullpathname)) {
					$this->logger->info("File soft-deleted");
				} elseif ($this->isIgnorable($fullpathname)) {
					$this->logger->info("File ignored");
				} else {
					$id = $this->addAndUpdate($fullpathname, $metadata);
				}

				if ($showpb) {
				} else {
					$output->writeln("done.");
				}
			}

			if ($showpb) {
				$progressBar->setMessage("");
				$progressBar->finish();
				$output->writeln("");
			}
		}


		// look for deleted files from disk
		$this->logger->info("Look for files deleted from disk");
		$photos = $this->photomgr->all();
		foreach ($photos as $photo) {
			$photopath = $this->photomgr->getFullSourcePath($photo);
			if (!is_file($photopath)) {
				$this->logger->info("Deleted file " . $photopath);
				$this->photomgr->delete($photo);
				// delete thumbnails
				$basedir = $this->settings->get('basedir');
				foreach (glob($basedir . '/public/thumb/*/' . $photo['id'] . '.jpg', GLOB_NOSORT) as $filename) {
					$this->logger->debug("Delete thumbnail " . $filename);
					unlink($filename);
				}
				foreach (glob($basedir . '/public/thumb/*/' . $photo['id'] . 'p*.jpg', GLOB_NOSORT) as $filename) {
					$this->logger->debug("Delete thumbnail " . $filename);
					unlink($filename);
				}
			}
		}
		$this->logger->info("Done");

		$now = (new \DateTime())->format("c");
		file_put_contents($LASTUPDATEFILE, $now);
		return 0;
	}
}
