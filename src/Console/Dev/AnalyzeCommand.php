<?php
/*
	Copyright (c) 2020-2021 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar\Console\Dev;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Pholar\Console\Command;

class AnalyzeCommand extends Command
{
	// the name of the command (the part after "bin/console")
	protected static $defaultName = 'dev:analyze';

	protected function configure() : void
	{
		$this
			->setDescription('Analyze source code')
			->addOption('memory-limit', null, InputOption::VALUE_OPTIONAL, "Memory limit for analysis", "12G")
			;
	}

	protected function execute(InputInterface $input, OutputInterface $output) : ?int
	{
		$memlim = $input->getOption('memory-limit');

		if ($output->isVerbose()) {
			$output->writeln("<info>Memory-limit: $memlim</info>");
		}
		
		$basedir = $this->settings->get('basedir');
		$cmd = $basedir . "/vendor/bin/phpstan analyze ";
		$cmd .= "--memory-limit '{$memlim}' ";

		$output->isVerbose() && $cmd .= " -v";
		$output->isVeryVerbose() && $cmd .= "v";
		$output->isDebug() && $cmd .= "v";
		$output->isQuiet() && $cmd .= " -q";

		$cmd .= " -- ";
		$cmd .= "$basedir/src $basedir/app ";
		
		if ($output->isDebug()) {
			$output->writeln("<info>$cmd</info>");
		}
		$ret = 0;
		passthru(escapeshellcmd($cmd), $ret);
		return $ret;
	}
}
