<?php
/*
	Copyright (c) 2020-2021 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar\Console\Dev;

use InvalidArgumentException;
use RuntimeException;

use Psr\Log\LoggerInterface;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Phinx\Config\Config;
use Phinx\Util\Util;

use Pholar\Console\Command;
use Pholar\Settings;

class MakemigrationCommand extends Command
{
	protected static $defaultName = 'dev:makemigration';

	/**
	 * The location of the default migration template.
	 */
	protected const DEFAULT_MIGRATION_TEMPLATE = 'vendor/robmorgan/phinx/src/Phinx/Migration/Migration.template.php.dist';


	/** @var Config */
	protected $phinxconfig;

	public function __construct(
		LoggerInterface $logger,
		Settings $settings,
		Config $phinxconfig
	) {
		parent::__construct($logger, $settings);
		$this->phinxconfig = $phinxconfig;
	}
	
	protected function configure() : void
	{
		$this
			->setDescription('Creates a new migration')
			->setHelp(
				"Creates a new database migration"
			)
			->addArgument('name', InputArgument::OPTIONAL, 'Class name of the migration (in CamelCase)');
	}

	/**
	 * This is a sripped-down copy of `Phinx\Console\Command\Create::execute()`
	 */
	protected function execute(InputInterface $input, OutputInterface $output) : ?int
	{
		$env = $this->settings->getEnv();

		$paths = $this->phinxconfig->getMigrationPaths();
		$path = array_shift($paths);
		$path = realpath($path);

		$namespace = $this->phinxconfig->getMigrationNamespaceByPath($path);
		
		$className = $input->getArgument('name');
		if ($className === null) {
			$currentTimestamp = Util::getCurrentTimestamp();
			$className = 'V' . $currentTimestamp;
			$fileName = $currentTimestamp . '.php';
		} else {
			if (!Util::isValidPhinxClassName($className)) {
				throw new InvalidArgumentException(sprintf(
					'The migration class name "%s" is invalid. Please use CamelCase format.',
					$className
				));
			}

			// Compute the file path
			$fileName = Util::mapClassNameToFileName($className);
		}

		if (!Util::isUniqueMigrationClassName($className, $path)) {
			throw new InvalidArgumentException(sprintf(
				'The migration class name "%s" already exists',
				$namespace . '\\' . $className
			));
		}

		$filePath = $path . DIRECTORY_SEPARATOR . $fileName;

		if (is_file($filePath)) {
			throw new InvalidArgumentException(sprintf(
				'The file "%s" already exists',
				$filePath
			));
		}

		// from Phinix\Config\Config::getMigrationBaseClassName()
		$migrationBaseClassName = 'AbstractMigration';
		$migrationBaseClassPath = 'Phinx\Migration\\' . $migrationBaseClassName;

		// Load the template.
		$contents = file_get_contents(self::DEFAULT_MIGRATION_TEMPLATE);
		
		// inject the class names appropriate to this migration
		$classes = [
			'$namespaceDefinition' => $namespace !== null ? (PHP_EOL . 'namespace ' . $namespace . ';' . PHP_EOL) : '',
			'$namespace' => $namespace,
			'$useClassName' => $migrationBaseClassPath,
			'$className' => $className,
			'$version' => Util::getVersionFromFileName($fileName),
			'$baseClassName' => $migrationBaseClassName,
		];
		$contents = strtr($contents, $classes);

		if (file_put_contents($filePath, $contents) === false) {
			throw new RuntimeException(sprintf(
				'The file "%s" could not be written to',
				$path
			));
		}

		$output->writeln('<info>using migration base class</info> ' . $classes['$useClassName']);
		$output->writeln('<info>using default template</info>');
		$output->writeln('<info>created</info> ' . Util::relativePath($filePath));

		return 0;
	}
}
