<?php
/*
	Copyright (c) 2020-2021 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar\Console\Dev;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Pholar\Console\Command;

class LintCommand extends Command
{
	// the name of the command (the part after "bin/console")
	protected static $defaultName = 'dev:lint';

	protected function configure() : void
	{
		$this
			->setDescription('Run linter on source code')
			->addOption('format', 'f', InputOption::VALUE_NONE, "Try to auto-format source code")
			;
	}

	protected function execute(InputInterface $input, OutputInterface $output) : ?int
	{
		$basedir = $this->settings->get('basedir');
		$cmd = $basedir . "/vendor/bin/phpcs -n";

		$format = $input->getOption('format');
		if ($format) {
			$cmd = $basedir . "/vendor/bin/phpcbf";
		}

		$ret = 0;
		foreach (['src', 'app', 'bin'] as $sub) {
			$output->writeln("in {$sub}:");
			passthru(escapeshellcmd("{$cmd} --standard='{$basedir}/ruleset.xml' '{$basedir}/{$sub}'"), $_ret);
			$ret += $_ret;
		}
		return $ret;
	}
}
