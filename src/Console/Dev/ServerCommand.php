<?php
/*
	Copyright (c) 2020-2021 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar\Console\Dev;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Pholar\Console\Command;

class ServerCommand extends Command
{
	// the name of the command (the part after "bin/console")
	protected static $defaultName = 'dev:server';

	protected function configure() : void
	{
		$this
			->setDescription('Run develop server')
			->addArgument('address:port', InputArgument::OPTIONAL, 'Listen on specified address and port', 'localhost:8888')
			;
	}

	protected function execute(InputInterface $input, OutputInterface $output) : ?int
	{
		$hostport = $input->getArgument('address:port');
		
		$pubdir = $this->settings->get('basedir') . '/public';
		chdir($pubdir);
		
		$version = \Pholar\App::getVersion();
		$env = $this->settings->getEnv(); // $_ENV['PHOLAR_ENV'] ?? getenv('PHOLAR_ENV') ?: "dev";
		$output->writeln("<info>Pholar {$version} </info> [<comment>{$env}</comment>]");

		passthru(escapeshellcmd("PHOLAR_ENV=$env php -S " . $hostport), $ret);

		return $ret;
	}
}
