<?php
/*
	Copyright (c) 2020-2021 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar\Console;

use Psr\Log\LoggerInterface;

#use Symfony\Component\Console\Input\InputArgument;
#use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Pholar\Console\Command;
use Pholar\Settings;
use Pholar\TaskMgr;

class TaskCommand extends Command
{
	// the name of the command (the part after "bin/console")
	protected static $defaultName = 'task';

	/** @var TaskMgr */
	protected $taskmgr;

	public function __construct(
		LoggerInterface $logger,
		Settings $settings,
		TaskMgr $taskmgr
	) {
		parent::__construct($logger, $settings);
		$this->taskmgr = $taskmgr;
	}

	protected function configure() : void
	{
		$this
			->setDescription('Execute pending tasks.')
			->setHelp("'task' will execute all pending tasks from the queue.")
			//->addArgument('path', InputArgument::OPTIONAL, "File or folder to update in db. Must be inside storage dir.")
			//->addOption('force', 'f', InputOption::VALUE_NONE, "Update all files, regardless of the modification date")
			;
	}

	protected function execute(InputInterface $input, OutputInterface $output) : ?int
	{
		while (true) {
			$task = $this->taskmgr->getNextTask();
			if (is_null($task)) {
				return 0;
			} else {
				$taskid = $task['id'];
				$output->writeln("<info>Running task #$taskid</info>... ");
				try {
					$this->taskmgr->execute($task);
				} catch (\Exception $e) {
					$output->writeln("<error>Task #$taskid Error: {$e->getMessage()}</error>");
					continue;
				}
				$output->writeln("<info>Task #$taskid Ok</info>");
			}
		}
	}
}
