<?php
/*
	Copyright (c) 2020-2021 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar\Console;

use DI\Container;
use Psr\Log\LoggerInterface;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Command\Command as SymfonyCommand;

use Pholar\Settings;
use Pholar\App;

/**
 * Base command class for Pholar Console commands
 */
class Command extends SymfonyCommand
{
	/** @var LoggerInterface */
	protected $logger;

	/** @var Settings */
	protected $settings;

	/** @var string */
	protected $cwd = "";

	public function __construct(
		LoggerInterface $logger,
		Settings $settings
	) {
		parent::__construct();
		$this->logger = $logger;
		$this->settings = $settings;

		$this->cwd = getcwd();
	}

	protected function initialize(InputInterface $input, OutputInterface $output) : void
	{
		$debug = $output->isDebug();

		/** @var Container */
		$container = App::getApp()->getContainer();
		
		/* replace logger with console logger on output */
		$this->logger = new ConsoleLogger($output);
		$container->set(LoggerInterface::class, $this->logger);

		/* set app debug value on output debug value */
		$this->settings->put('debug', $debug);
	}
}
