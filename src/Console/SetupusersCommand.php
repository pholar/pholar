<?php
/*
	Copyright (c) 2020-2021 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar\Console;

use \PDO;
use Psr\Log\LoggerInterface;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

use Pholar\Console\Command;
use Pholar\Settings;
use Pholar\SQL\SQLDriver;

class SetupusersCommand extends Command
{
	// the name of the command (the part after "bin/console")
	protected static $defaultName = 'setupusers';

	/** @var SQLDriver */
	protected $sqldriver;

	/** @var PDO */
	protected $pdo;

	public function __construct(
		LoggerInterface $logger,
		Settings $settings,
		SQLDriver $sqldriver,
		PDO $pdo
	) {
		parent::__construct($logger, $settings);
		$this->sqldriver= $sqldriver;
		$this->pdo = $pdo;
	}
	
	protected function configure() : void
	{
		$this
			->setDescription('Create administrator and guest users')
			->setHelp(
				"Create administrator and guest users.\n" .
				"If user exists in database, nothing is changed, unless --replace option is passed."
			)
			->addOption('replace', 'r', InputOption::VALUE_NONE, "Replace existing passwords")
			->addArgument("username", InputArgument::OPTIONAL, "Administrator user name", "admin")
			->addOption("password", "p", InputOption::VALUE_REQUIRED, "Administrator user password")
			;
	}

	protected function interact(InputInterface $input, OutputInterface $output) : void
	{
		$adminusername = $input->getArgument('username');
		$adminpassword = $input->getOption('password');

		if (is_null($adminpassword)) {
			$helper = $this->getHelper('question');

			$question = new Question("Admin user ($adminusername) password: ");
			$question->setHidden(true);
			$question->setHiddenFallback(false);
			$adminpassword = $helper->ask($input, $output, $question);
			if ($adminpassword == "") {
				throw new \Exception('The password cannot be empty');
			}

			$question = new Question("Admin password again: ");
			$question->setHidden(true);
			$question->setHiddenFallback(false);
			$adminpassword2 = $helper->ask($input, $output, $question);
			if ($adminpassword2 != $adminpassword) {
				throw new \Exception("Passwords do not match");
			}

			$input->setOption('password', $adminpassword);
		}
	}

	protected function execute(InputInterface $input, OutputInterface $output) : ?int
	{
		$adminusername = $input->getArgument('username');
		$adminpassword = $input->getOption('password');
		$replace = $input->getOption("replace");

		if (trim($adminpassword) == "") {
			throw new \Exception('The password cannot be empty');
		}

		$adminpassword = password_hash("$adminpassword", PASSWORD_DEFAULT);


		$sql = ($replace ?  $this->sqldriver::INSERT_REPLACE : $this->sqldriver::INSERT_IGNORE) . " INTO user "
			. "(user, password, can_login, can_edit, can_delete, can_share, can_download, can_upload, is_admin)"
			. "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?), (?, ?, ?, ?, ?, ?, ?, ?, ?)";

		$stm = $this->pdo->prepare($sql)
				   ->execute([
					   $adminusername, $adminpassword, 1, 1, 1, 1, 1, 1, 1,
					   'guest', '', 0, 0 ,0, 0, 1, 0, 0
				   ]);
		return 0;
	}
}
