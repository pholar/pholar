<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar;

use Pholar\Exceptions\QueryException;

use Psr\Http\Message\ServerRequestInterface as Request;
use Envms\FluentPDO\Queries\Select;

define('PAGE_PARAM', 'p');


class Pagination
{
	/** @var Select */
	public $query;

	/** @var int */
	public $pages;

	/** @var int */
	public $page;

	/** @var int */
	public $count;

	/** @var int */
	public $perpage;

	/** @var Request */
	private $request;
	
	public function __construct(Select $query, Request $request, int $perpage = 50)
	{
		$this->request = $request;
		$this->query = $query;
		$this->perpage = $perpage;
		$this->page = 1;
		$this->count = $this->getCount();
		
		$this->pages = (int) ceil($this->count / $this->perpage);
		
		
		$params = $request->getQueryParams();
		if (isset($params[PAGE_PARAM])) {
			$this->page = intval($params[PAGE_PARAM]);
			if ($this->page < 1) {
				$this->page = 1;
			}
			if ($this->page > $this->pages) {
				$this->page = $this->pages;
			}
		}
		
		$startfrom = ($this->page - 1) * $this->perpage;
		
		$query->limit($this->perpage)->offset($startfrom);
	}

	/**
	 * Get query's rows count
	 */
	private function getCount() : int
	{
		// TODO questo è brutto
		try {
			$it = $this->query->getIterator();
		} catch (\Exception $e) {
			throw new QueryException("Query error {$e->getMessage()} '". $this->query->getQuery(false) ."'");
		}
		return count($it->fetchAll(\PDO::FETCH_COLUMN, 0));
	}

	/**
	 * Get url to page $page
	 *
	 * @param $page int
	 * @return string : the url
	 */
	public function getUrlFor(int $page) : string
	{
		$params = $this->request->getQueryParams();
		$params[PAGE_PARAM] = $page;
		$query = [];
		foreach ($params as $k => $v) {
			$query[] = "$k=$v";
		}
		$query = implode("&", $query);
		
		$uri = $this->request->getUri()->withQuery($query);
		return $uri;
	}

	/**
	 * Get url to first page
	 */
	public function getUrlForFirst() : string
	{
		return $this->getUrlFor(1);
	}

	/**
	 * Get url to last page
	 */
	public function getUrlForLast() : string
	{
		return $this->getUrlFor($this->pages);
	}
	
	public function __toString() : string
	{
		return $this->page . "/" . $this->pages;
	}
}
