<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar;

use Symfony\Component\Process\ExecutableFinder;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class Utils
{
	/**
	 * Check if $string starts with $chars
	 *
	 * @param string $string
	 * @param string $chars
	 * @return bool
	 */
	public static function startsWith(string $string, string $chars) : bool
	{
		$l = strlen($chars);
		return substr($string, 0, $l) == $chars;
	}

	/**
	 * Check if $string ends with $chars
	 *
	 * @param string $string
	 * @param string $chars
	 * @return bool
	 */
	public static function endsWith(string $string, string $chars) : bool
	{
		$length = strlen($chars);

		return $length === 0 ||
		(substr($string, -$length) === $chars);
	}

	/**
	 * Tokenize search parameters, including double or single quoted keys.
	 *
	 * @param string $query String to tokenize
	 * @return array<string|array<string>> Tokens
	 */
	public static function tokenize(string $query = null) : array
	{
		if (is_null($query)) {
			return [];
		}
		// remove quotes from single words ("word" -> word, "two words" -> "two words")
		$query = preg_replace('|"([^ "]*)"|', '$1', $query);
		$query = preg_replace("|'([^ ']*)'|", '$1', $query);

		$tokens = [];
		$token = strtok($query, ' ');
		while ($token) {
			// find double quoted tokens
			if ($token[0]=='"' || strpos($token, ':"')) {
				$token .= ' '.strtok('"');//.'"';
				$token = str_replace(':"', ':', trim($token, '" '));
			}
			// find single quoted tokens
			if ($token[0]=="'" || strpos($token, ":'")) {
				$token .= ' '.strtok("'");//."'";
				$token = str_replace(":'", ':', trim($token, "' "));
			}

			// split on ':'
			if (strpos($token, ":")) {
				$token = explode(":", $token, 2);
			}
			$tokens[] = $token;
			$token = strtok(' ');
		}
		return $tokens;
	}

	/**
	 * return true if $strval is a 'true' string : "true", "t", "1"
	 *
	 * @param bool|string $strval
	 * @return bool
	 **/
	public static function istrue(bool|string $strval) : bool
	{
		if (is_bool($strval)) {
			return $strval;
		}
		$strval = strtolower($strval);
		return $strval == "true" || $strval === "t" || $strval === "1";
	}

	/**
	 * Run a command and return output
	 *
	 * @param array<string> $cmd : command and arguments
	 * @return string
	 * @throw ProcessFailedException
	 */
	public static function runcmd(array $cmd) : string
	{
		if (!self::startsWith($cmd[0], "/")) {
			$exefinder = new ExecutableFinder();
			$fullpathexe = $exefinder->find($cmd[0]);
			if (!is_null($fullpathexe)) {
				$cmd[0] = $fullpathexe;
			}
		}
		$cmd = implode(" ", array_map('escapeshellarg', $cmd));
		$process = new Process($cmd);
		$process->run();
		if (!$process->isSuccessful()) {
			throw new ProcessFailedException($process);
		}
		return trim($process->getOutput());
	}

	/**
	 * Convert a byte size into human readable value
	 *
	 * @param int $size	Size in bytes
	 * @param int $precision Number of decimal values (default: 2)
	 * @return string
	 */
	public static function bytes2str(int $size, int $precision = 2) : string
	{
		$precision = 2;
		$units = array('B', 'kiB', 'MiB', 'GiB', 'TiB');

		$bytes = max($size, 0);
		$pow = floor(($bytes ? log($bytes) : 0) / log(1024));
		$pow = min($pow, count($units) - 1);

		$bytes /= pow(1024, $pow);

		return round($bytes, $precision) . " " . $units[$pow];
	}
}
