<?php
/*
	Copyright (c) 2023 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace Pholar;

use Psr\Log\LoggerInterface;

use Envms\FluentPDO\Queries\Select;
use Envms\FluentPDO\Query;
use Envms\FluentPDO\Literal;

use Pholar\Settings;
use Pholar\SQL\SQLDriver;
use Pholar\Tasks\Status;
use Pholar\Exceptions\TaskRescheduled;
use Pholar\Exceptions\QueryException;

class TaskMgr
{
	/** @var Settings */
	protected $settings;

	/** @var SQLDriver */
	protected $sqldriver;

	/** @var LoggerInterface */
	protected $logger;

	/** @var Query */
	protected $db;

	public function __construct(
		Settings $settings,
		SQLDriver $sqldriver,
		LoggerInterface $logger,
		Query $db,
	) {
		$this->settings = $settings;
		$this->sqldriver = $sqldriver;
		$this->logger =$logger;
		$this->db = $db;
	}

	public function getTasks() : Select
	{
		return $this->db->from("task");
	}

	/**
	 * Schedule a new task
	 *
	 * @param string $taskcls   Task class name
	 * @param TaskArgs $args    Arguments for task
	 * @param int $require      Id of task required to be completed to run this task
	 * @return int              New Task id
	 **/
	public function schedule(string $taskcls, array $args, int $require = null) : int
	{
		$photo_id = new Literal('NULL');
		if (key_exists('id', $args)) {
			// TODO: let's pretend that 'id' in args is always a photo id
			$photo_id = (int) $args['id'];
		}

		ksort($args);
		$query = $this->db->insertInto('task')
			->values([
				'photo_id' => $photo_id,
				'priority' => 0,
				'task' => $taskcls,
				'args' => json_encode($args),
				'dependson' => $require,
			]);
		try {
			/** @throws \PDOException */
			$id = $query->execute();
		} catch (\PDOException $e) {
			throw new QueryException("Query error {$e->getMessage()} :: '". $query->getQuery(false) ."'");
		}
		return $id;
	}

	/**
	 * Reschedule a task
	 *
	 * @param int $taskid
	 * @param TaskData $data
	 * @return int           Rescheduled task id
	 */
	public function reschedule(int $taskid, array $data) : int
	{
		$data['status'] = Status::PENDING;
		$data['started_at'] = new Literal("NULL");
		$data['finished_at'] = new Literal("NULL");
		$data['output'] = new Literal("NULL");

		$this->update($taskid, $data);
		return $data['id'];
	}


	/**
	 * Update task data
	 *
	 * @param int $taskid    Task id
	 * @param TaskData $data Task data to update
	 * @return void
	 */
	public function update(int $taskid, array $data) : void
	{
		if (key_exists('args', $data)) {
			$data['args'] = json_encode($data['args']);
		}

		$this->db->update('task', $data)->where('id', $taskid)->execute();
	}


	/**
	 * Get task by Task id
	 *
	 * @param int $taskid
	 * @return ?TaskData
	 */
	public function getTaskById(int $taskid) : ?array
	{
		$t = $this->db->from('task')->where('id', $taskid)->fetch();
		return $t === false ? null : $t;
	}


	/**
	 * Get task by arguments
	 *
	 * @param string $taskcls
	 * @param TaskArgs $args
	 * @return TaskData[]
	 */
	public function getTasksByArgs(string $taskcls, array $args) : array
	{
		ksort($args);
		$sargs = json_encode($args);
		$t = $this->db->from('task')->where('args', $args)->fetchAll();
		return $t;
	}


	/**
	 * Get task by Photo id
	 *
	 * Get all tasks associated with a photo
	 *
	 * @param int $photo_id
	 * @return TaskData[]
	 */
	public function getTasksByPhotoId(int $photo_id) : array
	{
		$t = $this->db->from('task')->where('photo_id', $photo_id)->fetchAll();
		return $t;
	}


	/**
	 * Get next task to be processed
	 *
	 * @return ?TaskData
	 */
	public function getNextTask() : ?array
	{
		// il primo task, in stato "PENDING"
		//   ordina per priorità decrescente
		//      (non mi piace, ma poi vediamo. in teoria ci potrebbe essere un esecutore per ogni priorità, che diventano quindi "canali")
		//      il cui albero di "dependson" non ha stati "PENDING". eh.
		$t = $this->db->from('task')
			->leftJoin('task AS req ON task.dependson = req.id')
			->where('task.status', Status::PENDING)
			->where('(task.dependson is null or req.status = ?)', Status::PENDING)
			->orderBy('task.id ASC')
			->fetch();
		return $t === false ? null : $t;
	}

/*
	public function waitForTask()
	{
		// qui mi piacerebbe avere un sistema tipo pubsub,
		// in modo che nel caso l'esecutore non abbia task, si fermi in attesa che ne arrivino.
		// da vedere.
		// con postgresql dovrei poterlo fare, con mysql e sqlite non credo.
		// pero' posso anche semplicemente creare un FIFO su disco e leggerlo qui, in attesa che
		// "schedule()" o "reschedule()" non ci scrivano dentro
	}
*/
	
	/**
	 * Execute a task
	 *
	 * @param TaskData $task
	 * @return void
	 */
	public function execute(array $task) : void
	{
		$taskid = $task['id'];
		$NOW = new Literal($this->sqldriver::NOW);

		$this->update($taskid, [
			"status" => Status::RUNNING,
			"output" => "",
			"started_at" => $NOW,
		]);

		$res = null;
		try {
			$taskcls = $task['task'];
			$taskobj = App::getApp()->getContainer()->get($taskcls);
			$args = json_decode($task['args'], true);

			$res = $taskobj->run($taskid, $args);
		} catch (TaskRescheduled $e) {
			// task rescheduled itself. Nothing else to do.
			return;
		} catch (\Exception $e) {
			$this->update($taskid, [
				"status" => Status::ERROR,
				"output" => "$e : {$e->getMessage()}",
				"finished_at" => $NOW,
			]);
			throw $e;
		}

		if (is_null($res)) {
			$res = new Literal("NULL");
		}
		$this->update($taskid, [
			"status" => Status::DONE,
			"output" => $res,
			"finished_at" => $NOW,
		]);
	}
}
