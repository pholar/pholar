<?php
/*
	Copyright (c) 2020 Fabio Comuni

	This file is part of Pholar.

	Pholar is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3 of the
	License, or (at your option) any later version.

	Pholar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Pholar.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Pholar;

use Psr\Log\LoggerInterface;
use Envms\FluentPDO\Query;
use Envms\FluentPDO\Queries\Select;

class CollectionMgr
{
	/** @var LoggerInterface */
	protected $logger;

	/** @var Query */
	protected $db;


	public function __construct(
		LoggerInterface $logger,
		Query $db
	) {
		$this->logger = $logger;
		$this->db = $db;
	}

	
	/**
	 * Get all collections
	 *
	 * @return Select
	 */
	public function all() : Select
	{
		return $this->db->from("collection")->orderBy("name");
	}

	/**
	 * Get collection by id
	 *
	 * @param int $id Collection id
	 * @return ?CollectionData
	 */
	public function byId(int $id) : ?array
	{
		$u = $this->db->from("collection", $id)->fetch();
		return $u === false ? null : $u;
	}


	/**
	 * Get all collections with number of photos per collection
	 *
	 * @return Select
	 */
	public function photoCount() : Select
	{
		return $this->db->from("collection")
						->select("collection.*")
						->select("count(photo_collection.photo_id) as n_photos")
						->leftJoin('photo_collection ON collection.id = photo_collection.collection_id')
						->groupBy("collection.id")
						->orderBy("collection.name");
	}

	/**
	 * Get a photo from a colllection given the collection id and the photo id
	 *
	 * @param int $cid Collection id
	 * @param int $pid Photo id
	 * @return ?PhotoData
	 */
	public function photoById(int $cid, int $pid) : ?array
	{
		$u = $this->db->from("photo_collection")
			->select("photo.*")
			->leftJoin("photo ON photo.id = photo_collection.photo_id")
			->where("photo.id", $pid)
			->where("photo_collection.collection_id", $cid)
			->fetch();
		return $u === false ? null : $u;
	}


	/**
	 * Add a collection and return collection id
	 *
	 * @param string $name Collection name
	 * @param string $comment
	 * @return int Collection id
	 */
	public function add(string $name, string $comment = "") : int
	{
		$r = $this->db->from('collection')
						->select('id')
						->where('name', $name)
						->fetch();
		if ($r === false) {
			$r = $this->db->insertInto(
				'collection',
				['name' => $name, 'comment' => $comment]
			)->execute();
		} else {
			$r = $r['id'];
		}
		return $r;
	}

	/**
	 * Add a photo to collection id
	 *
	 * @params int $id Collection id
	 * @params int $photoid Photo id
	 */
	public function addPhoto(int $id, int $photoid) : void
	{
		try {
			$this->db->insertInto('photo_collection')
					->values(['photo_id'=>$photoid, 'collection_id'=>$id])
					->execute();
		} catch (\Exception $e) {
			if (strpos(
				$e->getMessage(),
				"UNIQUE constraint failed: photo_collection.photo_id, photo_collection.collection_id"
			) === false
			) {
				throw $e;
			}
		}
	}

	/**
	 * Remove a photo from a collection by idate
	 *
	 * @params int $id Collection id
	 * @params int $photoid Photo id
	 */
	public function removePhoto(int $id, int $photoid) : void
	{
		$this->db->deleteFrom('photo_collection')
			->where('photo_id', $photoid)
			->where('collection_id', $id)
			->execute();
	}

	/**
	 * Delete collection by id
	 *
	 * @param int $id Collection id
	 */
	public function delete(int $id) : void
	{
		$this->db->deleteFrom('collection', $id)->execute();
	}

	/**
	 * Save collection
	 *
	 * @param CollectionData  $collection
	 */
	public function save(array $collection) : void
	{
		$id = $collection['id'] ?? null;
		if (is_null($id)) {
			$this->db->insertInto('collection', $collection)->execute();
		} else {
			unset($collection['id']);
			$this->db->update('collection', $collection, $id)->execute();
		}
	}
}
