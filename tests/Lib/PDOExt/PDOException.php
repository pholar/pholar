<?php
namespace Tests\Lib\PDOExt;

use \Throwable;

class PDOException extends \PDOException
{
	/** @var string */
	protected $statement;

	public function __construct(string $statement, string $message = "", int $code = 0, Throwable $previous = null)
	{
		parent::__construct($message, $code, $previous);
		$this->statement = $statement;
	}

	public function getStatement() : string
	{
		return $this->statement;
	}
}
