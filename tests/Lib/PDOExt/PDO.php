<?php
namespace Tests\Lib\PDOExt;

class PDO extends \PDO
{
	/**
	 * Logged queries.
	 * @var array
	 */
	protected $log = [];

	/**
	 * @inheritDoc
	 */
	public function __construct($dsn, $username = null, $passwd = null, $options = null)
	{
		parent::__construct($dsn, $username, $passwd, $options);
		$this->setAttribute(self::ATTR_STATEMENT_CLASS, [PDOStatement::class, [$this]]);
	}

	/**
	 * @inheritDoc
	 */
	public function exec($statement) : int|false
	{
		$start = microtime(true);
		$result = parent::exec($statement);
		$this->addLog($statement, microtime(true) - $start);
		return $result;
	}

	/**
	 * @inheritDoc
	 */
	public function query(string $statement, ?int $mode = PDO::ATTR_DEFAULT_FETCH_MODE, mixed ...$fetchModeArgs) : PDOStatement|false
	{
		$start = microtime(true);
		$result = parent::query($statement, $mode, ...$fetchModeArgs);
		$this->addLog($statement, microtime(true) - $start);
		return $result;
	}

	/**
	 * @inheritDoc
	 */
	public function prepare($statement, $options = null) : PDOStatement|false 
	{
		if (is_null($options)) {
			$options = [];
		}

		try {
			return parent::prepare($statement, $options);
		} catch (\PDOException $e) {
			throw new PDOException(
				$statement,
				$e->getMessage(),
				(int)$e->getCode(),
				$e->getPrevious()
			);
		}
	}

	/**
	 * Add query to logged queries.
	 * @param string $statement
	 * @param float $time Elapsed seconds with microseconds
	 */
	public function addLog($statement, $time)
	{
		$query = [
			'statement' => $statement,
			'time' => $time * 1000
		];
		$this->log[] = $query;
	}

	/**
	 * Return logged queries.
	 * @return array<array{statement:string, time:float}> Logged queries
	 */
	public function getLog()
	{
		return $this->log;
	}
}
