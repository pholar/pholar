<?php
/**
 * Extends SystemSettings and set settings values for testings
 */
namespace Tests;

use Pholar\SystemSettings;
use Pholar\Meta\Meta;

class TestSystemSettings extends SystemSettings
{
	protected function getUserEnvFile() : string
	{
		$env = $this->getEnv();
		$env = str_replace(["..", ".", "/", "\\"], "", $env);
		return dirname(__FILE__) . "/etc/$env.settings.php";
	}
}