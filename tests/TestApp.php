<?php
/**
 * Extends App class to allow TestAppTrait to set the
 * global Slim\App instance (returned by Pholar\App::getApp())
 */
namespace Tests;

use Slim\App as SlimApp;

class TestApp extends \Pholar\App
{
	public static function getApp() : SlimApp
	{
		if (is_null(self::$app)) {
			self::$app = self::setup();
		}
		return self::$app;
	}
	
	public static function setup() : SlimApp
	{
		return self::$app;
	}

	public static function setApp(SlimApp $app) : void
	{
		self::$app = $app;
	}
}
