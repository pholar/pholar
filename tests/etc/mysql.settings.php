<?php
use Pholar\Meta\Meta;

return [
	'db' => "mysql://pholar:pholar@localhost/pholar",
	'debug' => true,
	'app' => [
		'storage' => dirname(dirname(__FILE__)) . "/fixtures/assets",
		'writeback' => Meta::WRITEBACK_NONE,
	],
	'paths' => [
		'etc' => dirname(dirname(__FILE__)) . '/etc',
		'public' => dirname(dirname(__FILE__)) . '/public',
	],
];