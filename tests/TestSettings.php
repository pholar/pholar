<?php
/**
 * Extends SystemSettings and set settings values for testings
 */
namespace Tests;

use Pholar\Settings;
use Pholar\Meta\Meta;

class TestSettings extends Settings
{
	protected function getUserEnvFile() : string
	{
		$env = $this->getEnv();
		$env = str_replace(["..", ".", "/", "\\"], "", $env);
		return dirname(__FILE__) . "/etc/$env.settings.php";
	}
}