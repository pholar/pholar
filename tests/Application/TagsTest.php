<?php
declare(strict_types=1);

namespace Tests\Appplication;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use PHPUnit\Framework\TestCase;
use Tests\AppTestTrait;
use Envms\FluentPDO\Literal;
use Envms\FluentPDO\Query;

class TagsTest extends TestCase
{
	use AppTestTrait;

	protected $app;

	protected function setUp(): void
	{
		$this->logout();
		$this->app = $this->getAppInstance();
		$this->setupDbTables($this->app);

		$this->app->getContainer()->get(Query::class)
			->insertInto('photo', [
				['id' => 1, 'recordmtime' => '2020-06-29 15:19:24', 'filename' => 'test1.jpg', 'location' => '/', 'format' => 'image/jpeg'],
				['id' => 2, 'recordmtime' => '2020-06-29 15:30:24', 'filename' => 'test.svg', 'location' => '/images/svg', 'format' => 'image/svg+xml'],
				['id' => 3, 'recordmtime' => '2020-06-29 15:40:24', 'filename' => 'Lorem Ipsum.doc', 'location' => '/documents', 'format' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document']
			])
			->execute();
		$this->app->getContainer()->get(Query::class)
			->insertInto('tag', [
				['id' => 1, 'tag' => 'tag1'],
				['id' => 2, 'tag' => 'tag2'],
			])
			->execute();
		$this->app->getContainer()->get(Query::class)
			->insertInto('photo_tag', [
				['photo_id' => 1, 'tag_id' => 1],
				['photo_id' => 2, 'tag_id' => 1],
				['photo_id' => 2, 'tag_id' => 2],
			])
			->execute();
	}

	protected function tearDown(): void
	{
		$this->app = null;
	}

	public function testTagSearch1()
	{
		$this->loginAdmin();
		$request = $this->createRequest('GET', '/?q=tag:tag1');
		$response = $this->app->handle($request);
		$reply = $response->getBody()->__toString();
		$this->assertEquals(200, $response->getStatusCode(), $reply);
		$this->assertStringContainsString('2 photos', $reply);
	}

	public function testTagSearch2()
	{
		$this->loginAdmin();
		$request = $this->createRequest('GET', '/?q=tag:tag2');
		$response = $this->app->handle($request);
		$reply = $response->getBody()->__toString();
		$this->assertEquals(200, $response->getStatusCode(), $reply);
		$this->assertStringContainsString('1 photos', $reply);
	}

	public function testTagEdit()
	{
		$tagid = 1;
		$data = ['tag' => 'newname',];

		$this->loginAdmin();
		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', "/tag/$tagid", [], [], [], $body);
		$response = $this->app->handle($request);

		$this->assertEquals("", $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());
		$tag = $this->app->getContainer()->get(Query::class)->from('tag', $tagid)->fetch();
		$this->assertEquals($data['tag'], $tag['tag']);
		$this->assertEquals(['/'], $response->getHeader('Location'));
	}

	public function testTagEditWithRedirect()
	{
		$tagid = 1;
		$data = ['redirect' => '/test', 'tag' => 'newname',];

		$this->loginAdmin();
		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', "/tag/$tagid", [], [], [], $body);
		$response = $this->app->handle($request);

		$this->assertEquals("", $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());
		$tag = $this->app->getContainer()->get(Query::class)->from('tag', $tagid)->fetch();
		$this->assertEquals($data['tag'], $tag['tag']);
		$this->assertEquals(['/test'], $response->getHeader('Location'));
	}

	public function testInvalidTagEdit()
	{
		// setup flash mock
		$flash= $this->createMock(\Slim\Flash\Messages::class);
		$flash->expects($this->once())
				 ->method('addMessage')
				 ->with($this->equalTo('error'), $this->equalTo('Tag not found'));
		$this->app->getContainer()->set(\Slim\Flash\Messages::class, $flash);


		$tagid = 3;
		$data = ['tag' => 'newname',];

		$this->loginAdmin();
		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', "/tag/$tagid", [], [], [], $body);
		$response = $this->app->handle($request);

		$this->assertEquals("", $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());
		$this->assertEquals(['/'], $response->getHeader('Location'));
	}

	public function testTagEditEmptyName()
	{
		// setup flash mock
		$flash= $this->createMock(\Slim\Flash\Messages::class);
		$flash->expects($this->once())
				 ->method('addMessage')
				 ->with($this->equalTo('error'), $this->equalTo("Tag name can't be empty"));
		$this->app->getContainer()->set(\Slim\Flash\Messages::class, $flash);


		$tagid = 1;
		$data = ['tag' => '',];

		$this->loginAdmin();
		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', "/tag/$tagid", [], [], [], $body);
		$response = $this->app->handle($request);

		$this->assertEquals("", $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());
		$this->assertEquals(['/'], $response->getHeader('Location'));
	}

	public function testTagDelete()
	{
		$db = $this->app->getContainer()->get(Query::class);
		$tagid = 1;

		$this->loginAdmin();
		$request = $this->createRequest('POST', "/tag/$tagid/delete");
		$response = $this->app->handle($request);

		$this->assertEquals("", $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());
		$this->assertEquals(['/'], $response->getHeader('Location'));

		// TODO: this should be better in unittests for TagMgr
		$tagcount = $db->from('tag', $tagid)->count();
		$this->assertEquals(0, $tagcount);
		$photocount = $db->from('photo')
						  ->leftJoin("photo_tag ON photo_tag.photo_id = photo.id")
						  ->where("photo_tag.tag_id", $tagid)
						  ->count();
		$this->assertEquals(0, $photocount);
	}

	public function testInvalidTagDelete()
	{
		$tagid = 3;

		$this->loginAdmin();
		$request = $this->createRequest('POST', "/tag/$tagid/delete");
		$response = $this->app->handle($request);

		$this->assertEquals("", $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());
		$this->assertEquals(['/'], $response->getHeader('Location'));
	}
}
