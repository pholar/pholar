<?php
declare(strict_types=1);

namespace Tests\Appplication;

use Psr\Http\Message\ResponseInterface as Response;
use PHPUnit\Framework\TestCase;
use Tests\AppTestTrait;
use Envms\FluentPDO\Query;

use Slim\Flash\Messages;
use Pholar\PhotoMgr;

class FoldersTest extends TestCase
{
	use AppTestTrait;
	protected $app;
	protected $testroot;

	protected function setUp(): void
	{
		$this->logout();
		$this->app = $this->getAppInstance();
		$this->setupDbTables($this->app);

		$photomgr = $this->app->getContainer()->get(PhotoMgr::class);
		$this->testroot = $photomgr->absPath("/testroot");
	}

	protected function tearDown(): void
	{
		@rmdir($this->testroot);
		$this->app = null;
	}

	public function testForbidden()
	{
		$request = $this->createRequest('POST', '/system/folder');
		$response = $this->app->handle($request);
		$this->assertEquals(403, $response->getStatusCode());
	}

	public function testCannotUpload()
	{
		$this->login('noupload');
		$request = $this->createRequest('POST', '/system/folder');
		$response = $this->app->handle($request);
		$this->assertEquals(403, $response->getStatusCode());
	}

	public function testInvalidParamRoot()
	{
		$this->loginAdmin();
		$request = $this->createRequest('POST', '/system/folder');
		$response = $this->app->handle($request);
		$this->assertEquals(400, $response->getStatusCode());
	}

	public function testInvalidParamName()
	{
		$this->loginAdmin();
		$data = [ 'root' => '/' ];
		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', '/system/folder', [], [], [], $body);
		$response = $this->app->handle($request);
		$this->assertEquals(400, $response->getStatusCode());
	}

	public function testInvalidPath()
	{
		// must call "Message::addMessage" with an error
		// setup flash mock
		$flash= $this->createMock(Messages::class);
		$flash->expects($this->once())
				 ->method('addMessage')
				 ->with($this->equalTo('error'));
		$this->app->getContainer()->set(Messages::class, $flash);
		
		$this->loginAdmin();
		$data = [
			'root' => '/', 
			'name' => '../../../',
			'redirect' => '/test',
		];
		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', '/system/folder', [], [], [], $body);
		$response = $this->app->handle($request);
		$this->assertEquals(302, $response->getStatusCode());
		$this->assertEquals(['/test'], $response->getHeader('Location'));
	}

	public function testInvalidRoot()
	{
		$this->loginAdmin();
		$data = [ 'root' => '/non-exists', 'name' => 'something', ];
		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', '/system/folder', [], [], [], $body);
		$response = $this->app->handle($request);
		$this->assertEquals(400, $response->getStatusCode());
	}

	public function testFailMkdir()
	{
		// setup flash mock
		$flash= $this->createMock(Messages::class);
		$flash->expects($this->once())
				->method('addMessage')
				->with($this->equalTo('error'));
		$this->app->getContainer()->set(Messages::class, $flash);
		
		$this->assertTrue(mkdir($this->testroot, 0555)); // Test root folder. Should not failt.
		$this->loginAdmin();
		$data = [
			'root' => '/testroot',
			'name' => 'something',
		];
		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', '/system/folder', [], [], [], $body);
		$response = $this->app->handle($request);
		$this->assertEquals("", $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());
	}

	public function testSuccessDefaultRedirect()
	{
		// setup flash mock
		$flash= $this->createMock(Messages::class);
		$flash->expects($this->once())
				->method('addMessage')
				->with($this->equalTo('success'));
		$this->app->getContainer()->set(Messages::class, $flash);
		
		$this->loginAdmin();
		$data = [
			'root' => '/',
			'name' => 'testroot',
		];
		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', '/system/folder', [], [], [], $body);
		$response = $this->app->handle($request);
		$this->assertEquals("", $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());
		$this->assertEquals(['/'], $response->getHeader('Location'));
	}

	public function testSuccessCustomRedirect()
	{
		// setup flash mock
		$flash= $this->createMock(Messages::class);
		$flash->expects($this->once())
				->method('addMessage')
				->with($this->equalTo('success'));
		$this->app->getContainer()->set(Messages::class, $flash);
		
		$this->loginAdmin();
		$data = [
			'root' => '/',
			'name' => 'testroot',
			'redirect' => '/test',
		];
		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', '/system/folder', [], [], [], $body);
		$response = $this->app->handle($request);
		$this->assertEquals("", $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());
		$this->assertEquals(['/test'], $response->getHeader('Location'));
	}

}