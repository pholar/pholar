<?php
declare(strict_types=1);

namespace Tests\Appplication;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use PHPUnit\Framework\TestCase;
use Tests\AppTestTrait;
use Envms\FluentPDO\Query;

use Pholar\SystemSettings;

class PhotosTest extends TestCase
{
	use AppTestTrait;

	protected $app;
	protected $settings;

	protected function setUp(): void
	{
		$this->logout();
		$this->app = $this->getAppInstance();
		$this->setupDbTables($this->app);
		$this->settings = $this->app->getContainer()->get(SystemSettings::class);

		copy($this->settings->get('app', 'storage') . '/test1.jpg', $this->settings->get('app', 'storage') . '/test1.bkp');
		@mkdir($this->settings->get('paths', 'public') . '/thumb', 0777, true);

		$this->app->getContainer()->get(Query::class)
			->insertInto('photo', [
				['id' => 1, 'recordmtime' => '2020-06-29 15:19:24', 'filename' => 'test1.jpg', 'location' => '/', 'format' => 'image/jpeg'],
				['id' => 2, 'recordmtime' => '2020-06-29 15:30:24', 'filename' => 'test.svg', 'location' => '/images/svg', 'format' => 'image/svg+xml'],
				['id' => 3, 'recordmtime' => '2020-06-29 15:40:24', 'filename' => 'Lorem Ipsum.doc', 'location' => '/documents', 'format' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document']
			])
			->execute();
	}

	protected function tearDown(): void
	{
		copy($this->settings->get('app', 'storage') . '/test1.bkp', $this->settings->get('app', 'storage') . '/test1.jpg');
		unlink($this->settings->get('app', 'storage') . '/test1.bkp');
		$this->delTree($this->settings->get('paths', 'public'));
		$this->app = null;
	}

	public function testLoginRedirect()
	{
		$request = $this->createRequest('GET', '/');
		$response = $this->app->handle($request);
		$this->assertEquals(302, $response->getStatusCode());
	}

	public function testLoginRedirectWithReRedirect()
	{
		$request = $this->createRequest('GET', '/photo/1');
		$response = $this->app->handle($request);
		$this->assertEquals(302, $response->getStatusCode());
		$this->assertEquals(["/login?redirect=%2Fphoto%2F1%3F"], $response->getHeader('Location'));
	}

	public function testPhotosList()
	{
		$this->loginAdmin();
		$request = $this->createRequest('GET', '/');
		$response = $this->app->handle($request);
		$this->assertEquals(200, $response->getStatusCode());
		$this->assertStringContainsString('3 photos', $response->getBody()->__toString());
	}

	public function testPhotosListSearch()
	{
		$this->loginAdmin();
		$request = $this->createRequest('GET', '/?q=test');
		$response = $this->app->handle($request);
		$this->assertEquals(200, $response->getStatusCode());
		$this->assertStringContainsString('2 photos', $response->getBody()->__toString());
	}

	public function testPhotoDetails()
	{
		$this->loginAdmin();
		$request = $this->createRequest('GET', '/photo/1');
		$response = $this->app->handle($request);
		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testInexistentPhotoDetails()
	{
		$this->loginAdmin();
		$request = $this->createRequest('GET', '/photo/255');
		$response = $this->app->handle($request);
		$this->assertEquals(404, $response->getStatusCode());
	}

	public function editPhoto(string $redir = null )
	{
		$data = [
			'datetime' => '2020-06-29 15:19:24',
			'rights' => 'rights',
			'creator' => 'creator',
			'rating' => '1',
			'title' => 'title',
			'comment' => 'comment',
			'latitude' => '1.2',
			'longitude' => '1.3',
			'tags' => 'pippo,pluto',
		];
		if (!is_null($redir)) {
			$data['redirect'] = $redir;
		} else {
			$redir = "/";
		}

		$this->loginAdmin();
		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST',	'/photo/1/edit', [], [], [], $body);
		$response = $this->app->handle($request);
		$this->assertEquals("", $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());
		$this->assertEquals([$redir], $response->getHeader('Location'));

		// check saved data match form
		$query = $this->app->getContainer()->get(Query::class);
		$photo = $query->from('photo', 1)->fetch();
		$tags = $query->from('tag')
					->leftJoin('photo_tag ON photo_tag.tag_id = tag.id')
					->where('photo_tag.photo_id',1)
					->fetchAll();
		$tags = implode(",", array_map(function($r) { return $r['tag']; }, $tags));
		$this->assertEquals($tags, $data['tags'], "photo tags '$tags'");
		unset($data['tags']);
		unset($data['redirect']);
		foreach($data as $field=>$value) {
			$this->assertEquals($value, $photo[$field], "photo field '$field'");
		}
	}

	public function testEditPhotoWithDefaultRedirect()
	{
		$this->editPhoto();
	}

	public function testEditPhotoWithCustomRedirect()
	{
		$this->editPhoto("/photo/1");
	}

	public function testEditPhotoWithInvalidId()
	{
		$this->loginAdmin();
		$request = $this->createRequest('POST', '/photo/255/edit');
		$response = $this->app->handle($request);
		$this->assertEquals(404, $response->getStatusCode());
	}

	public function testEditPhotoWithoutPermission()
	{
		$this->login('noedit');
		$request = $this->createRequest('POST', '/photo/1/edit');
		$response = $this->app->handle($request);
		$this->assertEquals(403, $response->getStatusCode());
	}

	public function testDeletePhoto()
	{
		$this->loginAdmin();
		$request = $this->createRequest('POST', '/photo/1/delete');
		$response = $this->app->handle($request);
		$this->assertEquals("", $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());
		$this->assertEquals(['/'], $response->getHeader('Location'));

		$photo = $this->app->getContainer()->get(Query::class)->from('photo', 1)->fetch();
		$this->assertFalse($photo, "Photo exists in database");

		$ignorefile = $this->settings->get('app', 'storage') . '/.test1.jpg.ignore';
		$this->assertTrue(file_exists($ignorefile), "Ignore file '$ignorefile' exists");
		unlink($ignorefile);
	}

	public function testDeletePhotoWithCustomRedirect()
	{
		$this->loginAdmin();
		$body = $this->formEncoded(['redirect' => '/test']);
		$request = $this->createRequest('POST', '/photo/1/delete', [], [], [], $body);
		$response = $this->app->handle($request);
		$this->assertEquals("", $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());
		$this->assertEquals(['/test'], $response->getHeader('Location'));

		$photo = $this->app->getContainer()->get(Query::class)->from('photo', 1)->fetch();
		$this->assertFalse($photo, "Photo exists in database");

		$ignorefile = $this->settings->get('app', 'storage') . '/.test1.jpg.ignore';
		$this->assertTrue(file_exists($ignorefile), "Ignore file '$ignorefile' exists");
		unlink($ignorefile);
	}

	public function testDeletePhotoWithInvalidId()
	{
		$this->loginAdmin();
		$request = $this->createRequest('POST', '/photo/255/delete');
		$response = $this->app->handle($request);
		$this->assertEquals(404, $response->getStatusCode());
	}

	public function testDeletePhotoWithoutPermission()
	{
		$this->login('nodelete');
		$request = $this->createRequest('POST', '/photo/2/delete');
		$response = $this->app->handle($request);
		$this->assertEquals(403, $response->getStatusCode());
	}

	public function testThumbnail()
	{
		$size = 10;
		$id = 1;
		$thumbfile = $this->settings->get('paths', 'public') . "/thumb/$size/$id.jpg";

		$this->loginAdmin();

		$request = $this->createRequest('GET', "/system/thumbnail/$size/$id");
		$response = $this->app->handle($request);

		$this->assertEquals(200, $response->getStatusCode());
		$this->assertFileExists($thumbfile);

		// diciamo che ci fidiamo che il thumbnail è giusto.
	}

	public function testInvalidThumbnail()
	{
		$size = 10;
		$id = 4;
		$thumbfile = $this->settings->get('paths', 'public') . "/thumb/$size/$id.jpg";

		$this->loginAdmin();

		$request = $this->createRequest('GET', "/system/thumbnail/$size/$id");
		$response = $this->app->handle($request);

		$this->assertEquals(404, $response->getStatusCode());
	}

	public function testThumbnailOfDeletedSource()
	{
		$this->app->getContainer()->get(Query::class)
			->insertInto('photo', [
				['id' => 4, 'recordmtime' => '2020-06-29 15:19:24', 'filename' => 'test2.jpg', 'location' => '/', 'format' => 'image/jpeg'],
			])
			->execute();

		$size = 10;
		$id = 4;
		$thumbfile = $this->settings->get('paths', 'public') . "/thumb/$size/$id.jpg";

		$this->loginAdmin();

		$request = $this->createRequest('GET', "/system/thumbnail/$size/$id");
		$response = $this->app->handle($request);

		$this->assertEquals(404, $response->getStatusCode());
	}

	public function testThumbnailAsAnonymousUser()
	{
		$size = 10;
		$id = 1;
		$thumbfile = $this->settings->get('paths', 'public') . "/thumb/$size/$id.jpg";

		$request = $this->createRequest('GET', "/system/thumbnail/$size/$id");
		$response = $this->app->handle($request);
		$this->assertEquals("", $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());
	}

	public function testDownload()
	{
		$id = 1;

		$this->loginAdmin();

		$request = $this->createRequest('GET', "/system/source/$id");
		$response = $this->app->handle($request);

		$this->assertEquals(200, $response->getStatusCode());
		$this->assertEquals(['attachment; filename="test1.jpg";'], $response->getHeader('Content-Disposition'));
		$this->assertEquals(['image/jpeg'], $response->getHeader('Content-Type'));
	}

	public function testInline()
	{
		$id = 1;

		$this->loginAdmin();

		$request = $this->createRequest('GET', "/system/source/$id?inline=1");
		$response = $this->app->handle($request);

		$this->assertEquals(200, $response->getStatusCode());
		$this->assertEquals(['inline; filename="test1.jpg";'], $response->getHeader('Content-Disposition'));
		$this->assertEquals(['image/jpeg'], $response->getHeader('Content-Type'));
	}

	public function testCannotDownload()
	{
		$this->login('nodwl');
		$request = $this->createRequest('GET', '/system/source/1');
		$response = $this->app->handle($request);
		$this->assertEquals(403, $response->getStatusCode());
	}
}
