<?php
declare(strict_types=1);

namespace Tests\Appplication;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use PHPUnit\Framework\TestCase;
use Tests\AppTestTrait;
use Envms\FluentPDO\Query;

class CollectionsTest extends TestCase
{
	use AppTestTrait;

	protected $app;

	protected function setUp(): void
	{
		$this->logout();
		$this->app = $this->getAppInstance();
		$this->setupDbTables($this->app);

		$pdo = $this->app->getContainer()->get(Query::class);
		$pdo->insertInto('photo', [
				['id' => 1, 'recordmtime' => '2020-06-29 15:19:24', 'filename' => 'test1.jpg', 'location' => '/', 'format' => 'image/jpeg'],
				['id' => 2, 'recordmtime' => '2020-06-29 15:30:24', 'filename' => 'test.svg', 'location' => '/images/svg', 'format' => 'image/svg+xml'],
				['id' => 3, 'recordmtime' => '2020-06-29 15:40:24', 'filename' => 'Lorem Ipsum.doc', 'location' => '/documents', 'format' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document']
			])
			->execute();
		$pdo->insertInto('collection', [
				['id' => 1, 'name' => 'Test', 'comment' => 'A test collection']
			])
			->execute();
		$pdo->insertInto('photo_collection', [
				['photo_id' => 1, 'collection_id' => 1],
				['photo_id' => 2, 'collection_id' => 1]
			])
			->execute();
	}

	protected function tearDown(): void
	{
		$this->app = null;
	}

	public function testLoginRedirect()
	{
		$request = $this->createRequest('GET', '/collection/1');
		$response = $this->app->handle($request);
		$this->assertEquals('', $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());
		$this->assertEquals(["/login?redirect=%2Fcollection%2F1%3F"], $response->getHeader('Location'));
	}

	public function testPhotosList()
	{
		$this->loginAdmin();
		$request = $this->createRequest('GET', '/collection/1');
		$response = $this->app->handle($request);
		$this->assertEquals(200, $response->getStatusCode());
		$this->assertStringContainsString('2 photos', $response->getBody()->__toString());
	}

	public function testPhotosListSearch()
	{
		$this->loginAdmin();
		$request = $this->createRequest('GET', '/collection/1?q=jpg');
		$response = $this->app->handle($request);
		$this->assertEquals(200, $response->getStatusCode());
		$this->assertStringContainsString('1 photos', $response->getBody()->__toString());
	}

	public function testInexistentCollection()
	{
		$this->loginAdmin();
		$request = $this->createRequest('GET', '/collection/2');
		$response = $this->app->handle($request);
		$this->assertEquals(404, $response->getStatusCode());
	}

	public function  testEditCollection()
	{
		$this->editCollection([
			'collname' => 'new name',
			'collcomment' => 'new comment',
			'redirect' => '/collection/1',
			'action' => 'editcollection',
		]);
	}

	public function  testEditCollectionWithEmptyFields()
	{
		$this->editCollection([
			'collname' => '',
			'collcomment' => '',
			'redirect' => '/collection/1',
			'action' => 'editcollection',
		]);
	}

	public function  testEditInvalidCollection()
	{
		$data = [
			'collname' => 'new title',
			'collcomment' => 'new comment',
			'redirect' => '/collection/2',
			'action' => 'editcollection',
		];
		$this->loginAdmin();
		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', '/collection/2', [], [], [], $body);

		$response = $this->app->handle($request);
		$this->assertEquals(404, $response->getStatusCode(), $response->getBody()->__toString());
	}

	private function editCollection(array $data)
	{
		$this->loginAdmin();
		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', '/collection/1', [], [], [], $body);

		$response = $this->app->handle($request);
		$this->assertEquals("", $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());
		$this->assertEquals([$data['redirect']], $response->getHeader('Location'));

		$collection = $this->app->getContainer()->get(Query::class)->from('collection', 1)->fetch();
		$this->assertEquals($data['collname'], $collection['name'], "collection name");
		$this->assertEquals($data['collcomment'], $collection['comment'], "collection comment");
	}

	public function testCollectionPhotoDetail()
	{
		$this->loginAdmin();
		$request = $this->createRequest('GET', '/collection/1/photo/1');
		$response = $this->app->handle($request);
		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testNotInCollectionPhotoDetail()
	{
		$this->loginAdmin();
		$request = $this->createRequest('GET', '/collection/1/photo/3');
		$response = $this->app->handle($request);
		$this->assertEquals(404, $response->getStatusCode());
	}
}
