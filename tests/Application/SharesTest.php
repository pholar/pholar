<?php
declare(strict_types=1);

namespace Tests\Appplication;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use PHPUnit\Framework\TestCase;
use Tests\AppTestTrait;
use Envms\FluentPDO\Literal;
use Envms\FluentPDO\Query;

use Pholar\UserMgr;

class SharesTest extends TestCase
{
	use AppTestTrait;

	protected $app;

	protected function setUp(): void
	{
		$this->logout();
		$this->app = $this->getAppInstance();
		$this->setupDbTables($this->app);

		$this->app->getContainer()->get(Query::class)
			->insertInto('photo', [
				['id' => 1, 'recordmtime' => '2020-06-29 15:19:24', 'filename' => 'test1.jpg', 'location' => '/', 'format' => 'image/jpeg'],
				['id' => 2, 'recordmtime' => '2020-06-29 15:30:24', 'filename' => 'test.svg', 'location' => '/images/svg', 'format' => 'image/svg+xml'],
				['id' => 3, 'recordmtime' => '2020-06-29 15:40:24', 'filename' => 'Lorem Ipsum.doc', 'location' => '/documents', 'format' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document']
			])
			->execute();
		$this->app->getContainer()->get(Query::class)
			->insertInto('collection', [
				['id' => 1, 'name' => 'Test', 'comment' => 'A test collection'],
				['id' => 2, 'name' => 'Another test', 'comment' => 'A collection to test'],
			])
			->execute();
		$this->app->getContainer()->get(Query::class)
			->insertInto('photo_collection', [
				['photo_id' => 1, 'collection_id' => 1],
				['photo_id' => 2, 'collection_id' => 1]
			])
			->execute();

		$now = new \DateTimeImmutable('now');
		$tomorrow = $now->add(new \DateInterval('P1D'))->format('Y-m-d');
		$yesterday = $now->sub(new \DateInterval('P1D'))->format('Y-m-d');

		$pass = password_hash('password', PASSWORD_DEFAULT);
		$null = new Literal("NULL");

		$this->app->getContainer()->get(Query::class)
			->insertInto('share', [
				['shareuid' => '123', 'collection_id' => 1, 'user' => 'admin', 'password' => $null, 'can_download' => 1, 'can_details' => 1, 'valid_from' => $null, 'valid_until' => $null],
				['shareuid' => '234', 'collection_id' => 1, 'user' => 'admin', 'password' => $pass, 'can_download' => 1, 'can_details' => 1, 'valid_from' => $null, 'valid_until' => $null],
				['shareuid' => '345', 'collection_id' => 1, 'user' => 'admin', 'password' => $null, 'can_download' => 1, 'can_details' => 1, 'valid_from' => $tomorrow, 'valid_until' => $null],
				['shareuid' => '456', 'collection_id' => 1, 'user' => 'admin', 'password' => $null, 'can_download' => 1, 'can_details' => 1, 'valid_from' => $null, 'valid_until' => $yesterday],
				['shareuid' => '567', 'collection_id' => 1, 'user' => 'admin', 'password' => $null, 'can_download' => 0, 'can_details' => 0, 'valid_from' => $null, 'valid_until' => $null],
			])
			->execute();
	}

	protected function tearDown(): void
	{
		$this->app = null;
	}

	public function testPublicShare()
	{
		$request = $this->createRequest('GET', '/share/123');
		$response = $this->app->handle($request);
		$body = $response->getBody()->__toString();
		$this->assertEquals(200, $response->getStatusCode(), $body);
		$this->assertStringContainsString('2 photos', $body);
		$this->assertEquals('guest', $_SESSION[UserMgr::SESSION_USER_NAME], 'Logged in as guest');
	}

	public function testPasswordProtectedShareRedirectToLogin()
	{
		$request = $this->createRequest('GET', '/share/234');
		$response = $this->app->handle($request);
		$this->assertEquals("", $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());
		$this->assertEquals(["/share/234/login"], $response->getHeader('Location'));
	}

	public function testPasswordProtectedShareLogin()
	{
		$data = ['password' => 'password'];
		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', '/share/234/login', [], [], [], $body);
		$response = $this->app->handle($request);
		$this->assertEquals("", $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());
		$this->assertEquals(["/share/234"], $response->getHeader('Location'));
		$this->assertTrue($_SESSION['234'], 'Logged in share 234');
	}

	public function testPasswordProtectedShareInvalidLogin()
	{
		$data = ['password' => 'invalid'];
		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', '/share/234/login', [], [], [], $body);
		$response = $this->app->handle($request);
		$this->assertEquals("", $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());
		$this->assertEquals(["/share/234/login"], $response->getHeader('Location'));
		$this->assertFalse(isset($_SESSION['234']), 'Not logged in share 234');
	}

	public function testPasswordProtectedShare()
	{
		$_SESSION['234'] = true;
		$request = $this->createRequest('GET', '/share/234');
		$response = $this->app->handle($request);
		$body = $response->getBody()->__toString();
		$this->assertEquals(200, $response->getStatusCode(), $body);
		$this->assertStringContainsString('2 photos', $body);
	}

	public function testLogoutCurrentUserAndLoginGuest()
	{
		$this->loginAdmin();
		$request = $this->createRequest('GET', '/share/123');
		$response = $this->app->handle($request);
		$body = $response->getBody()->__toString();
		$this->assertEquals(200, $response->getStatusCode(), $body);
		$this->assertStringContainsString('2 photos', $body);
		$this->assertEquals('guest', $_SESSION[UserMgr::SESSION_USER_NAME], 'Logged in as guest');
		$this->assertEquals('admin', $_SESSION[UserMgr::SESSION_PREV_NAME], 'Previously logged in as admin');
	}

	public function testSharedPhotoDetails()
	{
		$request = $this->createRequest('GET', '/share/123/photo/1');
		$response = $this->app->handle($request);
		$body = $response->getBody()->__toString();
		$this->assertEquals(200, $response->getStatusCode(), $body);
	}

	public function testInvalidSharedPhotoDetails()
	{
		$request = $this->createRequest('GET', '/share/123/photo/3');
		$response = $this->app->handle($request);
		$body = $response->getBody()->__toString();
		$this->assertEquals(404, $response->getStatusCode(), $body);
	}

	public function testFutureShare()
	{
		$request = $this->createRequest('GET', '/share/345');
		$response = $this->app->handle($request);
		$body = $response->getBody()->__toString();
		$this->assertEquals(404, $response->getStatusCode(), $body);
	}

	public function testExpiredShare()
	{
		$request = $this->createRequest('GET', '/share/456');
		$response = $this->app->handle($request);
		$body = $response->getBody()->__toString();
		$this->assertEquals(404, $response->getStatusCode(), $body);
	}

	public function testCannotViewDetails()
	{
		$request = $this->createRequest('GET', '/share/567/photo/1');
		$response = $this->app->handle($request);
		$body = $response->getBody()->__toString();
		$this->assertEquals(403, $response->getStatusCode(), $body);
	}

	public function testCannotDownload()
	{
		$request = $this->createRequest('GET', '/share/567/source/1');
		$response = $this->app->handle($request);
		$this->assertEquals(403, $response->getStatusCode());
	}


	/**
	 * create a share without validity limits, no password and full permissions
	 */
	public function testCreateShareSimple()
	{
		$cid = 2;
		$data = [
			'redirect' => '/collection/' . $cid,
			'password' => '',
			'valid_from' => '',
			'valid_until' => '',
			'can_download' => 'on',
			'can_details' => 'on',
		];

		$this->loginAdmin();

		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', "/collection/$cid/share", [], [], [], $body);
		$response = $this->app->handle($request);

		$this->assertEquals("", $response->getBody()->__toString());	
		$this->assertEquals(302, $response->getStatusCode());
	
		$db = $this->app->getContainer()->get(Query::class);
		$share = $db->from('share')->where('collection_id', $cid)->fetch();
		$this->assertNotFalse($share, "Share has been created");

		$this->assertEquals(
			[ "/collection/$cid#share-{$share['shareuid']}" ],
			$response->getHeader('Location'),
			"Redirects to share dialog"
		);

		$this->assertEquals(null, $share['password']);
		$this->assertEquals(null, $share['valid_from']);
		$this->assertEquals(null, $share['valid_until']);
		$this->assertEquals(true, $share['can_download']);
		$this->assertEquals(true, $share['can_details']);
	}

	/**
	 * create a share with validity limits, password and no permissions
	 */
	public function testCreateShareWithLimits()
	{
		$cid = 2;
		$data = [
			'redirect' => '/collection/' . $cid,
			'password' => 'password',
			'valid_from' => '2020-01-01',
			'valid_until' => '2020-12-31',
		];

		$this->loginAdmin();

		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', "/collection/$cid/share", [], [], [], $body);
		$response = $this->app->handle($request);

		$this->assertEquals("", $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());

		$db = $this->app->getContainer()->get(Query::class);
		$share = $db->from('share')->where('collection_id', $cid)->fetch();

		$this->assertNotFalse($share, "Share has been created");
		$this->assertEquals(
			[ "/collection/$cid#share-{$share['shareuid']}" ],
			$response->getHeader('Location'),
			"Redirects to share dialog"
		);

		$this->assertTrue(password_verify('password', $share['password']));
		$this->assertEquals('2020-01-01', $share['valid_from']);
		$this->assertEquals('2020-12-31' , $share['valid_until']);
		$this->assertEquals(false, $share['can_download'] === '1');
		$this->assertEquals(false, $share['can_details'] === '1');
	}

	public function testCreateShareOfInvalidCollection()
	{
		$cid = 3;
		$data = [
			'redirect' => '/collection/' . $cid,
			'password' => '',
			'valid_from' => '',
			'valid_until' => '',
			'can_download' => 'on',
			'can_details' => 'on',
		];

		$this->loginAdmin();
	
		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', "/collection/$cid/share", [], [], [], $body);
		$response = $this->app->handle($request);

		$this->assertEquals(404, $response->getStatusCode());
	}

	public function testCreateShareWithoutSharePermissions()
	{
		$cid = 3;
		$data = [
			'redirect' => '/collection/' . $cid,
			'password' => '',
			'valid_from' => '',
			'valid_until' => '',
			'can_download' => 'on',
			'can_details' => 'on',
		];

		$this->login('noshare');

		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', "/collection/$cid/share", [], [], [], $body);
		$response = $this->app->handle($request);

		$this->assertEquals(403, $response->getStatusCode());
	}

	public function testCreateShareAsAnonymousUser()
	{
		$cid = 3;
		$data = [
			'redirect' => '/collection/' . $cid,
			'password' => '',
			'valid_from' => '',
			'valid_until' => '',
			'can_download' => 'on',
			'can_details' => 'on',
		];

		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', "/collection/$cid/share", [], [], [], $body);
		$response = $this->app->handle($request);

		$this->assertEquals(403, $response->getStatusCode());
	}

	public function testEditShare()
	{
		$cid = 1;
		$suid = "123";
		$data = [
			'redirect' => '/test',
			'valid_from' => '2020-01-01',
			'valid_until' => '2020-12-31',

			'action' => 'edit',
		];

		$this->loginAdmin();

		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', "/collection/$cid/share/$suid", [], [], [], $body);
		$response = $this->app->handle($request);

		$this->assertEquals("", $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());

		$db = $this->app->getContainer()->get(Query::class);
		$share = $db->from('share')->where('shareuid', $suid)->fetch();

		$this->assertNotFalse($share, "Share exist (doh)");
		$this->assertEquals(['/test'], $response->getHeader('Location'));

		$this->assertEquals(null, $share['password']);
		$this->assertEquals('2020-01-01', $share['valid_from']);
		$this->assertEquals('2020-12-31' , $share['valid_until']);
		$this->assertEquals(false, $share['can_download'] === '1');
		$this->assertEquals(false, $share['can_details'] === '1');
	}

	public function testEditShareNotChangePassword()
	{
		$cid = 1;
		$suid = "234";
		$data = [
			'redirect' => '/test',
			'valid_from' => '2020-01-01',
			'valid_until' => '2020-12-31',

			'action' => 'edit',
		];

		$this->loginAdmin();

		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', "/collection/$cid/share/$suid", [], [], [], $body);
		$response = $this->app->handle($request);

		$this->assertEquals("", $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());

		$db = $this->app->getContainer()->get(Query::class);
		$share = $db->from('share')->where('shareuid', $suid)->fetch();

		$this->assertNotFalse($share, "Share exist (doh)");
		$this->assertEquals(['/test'], $response->getHeader('Location'));

		$this->assertTrue(password_verify('password', $share['password']));
		$this->assertEquals('2020-01-01', $share['valid_from']);
		$this->assertEquals('2020-12-31' , $share['valid_until']);
		$this->assertEquals(false, $share['can_download'] === '1');
		$this->assertEquals(false, $share['can_details'] === '1');
	}

	public function testEditShareWithoutPermission()
	{
		$cid = 1;
		$suid = "123";
		$data = [
			'redirect' => '/test',
			'valid_from' => '2020-01-01',
			'valid_until' => '2020-12-31',

			'action' => 'edit',
		];

		$this->login('noshare');

		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', "/collection/$cid/share/$suid", [], [], [], $body);
		$response = $this->app->handle($request);

		$this->assertEquals(403, $response->getStatusCode());
	}

	public function testEditShareAsAnonymousUser()
	{
		$cid = 1;
		$suid = "123";
		$data = [
			'redirect' => '/test',
			'valid_from' => '2020-01-01',
			'valid_until' => '2020-12-31',

			'action' => 'edit',
		];

		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', "/collection/$cid/share/$suid", [], [], [], $body);
		$response = $this->app->handle($request);

		$this->assertEquals(403, $response->getStatusCode());
	}

	public function testEditInvalidShare()
	{
		// setup flash mock
		$flash= $this->createMock(\Slim\Flash\Messages::class);
		$flash->expects($this->once())
				 ->method('addMessage')
				 ->with($this->equalTo('error'), $this->equalTo("Invalid share"));
		$this->app->getContainer()->set(\Slim\Flash\Messages::class, $flash);

		$cid = 1;
		$suid = "000";
		$data = [
			'redirect' => '/test',
			'valid_from' => '2020-01-01',
			'valid_until' => '2020-12-31',

			'action' => 'edit',
		];

		$this->loginAdmin();

		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', "/collection/$cid/share/$suid", [], [], [], $body);
		$response = $this->app->handle($request);

		$this->assertEquals("", $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());
		$this->assertEquals(['/test'], $response->getHeader('Location'));
	}

	public function testDeleteShare()
	{
		$cid = 1;
		$suid = "123";
		$data = [
			'redirect' => '/test',
			'action' => 'remove',
		];

		$this->loginAdmin();

		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', "/collection/$cid/share/$suid", [], [], [], $body);
		$response = $this->app->handle($request);

		$this->assertEquals("", $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());
		$this->assertEquals(['/test'], $response->getHeader('Location'));

		$db = $this->app->getContainer()->get(Query::class);
		$share = $db->from('share')->where('shareuid', $suid)->fetch();

		$this->assertFalse($share, "Select share return false");
	}

	/**
	 * No errors are shown if user try to delete an invalid share
	 * because.. it already doesn't exists, so tecnically it worked!
	 */
	public function testDeleteInvalidShare()
	{
		$cid = 1;
		$suid = "000";
		$data = [
			'redirect' => '/test',
			'action' => 'remove',
		];

		$this->loginAdmin();

		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', "/collection/$cid/share/$suid", [], [], [], $body);
		$response = $this->app->handle($request);

		$this->assertEquals("", $response->getBody()->__toString());
		$this->assertEquals(302, $response->getStatusCode());
		$this->assertEquals(['/test'], $response->getHeader('Location'));
	}

	public function testDeleteShareWithoutPermissions()
	{
		$cid = 1;
		$suid = "123";
		$data = [
			'redirect' => '/test',
			'action' => 'remove',
		];

		$this->login('noshare');

		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', "/collection/$cid/share/$suid", [], [], [], $body);
		$response = $this->app->handle($request);

		$this->assertEquals(403, $response->getStatusCode());

		$share = $this->app->getContainer()->get(Query::class)
						   ->from('share')->where('shareuid', $suid)->fetch();
		$this->assertNotFalse($share, "Share still exists");
	}

	public function testDeleteShareAsAnonymousUser()
	{
		$cid = 1;
		$suid = "123";
		$data = [
			'redirect' => '/test',
			'action' => 'remove',
		];

		$body = $this->formEncoded($data);
		$request = $this->createRequest('POST', "/collection/$cid/share/$suid", [], [], [], $body);
		$response = $this->app->handle($request);

		$this->assertEquals(403, $response->getStatusCode());

		$share = $this->app->getContainer()->get(Query::class)
						   ->from('share')->where('shareuid', $suid)->fetch();
		$this->assertNotFalse($share, "Share still exists");
	}

}
