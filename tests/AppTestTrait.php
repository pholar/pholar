<?php
declare(strict_types=1);

namespace Tests;

use DI\ContainerBuilder;
use Exception;
use PHPUnit\Framework\TestCase as PHPUnit_TestCase;
use Psr\Http\Message\ServerRequestInterface as Request;

use Slim\App;
use Slim\Factory\AppFactory;
use Slim\Interfaces\RouteParserInterface;
use Slim\Psr7\Factory\StreamFactory;
use Slim\Psr7\Headers;
use Slim\Psr7\Request as SlimRequest;
use Slim\Psr7\Uri;

use Phinx\Config\Config;
use Phinx\Migration\Manager;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\NullOutput;

use Pholar\HttpErrorHandler;
use Pholar\UserMgr;
use Pholar\SystemSettings;
use Pholar\Settings;

trait AppTestTrait
{
	protected function logout()
	{
		unset($_SESSION[UserMgr::SESSION_USER_NAME]);
	}

	protected function loginAdmin()
	{
		$_SESSION[UserMgr::SESSION_USER_NAME] = 'admin';
	}

	protected function login(string $user)
	{
		$_SESSION[UserMgr::SESSION_USER_NAME] = $user;
	}


	/**
	 * @return App
	 * @throws Exception
	 */
	protected function getAppInstance(): App
	{
		$_ENV['PHOLAR_ENV'] =  $_ENV['PHOLAR_ENV'] ?? getenv('PHOLAR_ENV') ?: "sqlite";
	
		// Instantiate PHP-DI ContainerBuilder
		$containerBuilder = new ContainerBuilder();


		// Set up dependencies
		$dependencies = require __DIR__ . '/../app/dependencies.php';
		$dependencies($containerBuilder);

		// Build PHP-DI Container instance
		$container = $containerBuilder->build();

		// replace settings with test settings
		$systemsettings = new TestSystemSettings();
		$container->set(SystemSettings::class, $systemsettings);

		// replace pdo from container for tests
		/*$pdo = new \Tests\Lib\PDOExt\PDO("sqlite::memory:");
		$pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		$pdo->exec('PRAGMA foreign_keys = ON;');
		$container->set('pdo', $pdo);*/

		// Mock logger
		$logger = $this->createMock(\Monolog\Logger::class);
		$container->set('logger', $logger);

		// Mock flash messages
		$flash= $this->createMock(\Slim\Flash\Messages::class);
		$container->set('flash', $flash);

		// Instantiate the app
		AppFactory::setContainer($container);
		$app = AppFactory::create();

		// replace RouteParserInterface in container
		$container->set(
			RouteParserInterface::class,
			function () use ($app) {
				return $app->getRouteCollector()->getRouteParser();
			}
		);

		$settings = $container->get(TestSettings::class);
		$container->set(Settings::class, $settings);

		// Register middleware
		$middleware = require __DIR__ . '/../app/middleware.php';
		$middleware($app, $settings);

		// Register routes
		$routes = require __DIR__ . '/../app/routes.php';
		$routes($app);


		// add error middleware without set default handler
		// (I can't create error handler in container... why? I don't rembember...)
		$displayErrorDetails = true;
		$errorMiddleware = $app->addErrorMiddleware($displayErrorDetails, false, false);
		
		TestApp::setApp($app);
		return $app;
	}

	protected function delTree(string $dir) {
		$files = array_diff(scandir($dir), array('.','..'));
		foreach ($files as $file) {
			(is_dir("$dir/$file")) ? $this->delTree("$dir/$file") : unlink("$dir/$file");
		}
		return rmdir($dir);
	}


	/**
	 * @param App $app
	 */
	protected function setupDbTables(App $app)
	{
		$this->dropDbTables($app);
		$settings = $app->getContainer()->get(Settings::class);
		$env = $settings->getEnv();

		$config = $app->getContainer()->get(Config::class);
        $manager = new Manager($config,  new StringInput(' '), new NullOutput());
        $manager->migrate($env);

		$pdo = $app->getContainer()->get(\PDO::class);
		$pdo->exec(
			"INSERT INTO user (user, password, can_login, can_edit, can_delete, can_share, can_download, is_admin) VALUES"
			."	   ('admin',    '', 1, 1, 1, 1, 1, 1),"
			."	   ('guest',    '', 0, 0 ,0, 0, 1, 0),"
			."     ('noedit',   '', 1, 0, 1, 1, 1, 0),"
			."     ('nodelete', '', 1, 1, 0, 1, 1, 0),"
			."     ('noshare',  '', 1, 1, 1, 0, 1, 0),"
			."     ('nodwl',    '', 1, 1, 1, 1, 0, 0);"
		);
	}

	/**
	 * @param App $app
	 */
	protected function dropDbTables(App $app)
	{
		$driver = $app->getContainer()->get(Settings::class)->get('dsn', 'scheme');
		$pdo = $app->getContainer()->get(\PDO::class);
		switch($driver) {
			case "mysql":
				$pdo->exec("SET FOREIGN_KEY_CHECKS = 0;");
				$r = $pdo->query("SELECT table_name FROM information_schema.tables WHERE table_schema = 'pholar' and table_type = 'VIEW';")->fetchAll(\PDO::FETCH_COLUMN);
				if (count($r)) {
					$views = implode(",", $r);
					$pdo->exec("DROP VIEW $views;");
				}
				
				$r = $pdo->query("SELECT table_name FROM information_schema.tables WHERE table_schema = 'pholar' and table_type != 'VIEW';")->fetchAll(\PDO::FETCH_COLUMN);
				if (count($r)) {
					$tbls = implode(",", $r);
					$pdo->exec("DROP TABLE $tbls;");
				}
				$pdo->exec("SET FOREIGN_KEY_CHECKS = 1;");
				break;
		}
	}

	/**
	 * @param array $data
	 * @return string
	 */
	protected function formEncoded(array $data = []) : string
	{
		return http_build_query($data);
	}



	/**
	 * Send a request.
	 *
	 * Content-Type header defaults to 'application/x-www-form-urlencoded'
	 * if not set with method POST
	 *
	 * @param string $method
	 * @param string $path
	 * @param array  $headers
	 * @param array  $cookies
	 * @param array  $serverParams
	 * @param string $body
	 * @return Request
	 */
	protected function createRequest(
		string $method,
		string $path,
		array $headers = [],
		array $cookies = [],
		array $serverParams = [],
		string $body = ""
	): Request {
		$query = '';
		if (strpos($path, "?") > 0) {
			list($path, $query) = explode("?", $path);
		}
		$uri = new Uri('', '', 80, $path, $query);
		$handle = fopen('php://temp', 'w+');
		fwrite($handle, $body);
		$stream = (new StreamFactory())->createStreamFromResource($handle);

		if ($method == "POST" && is_null($headers['Content-Type'] ?? null)) {
			$headers['Content-Type'] = 'application/x-www-form-urlencoded';
		}

		$h = new Headers();
		foreach ($headers as $name => $value) {
			$h->addHeader($name, $value);
		}


		return new SlimRequest($method, $uri, $h, $cookies, $serverParams, $stream);
	}
}
