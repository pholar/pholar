#!/bin/bash
set -e

if [ -z "$1" ] || [ ! -f "$1" ]
then
	echo "prepare sqlite db to be used with phinx migrations"
	echo "usage: to_phinx.sh <sqlite3 file>"
	exit
fi

sqlite3 "$1" <<< '
PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `phinxlog` (`version` BIGINTEGER NOT NULL, `migration_name` VARCHAR(100) NULL, `start_time` TIMESTAMP_TEXT NULL, `end_time` TIMESTAMP_TEXT NULL, `breakpoint` BOOLEAN_INTEGER NOT NULL DEFAULT 0, PRIMARY KEY (`version`));
INSERT OR IGNORE INTO phinxlog VALUES(20210924103038,'"'Initial','2021-11-26 21:03:09','2021-11-26 21:03:09'"',0);
DROP TABLE IF EXISTS `meta`;
COMMIT;
'