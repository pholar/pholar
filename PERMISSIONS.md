# PhotoMgr permissions


## User permissions

### can_login

If `FALSE` user can't login with user and password.

Can be used to disable an user without deleting it or to create a custom user
for autologin functon.

i.e. default user `guest`, used for shared, has `can_login` to `FALSE`


### can_edit

If `TRUE` user can:

- edit photo metadata
- update metadata from photo
- add/edit tags
- create/edit/delete collections #TODO: to review


### can_delete

If `TRUE` user can:

- delete photos

photos are never deleted from storage, but marked as 'hidden'.
metadata rows are deleted from database.


### can_share

If `TRUE` user can:

- add shares of collections
- edit/modify his shares

`is_admin` can view shares by everyone. nedd `can_share` also


### can_download

If `TRUE` user can:

- download original photo
- view PDF as embed instead of preview
- play video in webpage


### can_upload

if `TRUE` user can:

- upload new photos using web interface


### is_admin

If `TRUE` user is administrator and can

- see admin pages

**NOTE**: admin permission doesn't imply any other permission


## Share permissions

### can_download

if not null, override guest user `can_download` permission


### can_details

if `TRUE`, guest user can see photo details
