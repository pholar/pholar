// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL-3.0-or-later
(() => {
	window.application.register("dirsel", class extends Stimulus.Controller {
        static get targets() {
            return [ "label", "radio" ]
        }

        connect() {
            this.update();
        }

        update() {
            var c = this.radioTargets.filter((e) => e.checked)[0];
            var l = c.dataset.btnLabel;
            this.labelTarget.innerText = l;
        }

        select() {
            this.update();
        }
    })
})()
// @license-end
