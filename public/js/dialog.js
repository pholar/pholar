// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL-3.0-or-later
(() => {
	/** 
	 * connects click event on every link with href which starts with "#"
	 * if clicked link has href == "#", then is a close button, we search for every
	 * elements ".dialog.open" and ".closeable.open" and remove "open" class.
	 * Otherwhise, we search for the element with the id equal to the href value and
	 * we add "open" class to it.
	 * 
	 * This sort of duplicate css ":target" behaviour without changing url fragment.
	 * And is mostly plug-and-play
	*/ 

	document.addEventListener('DOMContentLoaded', () => {
		// catch all links opening a dialog
		document.querySelectorAll('a[href^="#"]').forEach((n) => {
			n.addEventListener("click", (evt) => {
				var selector = n.href.replace(/^.*#/, "#");
				// close all opened dialogs

				document.querySelectorAll('.dialog.open, .closeable.open').forEach(elm => {
					console.log("Closing", elm);
					elm.classList.remove("open");
				});

				if (selector != "#") {
					// this is an open button
					var elm = document.querySelector(selector);
					if (elm) {
						console.log("Opening", elm);
						evt.preventDefault();
						elm.classList.add("open");
						return false;
					}
				}
			});
		});

		// close on dialog backdrop click
		document.querySelectorAll('.dialog').forEach((n) => {
			n.addEventListener("click", (evt) => {
				if (evt.target == n) {
					console.log("Closing", n);
					n.classList.remove("open");
				}
			});
		});
	});
})()
