// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL-3.0-or-later
(() => {
	window.application.register("clipboard", class extends Stimulus.Controller {
		static get targets() {
			return [ "text", "btn" ];
		}

		connect() {
			this.btnTarget.classList.remove("hidden");
		}

		doneOk() {
			var img = this.btnTarget.querySelector("img")
			var imgsrc = "/img/edit-copy-symbolic.svg";
			img.src = "/img/test-pass-symbolic.svg";
			setTimeout(() => img.src = imgsrc, 2000 );
		}

		fallbackCopyTextToClipboard(text) {
			var textArea = document.createElement("textarea");
			textArea.value = text;

			// Avoid scrolling to bottom
			textArea.style.top = "0";
			textArea.style.left = "0";
			textArea.style.position = "fixed";

			document.body.appendChild(textArea);
			textArea.focus();
			textArea.select();

			try {
				var successful = document.execCommand('copy');
				var msg = successful ? 'successful' : 'unsuccessful';
				console.log('Fallback: Copying text command was ' + msg);
				this.doneOk();
			} catch (err) {
				console.error('Fallback: Oops, unable to copy', err);
			}

			document.body.removeChild(textArea);
		}

		copyTextToClipboard(text) {
			if (navigator.clipboard) {
				this.fallbackCopyTextToClipboard(text);
				return;
			}
			navigator.clipboard.writeText(text).then(function() {
				console.log('Async: Copying to clipboard was successful!');
				this.doneOk();
			}.bind(this), function(err) {
				console.error('Async: Could not copy text: ', err);
			});
		}

		copy(e) {
			e.preventDefault();
			var text = this.textTarget.dataset.text;
			this.copyTextToClipboard(text);
			return false;
		}
	})
})()
// @license-end
