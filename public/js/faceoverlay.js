// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL-3.0-or-later
(() => {
    window.application.register("faceoverlay", class extends Stimulus.Controller {
        static get targets() {
            return ["container", "face"]
        }

        connect() {

        }

        onenter(evt) {
            // porco di quel porco il porco
            // troviamo l'elemento 'face'
            var face = evt.target;
            while (face != null) {
                if (this.faceTargets.includes(face)) {
                    break;
                }
                if (face == this.containerTarget) {
                    return;
                }
                face = face.parentElement
            }
            this.faceTargets.forEach(e => e.classList.remove("show"));
            face.classList.add("show");
            this.containerTarget.append(face);
        }
        onleave(evt) {
            this.faceTargets.forEach(e => e.classList.remove("show"));
        }
    });
})()
// @license-end