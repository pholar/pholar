// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL-3.0-or-later
/* hierarchical highlighting in filed values */
(() => {
	window.application.register("hihi", class extends Stimulus.Controller {
		/*static get targets() {
			return [ "frag" ]
		}*/

		getElm(elm) {
			while (elm !== null && elm.nodeName !== "SPAN") elm  = elm.parentNode;
			return elm;
		}

		showFrags(e) {
			e.stopPropagation();
			var elm = this.getElm(e.target);
			elm ? elm.classList.add('hover') : null;
		}
		hideFrags(e) {
			var elm = this.getElm(e.target);
			elm ? elm.classList.remove('hover') : null;
		}
	})
})()
// @license-end
