// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL-3.0-or-later
(() => {
	window.application.register("widgets", class extends Stimulus.Controller {
        static get targets() {
			return [ "widget" ]
		}

        getName(elm) {
            return Array.from(elm.classList).filter(n => n != "widget")[0];
        }

		connect() {
            this.widgetTargets.forEach(elm => {
                const name = this.getName(elm);
                const isopen = localStorage.getItem(`widgets.${name}.isopen`);
                console.log(elm, `widgets.${name}.isopen`, '->', isopen);
                if (isopen == "false") elm.removeAttribute("open");
                if (isopen == "true") elm.open = "open";
            });
        }

        toggle(evt) {
            var elm = evt.target;
            var name = this.getName(elm);
            console.log(`widgets.${name}.isopen`, '<-', elm.open);
            localStorage.setItem(`widgets.${name}.isopen`, String(elm.open));
        }
    });
})()
// @license-end