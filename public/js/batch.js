// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL-3.0-or-later
(() => {
	window.application.register("batch", class extends Stimulus.Controller {
		static get targets() {
			return [ "tools", "check", "toolselectall", "toolunselectall" ]
		}

		connect() {
			this.check();
		}

		update(has_selection) {
			var is_full_selected = this.checkTargets.reduce((v, e) => { return e.checked && v; }, true);
			if (is_full_selected) {
				this.toolunselectallTarget.classList.remove("hidden");
				this.toolselectallTarget.classList.add("hidden");
			} else {
				this.toolunselectallTarget.classList.add("hidden");
				this.toolselectallTarget.classList.remove("hidden");
			}

			if (has_selection) {
				this.toolsTarget.classList.remove("hidden");
			} else {
				this.toolsTarget.classList.add("hidden");
			}
		}

		selectAll(e) {
			e.preventDefault();
			var status = true;
			this.checkTargets.map( e => e.checked = true );
			this.update(true);
		}

		deselectAll(e) {
			e.preventDefault();
			this.checkTargets.map( e => e.checked = false );
			this.update(false);
		}

		check(e) {
			console.log(e);
			var status = this.checkTargets.reduce((v, e) => { return e.checked || v; }, false);
			this.update(status);
		}
	})
})()
// @license-end
