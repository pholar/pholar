// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL-3.0-or-later
(() => {
	window.application.register("upload", class extends Stimulus.Controller {
        files = [];

        static get targets() {
			return [ "form", "fileinput", "submit", "addfile", "list", "placeholder", "figuretemplate" ]
		}

		connect() {
			this.fileinputTarget.classList.add("hidden");
            this.submitTarget.disabled = true;
            this.addfileTarget.classList.remove("hidden");
            this.placeholderTarget.classList.remove("hidden");
            window.URL = window.URL || window.webkitURL;
		}

        addFigure(id, data) {
            var name = data.name;
            console.log("addFigure", id, data, name);

            var url="/img/application-x-generic.png";

            if (data.type.match(/^video\//)) url="/img/video-x-generic.png";
            if (data.type.match(/^audio\//)) url="/img/audio-x-generic.png";
            if (data.type.match(/^image\//)) url="/img/image-x-generic.png";
            if (data.type.match(/document/)) url="/img/x-office-document.png";
            if (["image/png", "image/jpeg", "image/gif", "image/webp", "image/svg+xml"].indexOf(data.type) != -1) url=window.URL.createObjectURL(data);

            var fragment = this.figuretemplateTarget.content.cloneNode(true);
            fragment.querySelector("figure").id = "fig-" + id;
            fragment.querySelector("label").title = name;
            fragment.querySelector("label > img").src = url;
            fragment.querySelector("figcaption > h3").innerText = name;
            fragment.querySelector("label > a").dataset['id'] = id;
            // 
            fragment.querySelector("label > a").addEventListener("click", this.removefile.bind(this));
            this.listTarget.appendChild(fragment);
            
        }

        updateDisplay() {
            var hasfiles = this.files.filter(f => f != null).length > 0;
            var hasfigures = this.listTarget.querySelectorAll("figure").length > 0; 
            this.submitTarget.disabled = !hasfiles;

            if (hasfigures) {
                this.placeholderTarget.classList.add("hidden");
            } else {
                this.placeholderTarget.classList.remove("hidden");
            }
        }

        addFile(file) {
            console.log("addFile", file);
            var id = this.files.length;
            this.files.push(file);
            this.addFigure(id, file);
            this.updateDisplay();
        }

        fileInputChanged(evt) {
            var files = this.fileinputTarget.files;
            for(var k=0; k<files.length; k++) {
                this.addFile(files.item(k));
            }
        }

        selectfile(evt) {
            evt.preventDefault();
            this.fileinputTarget.click();
        }

        removefile(evt) {
            evt.preventDefault();
            var t = evt.target;
            while(t.nodeName != "A") t = t.parentElement;
            var id = t.dataset['id'];
            // remove figure
            this.listTarget.querySelector("#fig-" + id).remove();
            // remove file
            this.files[id] = null;

            this.updateDisplay();
        }



        async submit(evt) {
            evt.preventDefault();

            this.submitTarget.disabled = true;
            this.listTarget.querySelectorAll("label > a").forEach(elm => elm.classList.add("hidden"));

            var upload = function(id, file) {
                return new Promise(function (resolve, reject) {     
                    console.log("upload", id);
                    var progress = this.listTarget.querySelector("#fig-"+id+" > figcaption > progress");
                    progress.classList.remove("hidden");

                    var xhr = new XMLHttpRequest();
                    xhr.open("post", this.formTarget.action);
                    xhr.setRequestHeader('Accept', 'application/json');
                    xhr.responseType = "json";
                    xhr.onprogress = function (e) {
                        if (e.lengthComputable) {
                            console.log("progress" , e.loaded ,  "/",  e.total)
                        }
                    }
                    xhr.onload = (e) => {
                        if (xhr.response == undefined) reject("Invalid response from server");
                        console.log(xhr.response.message);
                        if (xhr.status == 200) {
                            resolve(xhr.response.message);
                        } else {
                            reject(xhr.response.message);
                        }
                    }
                    xhr.upload.addEventListener("progress", function(evt){
                        if (evt.lengthComputable) {
                            progress.value = evt.loaded / evt.total;
                            if (progress.value > 0.9) progress.removeAttribute('value');
                        }
                    }, false);

                    // post form via xhr, replacing file value
                    var data = new FormData(this.formTarget);
                    data.set("file[]", file);
                    xhr.send(data);
                }.bind(this));
            }.bind(this);

            var done = function(id, status, message) {
                this.listTarget.querySelector("#fig-" + id).title = message;
                this.listTarget.querySelector("#fig-" + id).classList.add(status);
                this.listTarget.querySelector("#fig-" + id + " > figcaption > progress").remove();
                this.listTarget.querySelector("#fig-" + id + " label > a").classList.remove("hidden");
                this.files[id] = null;
            }.bind(this);
            
            var success = true;
            var errormessages = [];
            for(var id=0; id<this.files.length; id++) {
                var file = this.files[id];
                if (file!= null) {
                    try {
                        await upload(id, file);
                        done(id, "success");
                    } catch(e) {
                        success = false;
                        errormessages.push(String(e));
                        done(id, "error", e);
                    };
                }
            }

            if (success) {
                var redirect = this.formTarget.querySelector("#redirect").value;
                console.log("redirect to", redirect);
                window.location.href = redirect;
            } else {
                var error = document.createElement("div");
                error.className = "message error";
                error.innerText = errormessages.join("\n");
                document.body.appendChild(error);
            }
        }

        startdrag(evt) {
            if (evt.dataTransfer.types.includes("Files")) {
                evt.preventDefault();
                evt.stopPropagation();
                this.listTarget.classList.add("droptarget");
            }
        }

        exitdrag(evt) {
            evt.preventDefault();
            evt.stopPropagation();
            this.listTarget.classList.remove("droptarget");
        }

        dodrop(evt) {
            evt.preventDefault();
            evt.stopPropagation();
            this.listTarget.classList.remove("droptarget");
            var files = evt.dataTransfer.files;
            for(var k=0; k<files.length; k++) {
                this.addFile(files.item(k));
            }
        }
	})
})()
// @license-end
