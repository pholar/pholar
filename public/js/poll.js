// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL-3.0-or-later
// poll notifications and status
(function(){
  const poll = async () => {
    const r = await fetch("/system/poll");
    const data = await r.json();

    const tasks = data['tasks'];
    const e =  document.querySelector("#jobspopup")
    if (tasks.length > 0) {
      e.innerText = `${tasks.length} pending jobs ...`
      e.classList.add("show")
    } else {
      e.classList.remove("show");
    }
    
  };

  poll();
  setInterval(poll, 5000);
  
})();
