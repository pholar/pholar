FROM debian:latest

ENV PHPVER=7.4

WORKDIR /app

RUN apt-get update \
	&& apt-get install -y --no-install-recommends \
		ca-certificates \
		wget \
		perl \
		sqlite3 \
		imagemagick \
		file \
		ffmpeg \
		ghostscript \
		libreoffice \
		php \
		php-fpm \
		php-gd \
		php-imagick \
		php-pdo \
		php-pdo-sqlite \
		php-xml \
		php-curl \
		php-mbstring \
		php-zip \
		php-json

COPY ./ /app/

RUN cp /app/docker/www.conf /etc/php/${PHPVER}/fpm/pool.d/www.conf \
	&& mkdir assets \
	&& cp .env.dist var/env \
	&& sed -i.bkp 's|STORAGEDIR=".*|STORAGEDIR="/app/assets"|' var/env \
	&& rm var/env.bkp \
	&& ln -s /app/var/env /app/.env \
	&& ./bin/getcomposer \
	&& ./bin/composer.phar install --no-dev \
	&& chmod a+x /app/vendor/phpexiftool/exiftool/exiftool \
	&& bin/console migrate \
	&& bin/console setupusers -p admin admin \
	&& chown -R www-data:www-data var public/thumb assets \
	&& cp -r var dist.var \
	&& cp -r public dist.public 

VOLUME [ "/app/public", "/app/var", "/app/assets" ]

EXPOSE 9000

ENTRYPOINT [ "/bin/bash", "/app/docker/entrypoint" ]

CMD [ "fpm" ]