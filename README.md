# Pholar - a simple web-based media organizer

Pholar allows to search, edit, group and share media files from a folder on disk in a simple web interface.

## Features

- store metadata in database for fast search
- simple query language
- show thumbnail for a reasonable range of media files (images, videos, documents)
- edit metadata and tags
- create collections of media for quick access
- multiuser with basic permission system
- allow public access to media or keep everything private
- share media via public links, with optional passwords, expiration date and custom permissions
- writeback as much as possible of edited medatata
- support sqlite3 and mysql as database

## requirements

for the web app:

- mariadb
- perl >= 5.28
- sqlite >= 3.27
- imagemagick >= 6.9
- php >= 7.3
- php-gd
- php-imagick
- php-pdo
- php-pdo-sqlite *or* php-pdo-mysql
- php-xml
- php-curl
- php-mbstring
- php-zip
- php-json
- `file` utility
- ffmpeg >= 4.1 (optional for video/audio support)
- libreoffice (optional for document support)
- ghostscript (optional for pdf/postscript thumbs)

optional:
- php-inotify : for `fs:watch` console command (**wip**)

**note:** versions are from current package versions in Debian Buster (currently 'stable')
Pholar is tested on Debian Stable and Archlinux.


## install

### database

Pholar suports `sqlite` and `mariadb`. 

**NOTE**: `sqlite` is deprecated, you should really use `mariadb` for anything than local development.

**MariaDB**

Create a database and a user with all permissions on it

### clone source

``` sh
git clone https://gitlab.com/pholar/pholar.git
cd pholar 
```

### install deps

``` sh
bin/getcomposer
bin/composer.phar install --no-dev
```

### Configure

Copy `etc/default.settings.php.dist` to `etc/default.settings.php`
ad edit values.

File comes with comments.

Optional settings are commented out, with default values.

Only required options is db uri.


### create db schema  and users

``` sh
bin/console migrate
bin/console setupusers
```

### set permissions

Change user and group as required per your setup.
Folders `var/` and `public/thumb` must be writable by webserver.

``` sh
chown -R www-data:www-data var public/thumb
```

### setup task queue worker

Heavy tasks (like read/write metadata, generate thumbnails, etc) are
run by a separate process.

Script `bin/taskrunner` is a long-running process that will execute tasks
as soon they are put in queue.

You must have this script running on your system. It should run as webserver
user as a system process. A simple Systemd service unit is provided in 
`services/pholar-taskrunner.service`



## Update

Pull last version, install new dependencies and migrate db

``` sh
git pull
bin/composer.phar install --no-dev
bin/console migrate
```

## keep db up to date

### set cronjob

To update db when new or modifed photos are written in storage dir, set a
cronjob or a systemd timer to run `bin/console update`. The script keeps track
of last run datetime and will update only files modified after that.

Script must be run as `www-data` user.

### use watch (wip)

`bin/console fs:watch` command use inotify to wait for file events in storage folder and
update database accordingly.

Script must be run as `www-data` user.

A systemd unit is avaiable to run `fs:watch` with systemd

Link `fotomgrwatch.service` to `/etc/systemd/system/fotomgrwatch.service`

Edit it to match your setup and start/enable it with `systemctl`

#### php-inotify via PECL in debian

	# apt install php-pear php-dev
	# pecl install inotify
	# echo "extension=inotify.so" > /etc/php/7.3/mods-available/inotify.ini
	# phpenmod -s cli inotify

## Develop

``` sh
bin/getcomposer
bin/composer.phar install
```

### configure

Create `etc/dev.settings.php` file with your settings.

Create `.env` file with

```
PHOLAR_ENV=dev
````

### create db and users

``` sh
bin/console migrate
bin/console setupusers
```

### run dev server

``` sh
bin/console dev:server
```

### add pre-commit hook

``` sh
echo -e "#\!/bin/bash\n./bin/console dev:lint" > .git/hooks/pre-commit
chmod +x .git/hooks/pre-commit
```


## Docker !really experimental!

An experimental `Dockerfile` is provided.
It runs Pholar in a Debian-based container with php-fpm exposed on port 9000
To build the image run

```sh
docker build -t pholar .
```

it expose these volumes:

- `/app/public` : the part of the app that must be server by a webserver
- `/app/var` : holds db, cache, logs, config
- `/app/assets` : were assets must be.

Configuration is in `/app/var/env`.

Console commands are exposed as extra command, eg:

```sh
docker run -rm -t pholar help
```

runs `console help`.

Webserver must proxy php requests to fpm via fcgi on port 9000.

For Apache, should be something like

```
<VirtualHost *:8080>
  ServerName www.example.com
  DocumentRoot "/path/to/host/app/public"

  ProxyPassMatch ^/(.*\.php(/.*)?)$ fcgi://localhost:9000/app/public/$1
  DirectoryIndex /index.php index.php

  <Directory "/path/to/host/app/public">
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
  </Directory>
</VirtualHost>
```

An example docker compose file is provided in `docker/docker-compose.yml`.
It uses `bitnami/apache` image as webserver.
Local volume folders must be created before run:

```sh
cd docker
mkdir -p app/assets app/public app/var
docker compose build pholar
docker compose fetch apache
docker compose up
```

Run console commands as

```sh
docker run --rm pholar update
```

---

```sh
docker container prune -f && docker image prune -f && docker volume prune -f
```
