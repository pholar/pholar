#!/usr/bin/env bash
if [ "$1" == "-h" ] || [ "$1" == "--help" ]
then
	echo $(basename "$0") "[mysql|sqlite] [phpunit options]"
	exit
fi

db=
if [ "$1" == "mysql" ] || [ "$1" == "sqlite" ]
then
	db="$1"
	shift
fi

params=$@


if [ "$db" == "mysql" ] || [ "$db" == "" ]
then
	echo "# Running tests on mysql"
	PHOLAR_ENV=mysql vendor/bin/phpunit $params
fi
if [ "$db" == "sqlite" ] || [ "$db" == "" ]
then
	echo "# Running tests on sqlite"
	PHOLAR_ENV=sqlite vendor/bin/phpunit $params
fi
