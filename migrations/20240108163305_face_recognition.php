<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class FaceRecognition extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
		$table = $this->table("face", )
			// id PK
			->addColumn('photo_id', 'integer', ['null' => false])
			->addColumn('face_id', 'integer', ['null' => true])
			->addColumn('rect', 'string', ['null'=>false,])
			->addColumn('name', 'string', ['null'=>false, 'default'=>''])

            ->addForeignKey('photo_id', 'photo', 'id', ['delete'=>'CASCADE', ])
            ->addIndex(['name'], ['name'=>'face_name', 'unique'=>false])
			->create();
    }
}
