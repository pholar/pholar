<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;
use Pholar\App;
use Pholar\Settings;

final class Initial extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
		$settings = App::getApp()->getContainer()->get(Settings::class);
		
        $table = $this->table("photo", )
            // id PK
            ->addColumn('recordmtime', 'datetime', array('null'=>true,) )
            ->addColumn('filename', 'string', array('limit'=>256,'null'=>true,) )
            ->addColumn('location', 'string', array('limit'=>256,'null'=>true,) )
            ->addColumn('filesize', 'integer', array('null'=>true,) )
            ->addColumn('mtime', 'datetime', array('null'=>true,) )
            ->addColumn('type', 'string', array('limit'=>128,'null'=>true,) )
            ->addColumn('mime', 'string', array('limit'=>128,'null'=>true,) )
            ->addColumn('width', 'integer', array('null'=>true,) )
            ->addColumn('height', 'integer', array('null'=>true,) )
            ->addColumn('bps', 'integer', array('null'=>true,) )
            ->addColumn('colors', 'integer', array('null'=>true,) )
            ->addColumn('orientation', 'integer', array('null'=>true,) )
            ->addColumn('xresolution', 'integer', array('null'=>true,) )
            ->addColumn('yresolution', 'integer', array('null'=>true,) )
            ->addColumn('exposuretime', 'float', array('null'=>true,) )
            ->addColumn('aperture', 'float', array('null'=>true,) )
            ->addColumn('iso', 'integer', array('null'=>true,) )
            ->addColumn('focallength', 'float', array('null'=>true,) )
            ->addColumn('focallength35mm', 'float', array('null'=>true,) )
            ->addColumn('flash', 'integer', array('null'=>true,) )
            ->addColumn('datetime', 'datetime', array('null'=>true,) )
            ->addColumn('cameramaker', 'string', array('limit'=>128,'null'=>true,) )
            ->addColumn('cameramodel', 'string', array('limit'=>128,'null'=>true,) )
            ->addColumn('creatortool', 'string', array('limit'=>128,'null'=>true,) )
            ->addColumn('creator', 'string', array('limit'=>1024,'null'=>true,) )
            ->addColumn('rights', 'text', array('null'=>true,) )
            ->addColumn('owner', 'text', array('null'=>true,) )
            ->addColumn('marked', 'boolean', array('null'=>true,) )
            ->addColumn('license', 'text', array('null'=>true,) )
            ->addColumn('morepermissions', 'text', array('null'=>true,) )
            ->addColumn('attributionurl', 'text', array('null'=>true,) )
            ->addColumn('attributionname', 'text', array('null'=>true,) )
            ->addColumn('rating', 'integer', array('null'=>true,) )
            ->addColumn('title', 'text', array('null'=>true,) )
            ->addColumn('description', 'text', array('null'=>true,) )
            ->addColumn('comment', 'text', array('null'=>true,) )
            ->addColumn('latitude', 'float', array('null'=>true,) )
            ->addColumn('longitude', 'float', array('null'=>true,) )
            ->addColumn('altitude', 'float', array('null'=>true,) )
            ->addColumn('position', 'string', array('limit'=>1024,'null'=>true,) )
            ->addColumn('ratio', 'float', array('null'=>true,) )
            ->addColumn('format', 'string', array('limit'=>128,'null'=>true,) )
            ->addColumn('duration', 'float', array('null'=>true,) )
            ->addColumn('bitrate', 'integer', array('null'=>true,) )
            ->addColumn('streams', 'integer', array('null'=>true,) )
            ->addColumn('videocodec', 'string', array('limit'=>128,'null'=>true,) )
            ->addColumn('pixelformat', 'string', array('limit'=>8,'null'=>true,) )
            ->addColumn('framerate', 'string', array('limit'=>16,'null'=>true,) )
            ->addColumn('videobitrate', 'integer', array('null'=>true,) )
            ->addColumn('audiocodec', 'string', array('limit'=>128,'null'=>true,) )
            ->addColumn('samplerate', 'integer', array('null'=>true,) )
            ->addColumn('channels', 'integer', array('null'=>true,) )
            ->addColumn('channellayout', 'string', array('limit'=>32,'null'=>true,) )
            ->addColumn('audiobitrate', 'integer', array('null'=>true,) )
            ->addColumn('language', 'string', array('limit'=>128,'null'=>true,) )
            ->addColumn('pages', 'integer', array('null'=>true,) )
            ->addColumn('hash', 'string', array('limit'=>16,'null'=>true,) )
            ->addIndex(['location'], ['name' => 'photo_location'])
            ->addIndex(['type'], ['name' => 'photo_type'])
            ->addIndex(['cameramaker'], ['name' => 'photo_cameramaker'])
            ->addIndex(['cameramodel'], ['name' => 'photo_cameramodel'])
            ->addIndex(['creator'], ['name' => 'photo_creator'])
            ->addIndex(['rights'], ['name' => 'photo_rights'])
            ->addIndex(['rating'], ['name' => 'photo_rating'])
            ->addIndex(['title'], ['name' => 'photo_title'])
            ->addIndex(['comment'], ['name' => 'photo_comment'])
            ->addIndex(['filename','location'], ['name'=>'photo_fullpath', 'unique'=>true])
            ->addIndex(['filename'], ['name'=>'photo_name', 'unique'=>false])
            ->create();
        
        $table = $this->table("tag", )
            // id PK
            ->addColumn('tag', 'string', array('limit'=>255,'null'=>true,) )
            ->addIndex(['tag'], ['name'=>'tag_name', 'unique'=>true])
            ->create();
        
        $table = $this->table("photo_tag", )
            ->addColumn('photo_id', 'integer', array('null'=>false,) )
            ->addColumn('tag_id', 'integer', array('null'=>false,) )
            ->addForeignKey('photo_id', 'photo', 'id', ['delete' => 'CASCADE', ])
            ->addForeignKey('tag_id', 'tag', 'id', ['delete' => 'CASCADE', ])
            ->addIndex(['photo_id','tag_id'], ['name'=>'photo_tag_unique', 'unique'=>true])
            ->create();
        
        $table = $this->table("collection", )
            // id PK
            ->addColumn('name', 'string', array('limit'=>255,'null'=>true,) )
            ->addColumn('comment', 'text', array('null'=>true,) )
            ->addIndex(['name'], ['unique' => true, 'name' => 'collection_name'])
            ->create();
        
        $table = $this->table("photo_collection", )
            ->addColumn('photo_id', 'integer', array('null'=>false,) )
            ->addColumn('collection_id', 'integer', array('null'=>false,) )
            ->addForeignKey('photo_id', 'photo', 'id', ['delete' => 'CASCADE', ])
            ->addForeignKey('collection_id', 'collection', 'id', ['delete' => 'CASCADE', ])
            ->addIndex(['photo_id','collection_id'], ['name'=>'photo_collection_unique', 'unique'=>true])
            ->create();
        
        $table = $this->table("user", array('id'=>false,'primary_key'=>'user',))
            ->addColumn('user', 'string', array('limit'=>255,'null'=>false,) )
            ->addColumn('password', 'string', array('limit'=>255,'null'=>false,) )
            ->addColumn('can_login', 'boolean', array('null'=>false,'default'=>true,) )
            ->addColumn('can_edit', 'boolean', array('null'=>false,'default'=>true,) )
            ->addColumn('can_delete', 'boolean', array('null'=>false,'default'=>true,) )
            ->addColumn('can_share', 'boolean', array('null'=>false,'default'=>true,) )
            ->addColumn('can_download', 'boolean', array('null'=>false,'default'=>true,) )
            ->addColumn('can_upload', 'boolean', array('null'=>false,'default'=>true,) )
            ->addColumn('can_details', 'boolean', array('null'=>false,'default'=>true,) )
            ->addColumn('is_admin', 'boolean', array('null'=>false,'default'=>false,) )
            ->addColumn('query', 'string', array('limit'=>1024,'null'=>false,'default'=>'',) )
            ->create();
        
        $table = $this->table("share", array('id'=>false,'primary_key'=>'shareuid',))
            ->addColumn('shareuid', 'string', array('limit'=>40,'null'=>false,) )
            ->addColumn('user', 'string', array('limit'=>255,'null'=>false,) )
            ->addColumn('collection_id', 'integer', array('null'=>false,) )
            ->addColumn('password', 'string', array('limit'=>255,'null'=>true,) )
            ->addColumn('valid_from', 'date', array('null'=>true,) )
            ->addColumn('valid_until', 'date', array('null'=>true,) )
            ->addColumn('can_download', 'boolean', array('null'=>false,'default'=>true,) )
            ->addColumn('can_details', 'boolean', array('null'=>false,'default'=>true,) )
            ->addForeignKey('user', 'user', 'user', ['delete' => 'CASCADE', ])
            ->addForeignKey('collection_id', 'collection', 'id', ['delete' => 'CASCADE', ])
            ->create();

        if ($settings->get('dsn', 'scheme') == "mysql") {
            $this->execute("DROP FUNCTION IF EXISTS hashdistance");
            $this->execute(
                "CREATE FUNCTION hashdistance(A1 VARCHAR(16), A2 VARCHAR(16))
                RETURNS INT DETERMINISTIC
                RETURN
                length(replace(conv(conv(A1, 16,10) ^ conv(A2, 16,10), 10, 2),'0', ''))"
            );
        }
    
    }
}
