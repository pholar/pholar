<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class Tasks extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
		$table = $this->table("task")
			// id PK
			->addColumn('photo_id', 'integer', ['null' => true])
			->addColumn('priority', 'integer', ['default' => 0,] )
			->addColumn('task', 'string', ['null'=>false,] )
			->addColumn('args', 'string', ['null'=>false,] )
			->addColumn('schedule', 'datetime', ['default' => '1979-01-01 00:00:00'])
			->addColumn('require', 'integer', ['null' => true])
			->addColumn('status', 'integer', ['default' => 0])
			->addColumn('output', 'string', ['null' => true, 'default' => null])
			->addColumn('started_at', 'datetime', ['null' => true, 'default' => null])
			->addColumn('finished_at', 'datetime', ['null' => true, 'default' => null])
			->addIndex(['args'], ['name'=>'task_args_idx', 'unique'=>false])
			->addIndex(['photo_id'], ['name'=>'photo_id_idx', 'unique'=>false])

			->create();
    }
}
