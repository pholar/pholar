<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class ResolveColumnNameClash extends AbstractMigration
{
    public function up(): void
    {
		$table = $this->table("task")
		    ->renameColumn("require", "dependson")
		    ->save();
    }
    
    public function down(): void
    {
		$table = $this->table("task")
		    ->renameColumn("dependson", "require")
		    ->save();
    }
}
